package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author sun li
 * @Date 2018/11/4 16:33
 * @Description
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class starts {
    public static void main(String[] args) {
        SpringApplication.run(starts.class, args);
    }
}
