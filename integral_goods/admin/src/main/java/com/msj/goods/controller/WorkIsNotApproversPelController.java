package com.msj.goods.controller;

import com.github.pagehelper.PageInfo;
import com.msj.goods.common.annotation.Log;
import com.msj.goods.common.constants.JsonResultConstants;
import com.msj.goods.common.utils.DateUtils;
import com.msj.goods.common.web.base.JsonResult;
import com.msj.goods.entity.IntegralApproval;
import com.msj.goods.entity.SysUser;
import com.msj.goods.service.IntegralApprovalService;
import com.msj.goods.service.SysDeptService;
import com.msj.goods.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sun li
 * @Date 2018/11/14 10:11
 * @Description 工作台 审批人和高级审批人
 */
@RestController
@RequestMapping("/approversPel")
@Api(description = "工作台 审批人和高级审批人")
public class WorkIsNotApproversPelController {

    @Autowired
    private IntegralApprovalService integralApprovalService;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysDeptService sysDeptService;

    @PostMapping(value = "/selectApproversList")
    @Log(title = "是审批人  查询待审批")
    @ApiOperation(value="查询待我审批和已审批列表", notes="查询我审批和已审批列表")
    @ApiImplicitParam(name = "pageSize,pageNum",value = "json对象",required = true)
    public JsonResult selectIntegralList(@RequestParam(value = "pageSize" ,required = false) Integer pageSize,
                                         @RequestParam(value = "pageNum" ,required = false) Integer pageNum,
                                         @RequestParam(value = "status" ,required = false) Integer status,
                                         @RequestParam(value = "search" ,required = false) String search){
        PageInfo<Map> pageInfo = integralApprovalService.selectApproversPel(pageSize,pageNum,status,search);
        return JsonResult.success(pageInfo , JsonResultConstants.SUCCESS);
    }

    @GetMapping(value = "/selectApproversDetail/{approvalId}")
    @Log(title = "待审和已审批详情页")
    @ApiOperation(value = "待审和已审批详情页")
    @ApiImplicitParam(name = "approvalId", value = "待审和已审批详情页", required = true)
    public JsonResult selectApproversDetail(@PathVariable("approvalId") String approvalId) {
        IntegralApproval aa = integralApprovalService.selectById(approvalId);
        IntegralApproval ia =  integralApprovalService.selectById(approvalId);
        Map map = new HashMap();
        map.put("addIntegral",ia.getSqIntegral());
        map.put("approvalContent",ia.getApprovalContent());
        map.put("userName",ia.getUserName());
        map.put("userImg",ia.getUserImg());
        SysUser use =  sysUserService.selectById(ia.getUserId());
        map.put("userDept",sysDeptService.selectById(use.getDeptId()).getDeptName());
        map.put("sqTime", DateUtils.getStringDate(ia.getSqTime()));
        if (ia.getSpTime() !=null){
            map.put("spTime",DateUtils.getStringDate(ia.getSpTime()));
        }
        map.put("approvalNum",ia.getApprovalNum());
        map.put("approvalImg",ia.getApprovalImg1().split(","));
        map.put("approvalTitle",ia.getApprovalTitle());
        map.put("status",ia.getStatus());
    /* String shenPiRen = ia.getShenPiRenIds();如果数据库是数组 就切割   */
        String [] ids = ia.getShenPiRenIds().split(",");
         for (String id : ids) {
        SysUser user =  sysUserService.selectById(ia.getShenPiRenIds());
        map.put("appName",user.getUserName());
        map.put("appImg",user.getAvatar());
        map.put("appDept",sysDeptService.selectById(user.getDeptId()).getDeptName());

         }

        return JsonResult.success(map , JsonResultConstants.SUCCESS);
    }

    @GetMapping(value = "/approversYesNo/{approvalId}/{status}")
    @Log(title = "前端传来 approvalId 和 状态来判断 status ")
    @ApiOperation(value = "待审和已审批详情页")
    @ApiImplicitParam(name = "approvalId", value = "待审和已审批详情页", required = true)
    public JsonResult approversYesNo(@PathVariable("approvalId") String approvalId,@PathVariable("status") String status) {

        return JsonResult.success(integralApprovalService.approversYesNo(approvalId,status) , JsonResultConstants.SUCCESS);
    }
}
