package com.msj.goods.controller;

import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.web.base.JsonResult;
import com.msj.goods.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sun li
 * @Date 2018/11/5 16:59
 * @Description
 */
@RestController
@RequestMapping("/user")
@Api(description = "员工信息")
public class LoginController {

    /*@Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRoleService sysRoleService;


    @PostMapping(value = "/login")
    @Log(title = "员工登录")
    @ApiOperation(value="员工登录", notes="员工登录系统")
    @ApiImplicitParam(name = "LoginVo",value = "员工json对象",required = true)
    public AjaxResult userLogin( @RequestParam("phone")String phone,@RequestParam("password")String password  ){
        Subject subject = ShiroUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(phone,password);
        try
        {
            //设置session 永远session
            ShiroUtils.setSession();
            subject.login(token);
        SysUser user =  ShiroUtils.getUserEntity();
            EntityWrapper<SysUserRole>  ew = new EntityWrapper<SysUserRole> ();
            ew.eq("user_id",user.getUserId());
            SysUserRole userRole = sysUserRoleService.selectOne(ew);
         SysRole role = sysRoleService.selectById(userRole.getRoleId());
         if (role.getRoleKey().equalsIgnoreCase("admin")){
             return AjaxResult.success("admin");
         }else if(role.getRoleKey().equalsIgnoreCase("superAdmin")){
             return AjaxResult.success("superAdmin");
         }else{
             return AjaxResult.success("common");
         }

        }
        catch (AuthenticationException e)
        {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage()))
            {
                msg = e.getMessage();
            }
            return AjaxResult.error(msg);
        }
    }*/

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private IntegralService integralService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysUserPostService sysUserPostService;

    @Autowired
    private SysRoleDeptService sysRoleDeptService;

    @Autowired
    private SysRoleService sysRoleService;







    @GetMapping("unauth")
    public JsonResult unAuth(){
        return JsonResult.failure(101,"登录超时，请重新登录");

    }
    @GetMapping("/logout")
    @ResponseBody
    @ApiOperation(value="退出系统", notes="用户退出系统")
    public JsonResult logout() {
        ShiroUtils.logout();
        return JsonResult.success("退出！");
    }
}
