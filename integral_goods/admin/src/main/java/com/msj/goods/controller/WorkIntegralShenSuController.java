package com.msj.goods.controller;

import com.github.pagehelper.PageInfo;
import com.msj.goods.common.annotation.Log;
import com.msj.goods.common.constants.JsonResultConstants;
import com.msj.goods.common.web.base.JsonResult;
import com.msj.goods.entity.IntegralApproval;
import com.msj.goods.service.IntegralApprovalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sun li
 * @Date 2018/11/10 11:42
 * @Description 积分申诉
 */
@RestController
@RequestMapping("/integral")
@Api(description = "积分申诉")
public class WorkIntegralShenSuController {

    @Autowired
    private IntegralApprovalService integralApprovalService;

    @PostMapping(value = "/selectIntegralListYes")
    @Log(title = "积分申诉通过")
    @ApiOperation(value="积分申诉通过列表", notes="积分申诉通过列表")
    @ApiImplicitParam(name = "pageSize,pageNum",value = "json对象",required = true)
    public JsonResult selectIntegralList(@RequestParam("pageSize") String pageSize, @RequestParam("pageNum") String pageNum){
        PageInfo<IntegralApproval> pageInfo = integralApprovalService.selectIntegralListYes(pageSize,pageNum);
        return JsonResult.success(pageInfo , JsonResultConstants.SUCCESS);
    }
    @PostMapping(value = "/selectIntegralListNo")
    @Log(title = "积分申诉审批拒绝列表")
    @ApiOperation(value="积分申诉审批拒绝列表", notes="积分申诉审批拒绝列表")
    @ApiImplicitParam(name = "pageSize,pageNum",value = "json对象",required = true)
    public JsonResult selectCswdListNo(@RequestParam("pageSize") String pageSize, @RequestParam("pageNum") String pageNum){
        PageInfo<IntegralApproval> pageInfo = integralApprovalService.selectIntegralListNo(pageSize,pageNum);
        return JsonResult.success(pageInfo , JsonResultConstants.SUCCESS);
    }
}
