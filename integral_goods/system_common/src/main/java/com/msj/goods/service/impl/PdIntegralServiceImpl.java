package com.msj.goods.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.entity.PdIntegral;
import com.msj.goods.entity.SysUser;
import com.msj.goods.mapper.PdIntegralMapper;
import com.msj.goods.service.PdIntegralService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 品德A积分管理 服务实现类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
@Service
public class PdIntegralServiceImpl extends ServiceImpl<PdIntegralMapper, PdIntegral> implements PdIntegralService {

    /**
     *  查询个人部门下所有的品德积分项
     */
    @Override
    public PageInfo<PdIntegral> selectDeclareMoral(String pageSize,String pageNum,String search) {

        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
       /* EntityWrapper<PdIntegral> ew = new EntityWrapper<>();
        ew.eq("dept_id", user.getDeptId());
        ew.like("behavior_title",search);*/
        List<PdIntegral> list = this.baseMapper.selectDeptAndParentMenu(user.getDeptId(),search);

        return new PageInfo<>(list);
    }
}
