package com.msj.goods.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.entity.SysUser;
import com.msj.goods.entity.YjIntegral;
import com.msj.goods.mapper.YjIntegralMapper;
import com.msj.goods.service.YjIntegralService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 业绩B积分管理 服务实现类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
@Service
public class YjIntegralServiceImpl extends ServiceImpl<YjIntegralMapper, YjIntegral> implements YjIntegralService {

    @Override
    public PageInfo<YjIntegral> selectDeclareResults(String pageSize,String pageNum,String search) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
       /* EntityWrapper<YjIntegral> ew = new EntityWrapper<>();
        ew.eq("dept_id", user.getDeptId());
        ew.like("behavior_title",search);*/
        List<YjIntegral> list = this.baseMapper.selectDeptParentList(user.getDeptId(),search);
        return new PageInfo<>(list);
    }
}
