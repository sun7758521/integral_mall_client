package com.msj.goods.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.constants.UserConstants;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.utils.StringUtils;
import com.msj.goods.entity.Integral;
import com.msj.goods.entity.SysRole;
import com.msj.goods.entity.SysUser;
import com.msj.goods.entity.SysUserRole;
import com.msj.goods.mapper.IntegralApprovalMapper;
import com.msj.goods.mapper.IntegralMapper;
import com.msj.goods.service.IntegralService;
import com.msj.goods.service.SysRoleService;
import com.msj.goods.service.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 积分表 服务实现类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
@Service
public class IntegralServiceImpl extends ServiceImpl<IntegralMapper, Integral> implements IntegralService {

    @Autowired
    private  IntegralMapper integralMapper;

    @Autowired
    private IntegralApprovalMapper integralApprovalMapper;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRoleService sysRoleService;
    /**
     *  查询所有积分榜展示数据
     *  以及所有的检索条件
     */

    @Override
    public PageInfo<Map> selectAllList(Integer pageSize, Integer pageNum, Integer times, String deptId, String postId, String typeId, String spTime1, String spTime2, String search) {
        PageHelper.startPage(pageNum,pageSize);
      SysUser user =  ShiroUtils.getUserEntity();

        List<Map>  integralBangs = null;

        /** 超级管理员 通过角色*/
        /*if (role.getRoleKey().equalsIgnoreCase("superAdmin")){*/
        if (user.getIsApprover()== UserConstants.SUPER_ADMIN){

            String deptIds = deptId;
            if(StringUtils.isNotNull(times) ){
                /**
                 *  查询本部门当天数据
                 */
                if (times==1){
                    integralBangs   = integralApprovalMapper.selectAllDayData(deptIds, postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询本部门 当月数据*/
                if (times==2 ){
                    integralBangs  = this.integralApprovalMapper.selectAllMonthData(deptIds, postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询本部门当季数据*/
                if (times==3 ){
                    integralBangs = this.integralApprovalMapper.selectAllJiData(deptIds, postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询本部门当年数据*/
                if (times==4){
                    integralBangs = this.integralApprovalMapper.selectAllYearData(deptIds, postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
            }
        }


        /** 普通员工 */
        if(user.getIsApprover()==UserConstants.COMMON){
            /** 当前登录用户属于哪个部门id  */
            Integer  deptIds =  user.getDeptId();
            /** 当前登录用户属于哪个部门id  */
            if(StringUtils.isNotNull(times) ){
                /**
                 *  查询本部门当天数据
                 */
                if (times==1){
                    integralBangs   = integralApprovalMapper.selectAdminAndEmpDayData(user.getIsApprover(),deptIds+"", postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询本部门 当月数据*/
                if (times==2 ){
                    integralBangs  = this.integralApprovalMapper.selectAdminAndEmpMonthData(user.getIsApprover(),deptIds+"", postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询本部门当季数据*/
                if (times==3 ){
                    integralBangs = this.integralApprovalMapper.selectAdminAndEmpJiData(user.getIsApprover(),deptIds+"", postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询本部门当年数据*/
                if (times==4){
                    integralBangs = this.integralApprovalMapper.selectAdminAndEmpYearData(user.getIsApprover(),deptIds+"", postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
            }
        }
        /** 管理员 */
        if(user.getIsApprover()==UserConstants.ADMIN){
            if(StringUtils.isNotNull(times) ){
                /**
                 *  查询管理员当天数据
                 */
                if (times==1){
                    integralBangs   = integralApprovalMapper.selectAdminAndEmpDayData(user.getIsApprover(),deptId, postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询管理员 当月数据*/
                if (times==2 ){
                    integralBangs  = this.integralApprovalMapper.selectAdminAndEmpMonthData(user.getIsApprover(),deptId, postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询管理员当季数据*/
                if (times==3 ){
                    integralBangs = this.integralApprovalMapper.selectAdminAndEmpJiData(user.getIsApprover(),deptId, postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
                /** 查询管理员当年数据*/
                if (times==4){
                    integralBangs = this.integralApprovalMapper.selectAdminAndEmpYearData(user.getIsApprover(),deptId, postId, typeId, spTime1,spTime2, search);
                    for (Map map : integralBangs) {
                        map.put("amount",Integer.parseInt(map.get("total")+"")-Integer.parseInt(map.get("deduce")+""));
                        map.put("sqIntegral",map.get("total"));
                        map.put("deduce",map.get("deduce"));
                    }
                }
            }

        }

        return new PageInfo<>(integralBangs);
    }
}
