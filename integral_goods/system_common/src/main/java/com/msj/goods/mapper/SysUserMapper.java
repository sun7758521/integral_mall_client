package com.msj.goods.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.msj.goods.entity.SysUser;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     *  通过手机号查询当前用户
     *  return user
     */
    SysUser selectPhoneByUser(@Param("phone") String phone);

    /**
     *  查询所有的员工
     *  return user
     */
    //List<Map> selectAllList();
}
