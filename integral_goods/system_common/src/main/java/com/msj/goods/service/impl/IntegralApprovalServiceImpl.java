package com.msj.goods.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.constants.UserConstants;
import com.msj.goods.common.constants.UsingStatusConstans;
import com.msj.goods.common.utils.DateUtils;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.utils.StringUtils;
import com.msj.goods.entity.*;
import com.msj.goods.mapper.IntegralApprovalMapper;
import com.msj.goods.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 审批管理 服务实现类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
@Service
public class IntegralApprovalServiceImpl extends ServiceImpl<IntegralApprovalMapper, IntegralApproval> implements IntegralApprovalService {

    @Autowired
    private IntegralApprovalMapper integralApprovalMapper;

    @Autowired
    private IntegralApprovalService integralApprovalService;
    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private IntegralLogService integralLogService;

    @Autowired
    private IntegralService integralService;

    @Autowired
    private SysPostService sysPostService;

    @Autowired
    private SysUserPostService  sysUserPostService;

    @Autowired
    private SysDeptService sysDeptService;


    /**
     * 查询冠军
     * @return IntegralApproval
     *
     * @param */
    @Override
    public List<Map> selectGJ(Integer deptIds) {
        return integralApprovalMapper.selectGJ(deptIds);
    }

    @Override
    public PageInfo<IntegralApproval> selectOneIntegralApproval(String pageSize,String pageNum) {
       SysUser user = ShiroUtils.getUserEntity();
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        if(user !=null){
            ew.eq("user_id",user.getUserId());
        }

        /** 审批通过的 */
        ew.eq("status","1");
        List<IntegralApproval>  logs = integralApprovalMapper.selectList(ew);
        Collections.reverse(logs);
        return   new PageInfo<>(logs);

    }

    @Override
    public boolean insertIntegralApprover(String addIntegral, String spRemark, String typeId, String[] from, String[] to, String[] approvalImg1, String approvalContent, String approvalTitle, String approvalId, String[] apps) {
        IntegralApproval integralApproval = new IntegralApproval();
        //所有提交人的ids

         SysUser sysUser = ShiroUtils.getUserEntity();
        for (String id : from) {
            SysUser user =  sysUserService.selectById(id);
            integralApproval.setApprovalNum("JFB"+ DateUtils.dateTime());
            integralApproval.setSqTime(DateUtils.getNowDate());
            integralApproval.setSqIntegral(Integer.parseInt(addIntegral) );
            integralApproval.setIntegralTypeId(Integer.parseInt(approvalId));
            integralApproval.setApprovalContent(approvalContent);
            integralApproval.setApprovalTitle(approvalTitle);
            integralApproval.setApprovalImg1(StringUtils.join(approvalImg1, ","));
            integralApproval.setSpRemark(spRemark);
            integralApproval.setTypeId(Integer.parseInt(typeId));
            integralApproval.setUserImg(user.getAvatar());
            integralApproval.setUserId(user.getUserId());
            integralApproval.setUserName(user.getUserName());
            integralApproval.setStatus(0);
            /**  添加用户部门id*/
            integralApproval.setUserDeptId(user.getDeptId());
            integralApproval.setUserDept(sysDeptService.selectById(user.getDeptId()).getDeptName());
            /**  通过用户id 查询职位添加职位id*/
            EntityWrapper<SysUserPost> ew = new EntityWrapper<SysUserPost>();
            ew.eq("user_id",user.getUserId());
            SysUserPost userPost =  sysUserPostService.selectOne(ew);
            integralApproval.setUserPostId(userPost.getPostId());
            /** 提交人的信息*/
            integralApproval.setTiJiaoId(sysUser.getUserId());
            integralApproval.setTiJiaoName(sysUser.getUserName());
            integralApproval.setTiJiaoNameImg(sysUser.getAvatar());
            integralApproval.setUserPhone(Long.parseLong(user.getPhonenumber()));
           /**  申请人  ids  默认是自己 from 是所有的申请人 */
            integralApproval.setTiJiaoRenIds(StringUtils.join(from, ","));

            /**  审批人 ids 把数组转成字符串保存到数据库*/
            integralApproval.setShenPiRenIds(StringUtils.join(apps, ","));

            integralApproval.setChaoSongIds(StringUtils.join(to, ","));
           /**  把 数组转成字符串保存到数据库*/
            integralApproval.setChaoSongIds(StringUtils.join(to, ","));

          /** 正常状态 0*/
            integralApproval.setStatus(Integer.parseInt(UserConstants.NORMAL));
            int  i = this.baseMapper.insert(integralApproval);
            if (i>0){
                return true;
            }
        }
        return false;
    }

    /**
     *   查询待审和已审批列表
     */
    @Override
    public PageInfo<Map> selectApproversPel(Integer pageSize, Integer pageNum,Integer status,String search) {
     SysUser user =  ShiroUtils.getUserEntity();
     // 分页
      PageHelper.startPage(pageNum,pageSize);
        List<Map> list = null;
        /** 管理员根据部门 */
     if (user.getIsApprover()==1 && status==0){
         list  = this.baseMapper.selectAllList(user.getUserId(),user.getDeptId(),search);
     }
     /** 查询审批通过和撤销审批的数据 */
    else if(user.getIsApprover()==1 && status==1){
         list  = this.baseMapper.selectShenPiTongAndCheXiaoList(user.getUserId(),user.getDeptId(),search);
     }
        /** 超级审批管理员 */
      else if(user.getIsApprover()==2 && status == 0){
           list = this.baseMapper.selectSuperNotApp(user.getUserId(),search);
       }else if(user.getIsApprover()==2 && status == 1){
            list = this.baseMapper.selectSuperYiShenCheXiao(user.getUserId(),search);
        }
       /** 普通员工没有审批权力 */
     else if(user.getIsApprover()==3){
           list = new ArrayList<>();
       }
        return  new PageInfo<>(list);
    }

    /**
     *  approvalImg1 上传的图片没有用到
     *  管理员自由奖扣
     */
    @Override
    public boolean freeIntegral(String addIntegral,String delIntegral, String spRemark, String typeId, String[] from, String[] approvalImg1, String approvalContent, String approvalTitle) {
     SysUser sysUser =  ShiroUtils.getUserEntity();
        for (String id : from) {
         int row =  insertApproval(addIntegral,delIntegral,spRemark,typeId,approvalContent,approvalTitle,approvalImg1,sysUser,id);
         int rows =  insertLog(addIntegral,delIntegral,spRemark,typeId,approvalContent,approvalTitle,sysUser);
            Integer i = updateIIntegral(id,addIntegral,delIntegral,typeId);
            if(i>0){
                return true;
            }
        }
        return false;
    }



    /**
     * 自由奖扣申请
     */
    @Override
    public boolean freeIntegralApprover(String addIntegral, String delIntegral, String spRemark, String typeId, String[] from, String[] to, String[] approvalImg1, String approvalContent, String approvalTitle, String approvalId, String[] apps) {

        for (String id : from) {
         int row =    insertIntegralApproval(addIntegral,delIntegral,spRemark,typeId,approvalContent,approvalTitle,approvalId,approvalImg1,apps,to,id,from);
         if(row >0){
             return true;
         }
        }
        return false;
    }

    /**
     * 申请  1通过、 2不通过、3撤销、
     */
    @Override
    public int approversYesNo(String approvalId, String status) {
        IntegralApproval approval = this.baseMapper.selectById(approvalId);
        int i = 0;
         if (status.equalsIgnoreCase("1")){
            approval.setStatus(1);
            approval.setSpTime(DateUtils.getNowDate());
            /** 修改总积分*/
            EntityWrapper<Integral> ew = new EntityWrapper<Integral>();
               ew.eq("user_id",approval.getUserId());
             Integral integral = integralService.selectOne(ew);
             integral.setCountIntegral(integral.getCountIntegral() + approval.getSqIntegral());
             integral.setAddIntegral(integral.getAddIntegral() + approval.getSqIntegral());
             integralService.updateById(integral);
           int row =  addLog(approval);
            i =  this.baseMapper.updateById(approval);
        }
        if (status.equalsIgnoreCase("2")){
            approval.setStatus(2);
            approval.setSpTime(DateUtils.getNowDate());
            i = this.baseMapper.updateById(approval);
        }
        if (status.equalsIgnoreCase("3")){
            approval.setStatus(3);
            approval.setSpTime(DateUtils.getNowDate());
            i = this.baseMapper.updateById(approval);
        }
        return i;
    }
    /** 审批通过添加日志 */
    private int addLog(IntegralApproval approval) {
        IntegralLog log = new IntegralLog();
            log.setGetTime(DateUtils.getNowDate());
            log.setIntegralContent(approval.getApprovalTitle());
            log.setRemark(approval.getRemark());
            log.setBianIntegral(approval.getSqIntegral());
            log.setIntegralTitle(approval.getApprovalTitle());
            log.setTypeId(approval.getTypeId());
            log.setApprovalNum(approval.getApprovalNum());
            log.setUserName(approval.getUserName());
            log.setUserId(approval.getUserId());
            log.setUserPhone(approval.getUserPhone());
            log.setUserImg(approval.getUserImg());
            /** 添加部门名称  */
            log.setUserDept(sysDeptService.selectById(approval.getUserDeptId()).getDeptName());
            EntityWrapper<SysUserPost> ew = new EntityWrapper<SysUserPost>();
            ew.eq("user_id",approval.getUserId());
             SysUserPost userPost =  sysUserPostService.selectOne(ew);
           /** 添加职位名称  */
            log.setUserPost(sysPostService.selectById(userPost.getPostId()).getPostName());
            log.setStatus(UsingStatusConstans.LOG_DEFULT);
     boolean flag =  integralLogService.insert(log);
        if (flag){
            return 1;
        }
         return 0;
    }

    /**
     * 抄送我列表
     */
    @Override
    public PageInfo<IntegralApproval> selectCopyList(String pageSize, String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
         SysUser sysUser =  ShiroUtils.getUserEntity();
        List<IntegralApproval> list = this.baseMapper.selectListCopy(sysUser.getUserId());
        return new PageInfo<>(list);
    }

    /**
     * 查询管理员
     */
    @Override
    public List<Map> selectAdminGJ(Integer isApprover) {
        return this.baseMapper.selectAdminGJ(isApprover);
    }
    /**
     *  超级管理员的排名以及名次
     * */
    @Override
    public List<Map> selectAdminAndSuperGJ() {
        return this.baseMapper.selectAdminAndSuperGJ();
    }

    /** 超级管理员显示待我审批的数量*/
    @Override
    public Integer selectSuperCount(Integer userId) {
        return this.baseMapper.selectSuperCount(userId);
    }

    /** 管理员显示待我审批的数量 */
    @Override
    public Integer selectCountDwsp(Integer userId,Integer deptId) {
        return this.baseMapper.selectCountDwsp(userId,deptId);
    }

    /**
     *   领导表扬加分  from 给员工申请
     */
    @Override
    public boolean leaderIntegral(String approvalContent, String approvalTitle, String typeId,  String[] approvalImg11, String addIntegral, String spRemark, String[] from) {
    SysUser user =  ShiroUtils.getUserEntity();
        for (String id : from) {
        int row = insertApproval(addIntegral,null,spRemark,typeId,approvalContent,approvalTitle,approvalImg11,user,id);
        int rowLog = insertLog(addIntegral,null,spRemark,typeId,approvalContent,approvalTitle,user);

         int i =   updateIIntegral(id,addIntegral,null,typeId);
         if(i>0){
             return true;
         }
        }
        return false;
    }


    /**
     *  自由奖扣添加申批记录
     */
    private int insertIntegralApproval(String addIntegral, String delIntegral, String spRemark, String typeId, String approvalContent, String approvalTitle, String approvalId, String[] approvalImg1, String[] apps, String[] to,String id, String[] from) {
        SysUser sysUser =  ShiroUtils.getUserEntity();

        IntegralApproval approval = new IntegralApproval();
        approval.setApprovalNum("JFB"+DateUtils.dateTime());
        if(delIntegral !=null){
            approval.setkIntegral(Integer.parseInt(delIntegral));
        }
        if(addIntegral !=null){
            approval.setSqIntegral(Integer.parseInt(addIntegral));
        }
        /** 奖励那个人的 id  */
        SysUser user =  sysUserService.selectById(id);
        approval.setUserId(user.getUserId());
        approval.setUserDeptId(user.getDeptId());
        approval.setUserPhone(Long.parseLong(user.getPhonenumber()));
        approval.setUserDept(sysDeptService.selectById(user.getDeptId()).getDeptName());
        EntityWrapper<SysUserPost> ew = new EntityWrapper<SysUserPost>();
        ew.eq("user_id",user.getUserId());
        SysUserPost userPost =  sysUserPostService.selectOne(ew);
        approval.setUserPostId(userPost.getPostId());
        approval.setUserPost(sysPostService.selectById(userPost.getPostId()).getPostName());
        approval.setUserName(user.getUserName());
        approval.setUserImg(user.getAvatar());
        approval.setSqTime(DateUtils.getNowDate());

        approval.setSpRemark(spRemark);
        if(typeId !=null){
            approval.setTypeId(Integer.parseInt(typeId));
        }
        approval.setApprovalTitle(approvalTitle);
        approval.setApprovalContent(approvalContent);
        if(approvalId !=null){
            approval.setIntegralTypeId(Integer.parseInt(approvalId));
        }
        /** 提交那个人的 id   from 申请人  to 抄送人    apps 审批人        */
        approval.setTiJiaoId(sysUser.getUserId());
        approval.setTiJiaoNameImg(sysUser.getAvatar());
        approval.setTiJiaoName(sysUser.getUserName());
        approval.setTiJiaoRenIds(StringUtils.join(from, ","));


        approval.setShenPiRenIds(StringUtils.join(apps, ","));
        /** 自由奖扣审批的默认状态 */
        approval.setStatus(UsingStatusConstans.APP_DEFULT);
        approval.setChaoSongIds(StringUtils.join(to, ","));
        approval.setApprovalImg1(StringUtils.join(approvalImg1, ","));
      int row = this.baseMapper.insert(approval);
      return  row;
    }

    /**
     * 添加申批记录里
     */
    private int insertApproval(String addIntegral, String delIntegral, String spRemark, String typeId, String approvalContent, String approvalTitle, String[] approvalImg1,SysUser sysUser,String id) {

        IntegralApproval approval = new IntegralApproval();
        approval.setApprovalNum("JFB"+DateUtils.dateTime());
        if(delIntegral !=null){
            approval.setkIntegral(Integer.parseInt(delIntegral));
        }
        if(addIntegral !=null){
            approval.setSqIntegral(Integer.parseInt(addIntegral));
        }
        /** 奖励那个人的所用信息 */
        SysUser user =  sysUserService.selectById(id);
        approval.setUserId(Integer.parseInt(id));
        approval.setUserDeptId(user.getDeptId());
        approval.setUserPhone(Long.parseLong(user.getPhonenumber()));
        approval.setUserDept(sysDeptService.selectById(user.getDeptId()).getDeptName());
        EntityWrapper<SysUserPost> ew = new EntityWrapper<SysUserPost>();
        ew.eq("user_id",user.getUserId());
        SysUserPost userPost =  sysUserPostService.selectOne(ew);
        approval.setUserPostId(userPost.getPostId());
        approval.setUserPost(sysPostService.selectById(userPost.getPostId()).getPostName());
        approval.setUserName(user.getUserName());
        approval.setUserImg(user.getAvatar());
        approval.setSqTime(DateUtils.getNowDate());
        approval.setSpRemark(spRemark);
        if(typeId !=null){
            approval.setTypeId(Integer.parseInt(typeId));
        }
        approval.setApprovalTitle(approvalTitle);
        approval.setApprovalContent(approvalContent);
        /** 提交那个人的信息也就相当于管理员的信息 */
        approval.setTiJiaoId(sysUser.getUserId());
        approval.setTiJiaoNameImg(sysUser.getAvatar());
        approval.setTiJiaoName(sysUser.getUserName());
        approval.setStatus(0);
        approval.setApprovalImg1(StringUtils.join(approvalImg1, ","));
        int row = this.baseMapper.insert(approval);
        return  row;
    }
    /**
     *  添加管理员奖扣 日志
     *
     * @param addIntegral
     * @param delIntegral
     * @param id
     * @param typeId
     */
    private Integer updateIIntegral(String id, String addIntegral, String delIntegral,String typeId) {
        EntityWrapper<Integral> ew = new EntityWrapper<Integral> ();
        ew.eq("user_id",id);
        Integral integral =  integralService.selectOne(ew);
        if(addIntegral != null){
            integral.setCountIntegral(integral.getCountIntegral() + Integer.parseInt(addIntegral));
            integral.setAddIntegral(integral.getAddIntegral() + Integer.parseInt(addIntegral));
        }
        if(delIntegral != null ){
            integral.setCountIntegral(integral.getCountIntegral() - Integer.parseInt(delIntegral));
            integral.setDelIntegral(integral.getDelIntegral() + Integer.parseInt(delIntegral));
        }
      boolean flag = integralService.updateById(integral);
        if(flag){
           return 1;
        }
        return 0;
    }

    /**
     *  添加管理员奖扣 日志
     *
     * @param addIntegral
     * @param delIntegral
     * @param spRemark
     * @param typeId
     * @param approvalContent
     * @param approvalTitle
     */
    private int insertLog(String addIntegral, String delIntegral, String spRemark, String typeId, String approvalContent, String approvalTitle,SysUser sysUser) {

        IntegralLog log = new IntegralLog();
        log.setApprovalNum("JFB"+DateUtils.getNowDate());
        if(addIntegral !=null){
            log.setBianIntegral(Integer.parseInt(addIntegral));
        }
        if(delIntegral !=null){
            log.setkIntegral(Integer.parseInt(delIntegral));
        }
        if(typeId != null){
            log.setTypeId(Integer.parseInt(typeId));
        }
        /** 代表领导奖励的日志 */
        log.setStatus(UsingStatusConstans.ADMIN__LOG_DEFULT);
        log.setUserId(sysUser.getUserId());
        log.setUserImg(sysUser.getAvatar());
        log.setUserPhone(Long.parseLong(sysUser.getPhonenumber()));
        log.setUserName(sysUser.getUserName());
        EntityWrapper<SysUserPost> ew = new EntityWrapper<SysUserPost>();
        ew.eq("user_id",sysUser.getUserId());
        SysUserPost userPost =  sysUserPostService.selectOne(ew);
        log.setUserPost(sysPostService.selectById(userPost.getPostId()).getPostName());
        log.setUserDept(sysDeptService.selectById(sysUser.getDeptId()).getDeptName());
        log.setRemark(spRemark);
        log.setIntegralTitle(approvalTitle);
        log.setIntegralContent(approvalContent);
        log.setGetTime(DateUtils.getNowDate());
       boolean flag = integralLogService.insert(log);
       if(flag){
           return 1;
       }
       return 0;
    }


    /**
     *  查询审批日志列表
     * @param pageModelParams
     */
    @Override
    public PageInfo<IntegralApproval> selectIntegralApprovalLog(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
              ew.eq("user_id",user.getUserId());
              ew.eq("status",0);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        Collections.reverse(list);
        return new PageInfo<>(list);
    }

    /**
     *  查询有多少人给自己抄送数量
     */
    @Override
    public Integer selectCsrs(Integer userId) {
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("status",0);
        int num = 0;
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        for (IntegralApproval app : list) {
            if (app.getChaoSongIds()!=null ){
            String[] ids  =  app.getChaoSongIds().split(",");
                for (String id : ids) {
                    if(id.equals(userId+"")){
                        num++;
                    }
                }
            }
        }
        return num;
    }

    /**
     *  查看审批日志列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectApproverLog(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("ti_jiao_id",user.getUserId());
       // ew.eq("status",0);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return  new PageInfo<>(list);

    }

    /**
     *  查询我发起的列表日志 审核通过和不通过的列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectMyFq(Integer pageSize,Integer pageNum,String status,String search) {
        //分页
        PageHelper.startPage(pageNum ,pageSize);
        SysUser user = ShiroUtils.getUserEntity();

        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        if(status.equals("0")){
            ew.like("user_name",search);
            ew.eq("ti_jiao_id",user.getUserId());
            ew.eq("status",0);
        }
        if(status.equals("1")){
            ew.like("user_name",search);
            ew.eq("ti_jiao_id", user.getUserId());
            ew.in("status","1,2");
        }
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        Collections.reverse(list);
        return new PageInfo<>(list);
    }

    /**
     *  我发起的已审核通过和不通过的列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectMyFqYesAndNo(String pageSize,String pageNum) {
        //分页

        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        //通过当前登录用户查询抄送的有多少数据 未审核的
        ew.eq("chao_song_id", user.getUserId());
        ew.in("status","1,2");

        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  抄送我的未审核列表   1，审核通过 2.审核不通过
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectCswdList(String pageSize,String pageNum,String search,String status) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
       SysUser user = ShiroUtils.getUserEntity();
        List<IntegralApproval> list = null;
        //审批未通过列表
       if(status.equals("0")){
           list   = this.baseMapper.selectCswdNot(search,user.getUserId());
       }
       /**1，审核通过 2.审核不通过 */
       if(status.equals("1")){
           list   = this.baseMapper.selectCswdYesAndCheXiao(search,user.getUserId());
       }
        return new PageInfo<>(list);
    }


    /**
     *  积分申诉通过列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectIntegralListYes(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("chao_song_id", user.getUserId());
        ew.eq("status", 1);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  积分申诉审批拒绝列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectIntegralListNo(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("chao_song_id", user.getUserId());
        ew.eq("status", 2);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }


}
