package com.msj.goods.mapper;

import com.msj.goods.entity.SysDept;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    /**
     *  查询所有部门
     * */
    List<Map> selectDeptList();
}
