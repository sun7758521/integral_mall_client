package com.msj.goods.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.msj.goods.entity.IntegralApproval;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 审批管理 Mapper 接口
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
public interface IntegralApprovalMapper extends BaseMapper<IntegralApproval> {

    /**
     * 查询冠军 本部门的积分冠军
     *
     * @return IntegralApproval
     **/
    List<Map> selectGJ(@Param("deptIds") Integer deptIds);

    /**
     * 查询积分榜当日数据
     *
     * @param deptId
     * @param postId
     * @param typeId
     * @param spTime2
     * @return IntegralApproval
     */
    List<Map> selectAllDayData(@Param("deptIds") String deptIds,

                               @Param("postId") String postId, @Param("typeId") String typeId, @Param("spTime1") String spTime1, @Param("spTime2") String spTime2, @Param("search") String search);

    List<Map> selectAllMonthData(@Param("deptIds") String deptIds,

                                 @Param("postId") String postId, @Param("typeId") String typeId, @Param("spTime1") String spTime1, @Param("spTime2") String spTime2, @Param("search") String search);


    List<Map> selectAllJiData(@Param("deptIds") String deptIds,

                              @Param("postId") String postId, @Param("typeId") String typeId, @Param("spTime1") String spTime1, @Param("spTime2") String spTime2, @Param("search") String search);

    List<Map> selectAllYearData(@Param("deptIds") String deptIds,

                                @Param("postId") String postId, @Param("typeId") String typeId, @Param("spTime1") String spTime1, @Param("spTime2") String spTime2, @Param("search") String search);

    /**
     * 抄送我列表
     */
    List<IntegralApproval> selectListCopy(@Param("userId") Integer userId);

    /**
     * 审批人查询未审批的数据
     */
    List<Map> selectAllList(@Param("userId") Integer userId, @Param("deptId") Integer deptId, @Param("search") String search);

    /**
     * 查询审批通过和撤销审批的数据
     */

    List<Map> selectShenPiTongAndCheXiaoList(@Param("userId") Integer userId, @Param("deptId") Integer deptId, @Param("search") String search);

    /**
     * 抄送我的未审核列表
     */
    List<IntegralApproval> selectCswdNot(@Param("search") String search, @Param("userId") Integer userId);

    /**
     * 抄送我的审批通过和撤销审批
     */
    List<IntegralApproval> selectCswdYesAndCheXiao(@Param("search") String search, @Param("userId") Integer userId);

    /**
     * 查询管理员以及超级管理员的排名以及名词
     */
    List<Map> selectAdminGJ(@Param("isApprover") Integer isApprover);

    /**
     * 超级管理员的排名
     */
    List<Map> selectAdminAndSuperGJ();

    /**
     * 普通员工和管理员查询本部门当天数据排名
     */
    List<Map> selectAdminAndEmpDayData(@Param("isApprover") Integer isApprover, @Param("deptIds") String deptIds, @Param("postId") String postId, @Param("typeId") String typeId, @Param("spTime1") String spTime1, @Param("spTime2") String spTime2, @Param("search") String search);

    /**
     * 普通员工和管理员查询本部门当月数据排名
     */
    List<Map> selectAdminAndEmpMonthData(@Param("isApprover") Integer isApprover, @Param("deptIds") String deptIds, @Param("postId") String postId, @Param("typeId") String typeId, @Param("spTime1") String spTime1, @Param("spTime2") String spTime2, @Param("search") String search);

    /**
     * 普通员工和管理员查询本部门当季数据排名
     */
    List<Map> selectAdminAndEmpJiData(@Param("isApprover")Integer isApprover, @Param("deptIds")String deptIds,
                                      @Param("postId")String postId, @Param("typeId")String typeId,
                                      @Param("spTime1")String spTime1, @Param("spTime2")String spTime2,
                                      @Param("search")String search);
    /**
     * 普通员工和管理员查询本部门当年数据排名
     */
    List<Map> selectAdminAndEmpYearData(@Param("isApprover")Integer isApprover, @Param("deptIds")String deptIds,
                                        @Param("postId")String postId, @Param("typeId")String typeId,
                                        @Param("spTime1")String spTime1, @Param("spTime2")String spTime2,
                                        @Param("search")String search);
    /**
     *  超级管理员 查询自己未审批的列表
     */
    List<Map> selectSuperNotApp(@Param("userId")Integer userId,
                                @Param("search")String search);

    /**
     *  超级管理员 查询自己未审批的列表
     */
    List<Map> selectSuperYiShenCheXiao( @Param("userId")Integer userId,
                                       @Param("search")String search);

    /** 超级管理员显示待我审批的数量*/
    Integer selectSuperCount( @Param("userId") Integer userId);

    /** 管理员显示待我审批的数量 */
    Integer selectCountDwsp(@Param("userId")Integer userId,@Param("deptId") Integer deptId);
}
