package com.msj.goods.service;

import com.baomidou.mybatisplus.service.IService;
import com.github.pagehelper.PageInfo;
import com.msj.goods.entity.SysUser;

import java.util.List;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
public interface SysUserService extends IService<SysUser> {

    /**
     *  通过手机号查询当前用户
     *  return user
     */
    SysUser selectPhoneByUser(String phone);

    /**
     *  查询所有的员工
     *  return user
     */
    PageInfo<SysUser> selectAllUser(String pageSize, String pageNum, String search, String deptId, String postId);

    /**
     * 查询部门所用审批人
     */
    List<SysUser> selectApproverPel();

    /**
     *  通过手机号查询当前用户
     *  return user
     */
    SysUser selectPhoneUser(String mobile);

}
