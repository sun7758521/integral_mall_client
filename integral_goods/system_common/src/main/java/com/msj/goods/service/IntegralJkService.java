package com.msj.goods.service;

import com.msj.goods.entity.IntegralJk;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 积分奖扣表 服务类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
public interface IntegralJkService extends IService<IntegralJk> {

}
