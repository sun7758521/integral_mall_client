package com.msj.goods.mapper;

import com.msj.goods.entity.IntegralJk;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 积分奖扣表 Mapper 接口
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
public interface IntegralJkMapper extends BaseMapper<IntegralJk> {

}
