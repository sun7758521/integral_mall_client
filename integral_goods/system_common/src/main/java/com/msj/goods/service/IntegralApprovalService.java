package com.msj.goods.service;

import com.baomidou.mybatisplus.service.IService;
import com.github.pagehelper.PageInfo;
import com.msj.goods.entity.IntegralApproval;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 审批管理 服务类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
public interface IntegralApprovalService extends IService<IntegralApproval> {

    /**
     * 查询冠军
     * @return IntegralApproval
     *
     * */
    List<Map> selectGJ(Integer deptIds);

    /**
     * 查询个人所有的日志
     * @param
     */
    PageInfo<IntegralApproval> selectOneIntegralApproval(String pageSize,String pageNum);

    /**
     *  查询审批日志列表
     * @param  pageSize, pageNum
     */
    PageInfo<IntegralApproval> selectIntegralApprovalLog(String pageSize,String pageNum);

    /**
     *  查询有多少人给自己抄送数量
     */
    Integer selectCsrs(Integer userId);

    /**
     *  查看审批日志列表
     * @param  pageSize, pageNum
     */
    PageInfo<IntegralApproval> selectApproverLog(String pageSize,String pageNum);

    /**
     *  查询我发起的列表日志
     * @param  pageSize, pageNum
     */
    PageInfo<IntegralApproval> selectMyFq(Integer pageSize,Integer pageNum,String status,String search);

    /**
     *  我发起的已审核通过和不通过的列表
     * @param  pageSize, pageNum
     */
    PageInfo<IntegralApproval> selectMyFqYesAndNo(String pageSize,String pageNum);

    /**
     *  抄送我的未审核列表
     * @param  pageSize, pageNum
     */
    PageInfo<IntegralApproval> selectCswdList(String pageSize,String pageNum,String search,String status);

    /**
     *  抄送我的已审核列表
     * @param  pageSize, pageNum
     */
    //PageInfo<IntegralApproval> selectCswdListYes(String pageSize,String pageNum);

    /**
     *  积分申诉通过列表
     * @param  pageSize, pageNum
     */
    PageInfo<IntegralApproval> selectIntegralListYes(String pageSize,String pageNum);

    /**
     *  积分申诉审批拒绝列表
     * @param  pageSize, pageNum
     */
    PageInfo<IntegralApproval> selectIntegralListNo(String pageSize,String pageNum);

    /**
     *  往审批管理添加待审批记录
     */
    boolean insertIntegralApprover(String addIntegral, String spRemark, String typeId, String[] from, String[] to, String[] approvalImg1, String approvalContent, String approvalTitle, String approvalId, String[] apps);

    /**
     *   查询待我审批和已审批列表
     */
    PageInfo<Map> selectApproversPel(Integer pageSize, Integer pageNum,Integer status,String search);

    /**
     *  管理员自由奖扣
     */
    boolean freeIntegral(String addIntegral,String delIntegral, String spRemark, String typeId, String[] from,  String[] approvalImg1, String approvalContent, String approvalTitle);

    /**
     * 自由奖扣申请
     */
    boolean freeIntegralApprover(String addIntegral, String delIntegral, String spRemark, String typeId, String[] from, String[] to, String[] approvalImg1, String approvalContent, String approvalTitle, String approvalId, String[] apps);

    /**
     * 申请  通过、 不通过、撤销、
     */
    int approversYesNo(String approvalId, String status);

    /**
     * 抄送我列表
     */
    PageInfo<IntegralApproval> selectCopyList(String pageSize, String pageNum);

    /**
     * 查询管理员
     */
    List<Map> selectAdminGJ(Integer isApprover);

    /** 超级管理员的排名*/
    List<Map> selectAdminAndSuperGJ();

    /** 超级管理员显示待我审批的数量*/
    Integer selectSuperCount(Integer userId);

    /** 管理员显示待我审批的数量 */
    Integer selectCountDwsp(Integer userId,Integer deptId);

    /**
     *   领导表扬加分  from 给员工申请
     */
    boolean leaderIntegral(String approvalContent, String approvalTitle, String typeId,  String[] approvalImg11, String addIntegral, String spRemark, String[] from);
}
