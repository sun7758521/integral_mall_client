package com.msj.goods.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.web.base.PageModelParams;
import com.msj.goods.entity.SysDept;
import com.msj.goods.entity.SysUser;
import com.msj.goods.entity.SysUserVo;
import com.msj.goods.mapper.SysUserMapper;
import com.msj.goods.service.SysDeptService;
import com.msj.goods.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private SysUserService sysUserService;

    /**
     *  通过手机号查询当前用户
     *  return user
     */
    @Override
    public SysUser selectPhoneByUser(String phone) {
        return sysUserMapper.selectPhoneByUser(phone);
    }

    /**
     *  查询所有的员工
     *  return user
     */
    @Override
    public PageInfo<SysUser> selectAllUser(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        EntityWrapper<SysUser> ew = new EntityWrapper<>();
        ew.eq("del_flag",'0');
        ew.eq("status",'0');
        ew.eq("integral_status","1");
        List<SysUser> userList = sysUserService.selectList(ew);
        for (SysUser user : userList) {
                SysDept dept = sysDeptService.selectById(user.getDeptId());
            user.setDeptName(dept.getDeptName());
        }
        return new PageInfo<>(userList);
    }

    @Override
    public List<SysUser> selectApproverPel() {
        SysUser user =  ShiroUtils.getUserEntity();
        EntityWrapper<SysUser> ew = new EntityWrapper<>();
        //ew.eq("user_id",19);  user.getUserId()
        ew.eq("dept_id",user.getDeptId());
        ew.eq("del_flag","0");
        ew.eq("status","0");
        ew.eq("is_approver","1");
        List<SysUser> userList = sysUserService.selectList(ew);
        return userList;
    }

}
