package com.msj.goods.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.msj.goods.common.constants.JsonResultConstants;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.web.base.JsonResult;
import com.msj.goods.entity.*;
import com.msj.goods.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sun li
 * @Date 2018/11/7 16:01
 * @Description
 */
@RestController
@RequestMapping("/personal")
@Api(description = "个人中心")
public class PersonalController {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private IntegralService integralService;

    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private SysPostService sysPostService;

    @Autowired
    private SysUserPostService sysUserPostService;


    @PostMapping(value = "/indexHead")
    @ApiOperation(value="查询所有部门", notes="展示所有部门数据")
    @ApiImplicitParam(name = "",value = "json对象",required = true)
    public JsonResult indexHead(){
        SysUser user = ShiroUtils.getUserEntity();
        Map<String,Object> map = new HashMap<>();
        map.put("name",user.getUserName());
        map.put("base",user.getJiChuIntegral());
        map.put("avatar",user.getAvatar());
        EntityWrapper<Integral> ew = new EntityWrapper<>();
        ew.eq("user_id",user.getUserId());
        Integral integral = integralService.selectOne(ew);
        if(integral != null){
            map.put("amount",integral.getCountIntegral());
        }
        SysDept dept = sysDeptService.selectById(user.getDeptId());
        if(dept !=null){
            map.put("deptName",dept.getDeptName());
        }

        EntityWrapper<SysUserPost> ew1 = new EntityWrapper<>();
        ew1.eq("user_id",user.getUserId());
        SysUserPost userPost = sysUserPostService.selectOne(ew1);
        if(userPost !=null){
            SysPost post = sysPostService.selectById(userPost.getPostId());
            map.put("postName",post.getPostName());
        }


        return JsonResult.success(map , JsonResultConstants.SUCCESS);
    }
    /**======= todo===================================== */
    @PostMapping(value = "/indexHeadBaseIntegral")
    @ApiOperation(value="个人基础积分", notes="个人基础积分信息")
    @ApiImplicitParam(name = "",value = "json对象",required = true)
    public JsonResult indexHeadBase(){
        SysUser user = ShiroUtils.getUserEntity();
        Map<String,Object> map = new HashMap<>();
        SysUserPost userPost = sysUserPostService.selectById(user.getUserId());
        SysPost post = sysPostService.selectById(userPost.getPostId());
        map.put("postIntegral",post.getIntegral());
        map.put("base",user.getJiChuIntegral());
        map.put("avatar",user.getAvatar());
        map.put("userName",user.getUserName());
        map.put("lengthService","");
        map.put("schooling","");
        map.put("theTitle","");
        map.put("honor","");
        map.put("specialty","");
        return JsonResult.success(map, JsonResultConstants.SUCCESS);
    }

    @PostMapping(value = "/indexEcharts")
    @ApiOperation(value="查询个人的Echarts", notes="查询个人积分Echarts")
    @ApiImplicitParam(name = "",value = "json对象",required = true)
    public JsonResult indexEcharts(){
        Map<String,Object> map = new HashMap<>();
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<Integral> ew = new EntityWrapper<>();
        ew.eq("user_id",user.getUserId() );
        Integral integral  =  integralService.selectOne(ew);
        if(integral != null){
            map.put("addIntegral", integral.getAddIntegral());
            map.put("countIntegral", integral.getCountIntegral());
            map.put("delIntegral", integral.getDelIntegral());
            map.put("baseIntegral",user.getJiChuIntegral());
        }

        return JsonResult.success(map , JsonResultConstants.SUCCESS);
    }
}
