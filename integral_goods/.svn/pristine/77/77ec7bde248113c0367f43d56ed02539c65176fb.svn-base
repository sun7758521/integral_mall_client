package com.msj.goods.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.constants.UserConstants;
import com.msj.goods.common.utils.DateUtils;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.utils.StringUtils;
import com.msj.goods.entity.*;
import com.msj.goods.mapper.IntegralApprovalMapper;
import com.msj.goods.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 审批管理 服务实现类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
@Service
public class IntegralApprovalServiceImpl extends ServiceImpl<IntegralApprovalMapper, IntegralApproval> implements IntegralApprovalService {

    @Autowired
    private IntegralApprovalMapper integralApprovalMapper;

    @Autowired
    private IntegralApprovalService integralApprovalService;
    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private IntegralLogService integralLogService;

    @Autowired
    private IntegralService integralService;

    @Autowired
    private SysPostService sysPostService;

    @Autowired
    private SysUserPostService  sysUserPostService;
    @Autowired
    private SysDeptService sysDeptService;
    /**
     * 查询冠军
     * @return IntegralApproval
     *
     * @param */
    @Override
    public List<Map> selectGJ(Integer deptIds) {
        return integralApprovalMapper.selectGJ(deptIds);
    }

    @Override
    public PageInfo<IntegralApproval> selectOneIntegralApproval(String pageSize,String pageNum) {
       SysUser user = ShiroUtils.getUserEntity();
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("user_phone",user.getPhonenumber());
        ew.eq("user_name",user.getUserName());
       // ew.eq("status","1");
        List<IntegralApproval>  logs = integralApprovalMapper.selectList(ew);
        Collections.reverse(logs);
        return   new PageInfo<>(logs);

    }

    @Override
    public boolean insertIntegralApprover(String addIntegral, String spRemark, String typeId, String[] from, String[] to, String[] approvalImg1, String approvalContent, String approvalTitle, String approvalId, String[] apps) {
        IntegralApproval integralApproval = new IntegralApproval();
        //所有提交人的ids

         SysUser sysUser = ShiroUtils.getUserEntity();
        for (String id : from) {
            SysUser user =  sysUserService.selectById(id);
            integralApproval.setApprovalNum("JFB"+ DateUtils.dateTime());
            integralApproval.setSqTime(DateUtils.getNowDate());
            integralApproval.setSqIntegral(Integer.parseInt(addIntegral) );
            integralApproval.setIntegralTypeId(Integer.parseInt(approvalId));
            integralApproval.setApprovalContent(approvalContent);
            integralApproval.setApprovalTitle(approvalTitle);
            integralApproval.setApprovalImg1(Arrays.toString(approvalImg1));
            integralApproval.setSpRemark(spRemark);
            integralApproval.setTypeId(Integer.parseInt(typeId));
            integralApproval.setUserImg(user.getAvatar());
            integralApproval.setUserId(user.getUserId());
            integralApproval.setUserName(user.getUserName());
            /**  添加用户部门id */
            integralApproval.setUserDeptId(user.getDeptId());
            integralApproval.setUserDept(sysDeptService.selectById(user.getDeptId()).getDeptName());
            /**  通过用户id 查询职位添加职位id */
            EntityWrapper<SysUserPost> ew = new EntityWrapper<SysUserPost>();
            ew.eq("user_id",user.getUserId());
            SysUserPost userPost =  sysUserPostService.selectOne(ew);
            integralApproval.setUserPostId(userPost.getPostId());
            /** 提交人的信息 */
            integralApproval.setTiJiaoId(sysUser.getUserId());
            integralApproval.setTiJiaoName(sysUser.getUserName());
            integralApproval.setTiJiaoNameImg(sysUser.getAvatar());
            integralApproval.setUserPhone(Long.parseLong(user.getPhonenumber()));
            /**  审批人 ids  默认是自己 */
            integralApproval.setShenPiRenIds(StringUtils.join(from, ","));
            /**  把数组转成字符串保存到数据库 */
            integralApproval.setShenPiRenIds(StringUtils.join(apps, ","));
           /* for (String chaoId : to) {
                integralApproval.setChaoSongId(Long.parseLong(chaoId));
            }*/
           /**  把 数组转成字符串保存到数据库 */
            integralApproval.setChaoSongIds(StringUtils.join(to, ","));

            /** 正常状态 0 */
            integralApproval.setStatus(Integer.parseInt(UserConstants.NORMAL));
            int  i = this.baseMapper.insert(integralApproval);
            if (i>0){
                return true;
            }
        }
        return false;
    }

    /**
     *   查询待审和已审批列表
     */
    @Override
    public PageInfo<IntegralApproval> selectApproversPel(Integer pageSize, Integer pageNum,Integer status) {
     SysUser user =  ShiroUtils.getUserEntity();
     // 分页
     PageHelper.startPage(pageNum,pageSize);
        List<IntegralApproval> list = null;
     if (user.getIsApprover()==1 && status==0){
         EntityWrapper<IntegralApproval> ew = new EntityWrapper<IntegralApproval>();
         ew.eq("user_id", user.getUserId());
         ew.eq("status",0);
         list  = this.baseMapper.selectList(ew);
     }else if(user.getIsApprover()==1 && status==1){
         EntityWrapper<IntegralApproval> ew1 = new EntityWrapper<IntegralApproval>();
         ew1.eq("user_id", user.getUserId());
         ew1.in("status","1,2");
          list = this.baseMapper.selectList(ew1);
     } else{
         list = new ArrayList<>();
     }
        Collections.reverse(list);
        return  new PageInfo<>(list);
    }

    /**
     *  approvalImg1 上传的图片没有用到
     *  管理员自由奖扣
     */
    @Override
    public boolean freeIntegral(String addIntegral,String delIntegral, String spRemark, String typeId, String[] from, String[] approvalImg1, String approvalContent, String approvalTitle) {
        for (String id : from) {
            insertApproval(addIntegral,delIntegral,spRemark,typeId,approvalContent,approvalTitle,approvalImg1);
            insertLog(addIntegral,delIntegral,spRemark,typeId,approvalContent,approvalTitle);
            Integer i = updateIIntegral(id,addIntegral,delIntegral,typeId);
            if(i>0){
                return true;
            }
        }
        return false;
    }



    /**
     * 自由奖扣申请
     */
    @Override
    public boolean freeIntegralApprover(String addIntegral, String delIntegral, String spRemark, String typeId, String[] from, String[] to, String[] approvalImg1, String approvalContent, String approvalTitle, String approvalId, String[] apps) {

        for (String id : from) {
            insertIntegralApproval(addIntegral,delIntegral,spRemark,typeId,approvalContent,approvalTitle,approvalId,approvalImg1,apps,to);
        }
        return false;
    }

    /**
     * 申请  1通过、 2不通过、3撤销、
     */
    @Override
    public int approversYesNo(String approvalId, String status) {
        IntegralApproval approval = this.baseMapper.selectById(approvalId);
        int i = 0;
         if (status.equalsIgnoreCase("1")){
            approval.setStatus(1);
            approval.setSpTime(DateUtils.getNowDate());
            i =  this.baseMapper.updateById(approval);
        }
        if (status.equalsIgnoreCase("2")){
            approval.setStatus(2);
            approval.setSpTime(DateUtils.getNowDate());
            i = this.baseMapper.updateById(approval);
        }
        if (status.equalsIgnoreCase("3")){
            approval.setStatus(3);
            approval.setSpTime(DateUtils.getNowDate());
            i = this.baseMapper.updateById(approval);
        }
        return i;
    }

    /**
     * 抄送我列表
     */
    @Override
    public PageInfo<IntegralApproval> selectCopyList(String pageSize, String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
         SysUser sysUser =  ShiroUtils.getUserEntity();
        List<IntegralApproval> list = this.baseMapper.selectListCopy(sysUser.getUserId());
        return new PageInfo<>(list);
    }


    /**
     *  自由奖扣添加申批记录
     */
    private void insertIntegralApproval(String addIntegral, String delIntegral, String spRemark, String typeId, String approvalContent, String approvalTitle, String approvalId, String[] approvalImg1, String[] apps, String[] to) {
        SysUser sysUser =  ShiroUtils.getUserEntity();

        IntegralApproval approval = new IntegralApproval();
        approval.setApprovalNum("JFB"+DateUtils.getNowDate());
        approval.setkIntegral(Integer.parseInt(delIntegral));
        approval.setAddIntegral(Long.parseLong(addIntegral));
        approval.setSpRemark(spRemark);
        approval.setTypeId(Integer.parseInt(typeId));
        approval.setApprovalTitle(approvalTitle);
        approval.setApprovalContent(approvalContent);
        approval.setIntegralTypeId(Integer.parseInt(approvalId));
        approval.setTiJiaoId(sysUser.getUserId());
        approval.setTiJiaoNameImg(sysUser.getAvatar());
        approval.setTiJiaoName(sysUser.getUserName());
        approval.setStatus(0);
        approval.setChaoSongIds(StringUtils.join(to, ","));
        approval.setChaoSongIds(StringUtils.join(apps, ","));
        approval.setApprovalImg1(StringUtils.join(approvalImg1, ","));
    }

    /**
     * 添加申批记录里
     */
    private void insertApproval(String addIntegral, String delIntegral, String spRemark, String typeId, String approvalContent, String approvalTitle, String[] approvalImg1) {
        SysUser sysUser =  ShiroUtils.getUserEntity();
        IntegralApproval approval = new IntegralApproval();
        approval.setApprovalNum("JFB"+DateUtils.getNowDate());
        approval.setkIntegral(Integer.parseInt(delIntegral));
        approval.setAddIntegral(Long.parseLong(addIntegral));
        approval.setSpRemark(spRemark);
        approval.setTypeId(Integer.parseInt(typeId));
        approval.setApprovalTitle(approvalTitle);
        approval.setApprovalContent(approvalContent);
        approval.setTiJiaoId(sysUser.getUserId());
        approval.setTiJiaoNameImg(sysUser.getAvatar());
        approval.setTiJiaoName(sysUser.getUserName());
        approval.setApprovalImg1(StringUtils.join(approvalImg1, ","));
    }
    /**
     *  添加管理员奖扣 日志
     *
     * @param addIntegral
     * @param delIntegral
     * @param id
     * @param typeId
     */
    private Integer updateIIntegral(String id, String addIntegral, String delIntegral,String typeId) {
        EntityWrapper<Integral> ew = new EntityWrapper<Integral> ();
        ew.eq("user_id",id);
        Integral integral =  integralService.selectOne(ew);
        if(addIntegral != null && addIntegral !="0"){
            integral.setCountIntegral(integral.getCountIntegral() + Integer.parseInt(addIntegral));
            integral.setAddIntegral(integral.getAddIntegral() + Integer.parseInt(addIntegral));
        }
        if(delIntegral != null && delIntegral !="0"){
            integral.setCountIntegral(integral.getCountIntegral() - Integer.parseInt(delIntegral));
            integral.setDelIntegral(integral.getDelIntegral() + Integer.parseInt(delIntegral));
        }
      boolean flag = integralService.updateById(integral);
        if(flag){
           return 1;
        }
        return 0;
    }

    /**
     *  添加管理员奖扣 日志
     *
     * @param addIntegral
     * @param delIntegral
     * @param spRemark
     * @param typeId
     * @param approvalContent
     * @param approvalTitle
     */
    private void insertLog(String addIntegral, String delIntegral, String spRemark, String typeId, String approvalContent, String approvalTitle) {
        IntegralLog log = new IntegralLog();
        log.setApprovalNum("JFB"+DateUtils.getNowDate());
        log.setBianIntegral(Integer.parseInt(addIntegral));
        log.setkIntegral(Integer.parseInt(delIntegral));
        log.setRemark(spRemark);
        log.setIntegralTitle(approvalTitle);
        log.setIntegralContent(approvalContent);
        log.setTypeId(Integer.parseInt(typeId));
        log.setGetTime(DateUtils.getNowDate());
        /** 代表领导奖励的日志 */
        log.setStatus(2);
    }


    /**
     *  查询审批日志列表
     * @param pageModelParams
     */
    @Override
    public PageInfo<IntegralApproval> selectIntegralApprovalLog(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
              ew.eq("user_id",user.getUserId());
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        Collections.reverse(list);
        return new PageInfo<>(list);
    }

    /**
     *  查询有多少人给自己抄送数量
     */
    @Override
    public Integer selectCsrs(Integer userId) {
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        int num = 0;
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        for (IntegralApproval app : list) {
            if (app.getChaoSongIds()!=null ){
            String[] ids  =  app.getChaoSongIds().split(",");
                for (String id : ids) {
                    if(id.equals(userId+"")){
                        num++;
                    }
                }
            }
        }
        return num;
    }

    /**
     *  查看审批日志列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectApproverLog(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("ti_jiao_id",user.getUserId());
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return  new PageInfo<>(list);

    }

    /**
     *  查询我发起的列表日志
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectMyFq(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("ti_jiao_id",user.getUserId());
        ew.eq("status",0);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  我发起的已审核通过和不通过的列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectMyFqYesAndNo(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        //通过当前登录用户查询抄送的有多少数据 未审核的
        ew.eq("chao_song_id", user.getUserId());
        ew.in("status","1,2");
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  抄送我的未审核列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectCswdList(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
       SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        //通过当前登录用户查询抄送字机的有多少数据 未审核的
        ew.eq("chao_song_id",user.getUserId());
        ew.eq("status",0);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  抄送我的已审核列表
     *  1，审核通过 2.审核不通过
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectCswdListYes(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("chao_song_id", user.getUserId());
        ew.in("status","1,2");
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  积分申诉通过列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectIntegralListYes(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("chao_song_id", user.getUserId());
        ew.eq("status", 1);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  积分申诉审批拒绝列表
     * @param list
     */
    @Override
    public PageInfo<IntegralApproval> selectIntegralListNo(String pageSize,String pageNum) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("chao_song_id", user.getUserId());
        ew.eq("status", 2);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }


}
