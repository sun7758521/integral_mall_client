package com.msj.goods.controller;

import com.github.pagehelper.PageInfo;
import com.msj.goods.common.annotation.Log;
import com.msj.goods.common.constants.JsonResultConstants;
import com.msj.goods.common.web.base.JsonResult;
import com.msj.goods.service.IntegralService;
import com.msj.goods.service.IntegralTypeService;
import com.msj.goods.service.SysDeptService;
import com.msj.goods.service.SysPostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author sun li
 * @Date 2018/11/7 8:50
 * @Description
 */
@RestController
@RequestMapping("/rank")
@Api(description = "积分榜")
public class IntegralBangController {

    @Autowired
    private IntegralService integralService;

    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private SysPostService sysPostService;

    @Autowired
    private IntegralTypeService integralTypeService;

    @PostMapping(value = "/index")
    @Log(title = "积分榜首页")
    @ApiOperation(value="积分榜首页", notes="积分榜展示数据")
    @ApiImplicitParam(name = "pageModelParams",value = "json对象",required = true)
    public JsonResult index(@RequestParam("pageSize") Integer pageSize, @RequestParam("pageNum") Integer pageNum,
                            @RequestParam(value = "times",required=false) Integer times, @RequestParam(value = "deptId", required=false) String deptId,
                            @RequestParam(value = "postId" ,required=false) String postId, @RequestParam(value ="typeId", required=false) String typeId,
                            @RequestParam(value = "spTime1" ,required=false) String spTime1, @RequestParam(value ="spTime2", required=false) String spTime2,
                            @RequestParam(value = "search" ,required=false) String search){
        PageInfo<Map> pageInfo = integralService.selectAllList(pageSize,pageNum,times,deptId,postId,typeId,spTime1,spTime2,search);
        return JsonResult.success(pageInfo  , JsonResultConstants.SUCCESS);
    }

    @PostMapping(value = "/selectDept")
    @ApiOperation(value="查询所有部门", notes="展示所有部门数据")
    @ApiImplicitParam(name = "",value = "json对象",required = true)
    public JsonResult selectDept(){
        List<Map> dept =  sysDeptService.selectDeptList();
        return JsonResult.success(dept , JsonResultConstants.SUCCESS);
    }

    @PostMapping(value = "/selectPost")
    @ApiOperation(value="查询所有职位", notes="展示所有职位数据")
    @ApiImplicitParam(name = "",value = "json对象",required = true)
    public JsonResult selectPost(){
        List<Map>  post =  sysPostService.selectPostList();
        return JsonResult.success(post , JsonResultConstants.SUCCESS);
    }

    @PostMapping(value = "/selectType")
    @ApiOperation(value="查询所有积分类型", notes="展示所有积分类型数据")
    @ApiImplicitParam(name = "",value = "json对象",required = true)
    public JsonResult selectType(){
        List<Map>  typeList =  integralTypeService.selectIntegralTypeList();
        return JsonResult.success(typeList , JsonResultConstants.SUCCESS);
    }
}
