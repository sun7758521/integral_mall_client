package com.msj.goods.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.constants.UserConstants;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.utils.StringUtils;
import com.msj.goods.entity.SysDept;
import com.msj.goods.entity.SysUser;
import com.msj.goods.entity.SysUserPost;
import com.msj.goods.mapper.SysUserMapper;
import com.msj.goods.service.SysDeptService;
import com.msj.goods.service.SysUserPostService;
import com.msj.goods.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysUserPostService sysUserPostService;

    /**
     *  通过手机号查询当前用户
     *  return user
     */
    @Override
    public SysUser selectPhoneByUser(String phone) {
        return sysUserMapper.selectPhoneByUser(phone);
    }

    /**
     *  查询所有的员工
     *  return user
     */
    @Override
    public PageInfo<SysUser> selectAllUser(String pageSize, String pageNum, String search, String deptId, String postId) {
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum) ,Integer.parseInt(pageSize));
        EntityWrapper<SysUser> ew = new EntityWrapper<>();
        ew.eq("del_flag",0);
        ew.eq("status",0);

        if(StringUtils.isNotNull(search)){
            ew.like("user_name",search);
        }
        if(StringUtils.isNotNull(deptId)){
            ew.like("dept_id",deptId);
        }
        List<SysUser> userList = this.baseMapper.selectList(ew);
        for (SysUser user : userList) {
            SysDept dept = sysDeptService.selectById(user.getDeptId());
            if(dept !=null){
                user.setDeptName(dept.getDeptName());
            }

            if(StringUtils.isNotNull(postId)){
                SysUserPost userPost =  sysUserPostService.selectById(user.getUserId());
                if(userPost !=null){
                    ew.eq("post_id",postId);
                }

            }
        }

        return new PageInfo<>(userList);
    }

    @Override
    public List<SysUser> selectApproverPel() {
        SysUser user =  ShiroUtils.getUserEntity();
        EntityWrapper<SysUser> ew = new EntityWrapper<>();
        if (user.getIsApprover()== UserConstants.ADMIN){
            /*ew.eq("dept_id",user.getDeptId());*/
            ew.eq("del_flag","0");
            ew.eq("status","0");
            ew.eq("is_approver",2);
        }
        if (user.getIsApprover()== UserConstants.SUPER_ADMIN){

            ew.eq("del_flag","0");
            ew.eq("status","0");
            ew.eq("is_approver",2);
        }
        if(user.getIsApprover()== UserConstants.COMMON){
            ew.eq("dept_id",user.getDeptId());
            ew.eq("del_flag","0");
            ew.eq("status","0");
            ew.eq("is_approver",1);
        }
        List<SysUser> userList = sysUserService.selectList(ew);
        return userList;
    }

    /**
     *  通过手机号查询当前用户
     *  return user
     */
    @Override
    public SysUser selectPhoneUser(String mobile) {
        EntityWrapper<SysUser> ew = new EntityWrapper<>();
        ew.eq("phonenumber",mobile );
       SysUser userList = sysUserService.selectOne(ew);
        return userList;
    }

}
