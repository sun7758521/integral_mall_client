package com.msj.goods.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.annotation.Log;
import com.msj.goods.common.constants.JsonResultConstants;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.web.base.JsonResult;
import com.msj.goods.common.web.base.PageModelParams;
import com.msj.goods.entity.*;
import com.msj.goods.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author sun li
 * @Date 2018/11/8 9:29
 * @Description
 */
@RestController
@RequestMapping("/workbench")
@Api(description = "工作台")
public class WorkbenchController {


    @Autowired
    private GzdService gzdService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private XwIntegralService xwIntegralService;

    @Autowired
    private PdIntegralService pdIntegralService;

    @Autowired
    private YjIntegralService yjIntegralService;

    @PostMapping(value = "/index")
    @ApiOperation(value="展示工作台所有的数据", notes="所有的数据")
    @ApiImplicitParam(name = "",value = "json对象",required = true)
    public JsonResult selectWorkbench(){
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<SysUserRole> ew1 = new EntityWrapper<>();
        ew1.eq("user_id",user.getUserId());
        SysUserRole userRole = sysUserRoleService.selectOne(ew1);
        SysRole role =  sysRoleService.selectById(userRole.getRoleId());
        EntityWrapper<Gzd> ew = new EntityWrapper<>();
        List<Gzd> gzt = null;

        if(role.getRoleName().equalsIgnoreCase("管理员")){
            gzt  =  gzdService.selectList(ew);
        }else if(gzdService.selectOne(ew).getYyType().contains("员工应用")){
            ew.eq("yy_type","员工应用");
            gzt = gzdService.selectList(ew);
        }else{
            JsonResult.failure(JsonResultConstants.FAIL);
        }
        return JsonResult.success(gzt , JsonResultConstants.SUCCESS);
    }

    @PostMapping(value = "/declareBehavior")
    @Log(title = "行为积分")
    @ApiOperation(value="行为积分", notes="查询行为积分项")
    @ApiImplicitParam(name = "pageModelParams",value = "json对象",required = true)
    public JsonResult declareBehavior(@RequestBody PageModelParams pageModelParams){
        PageInfo<XwIntegral> pageInfo = xwIntegralService.selectDeptIntegral(pageModelParams);
        return JsonResult.success(pageInfo , JsonResultConstants.SUCCESS);
    }

    @GetMapping(value = "/declareBehaviorDetail/{behaviorId}")
    @Log(title = "行为积分详情页")
    @ApiOperation(value = "行为积分详情页")
    @ApiImplicitParam(name = "behaviorId", value = "行为积分唯一标识", required = true)
    public JsonResult declareBehaviorDetail(@PathVariable("behaviorId") String behaviorId) {
        return JsonResult.success(xwIntegralService.selectById(behaviorId) , JsonResultConstants.SUCCESS);
    }

    @PostMapping(value = "/declareMoral")
    @Log(title = "品德积分")
    @ApiOperation(value="品德积分", notes="查询品德积分项")
    @ApiImplicitParam(name = "pageModelParams",value = "json对象",required = true)
    public JsonResult declareMoral(@RequestBody PageModelParams pageModelParams){
        PageInfo<PdIntegral> pageInfo = pdIntegralService.selectDeclareMoral(pageModelParams);
        return JsonResult.success(pageInfo , JsonResultConstants.SUCCESS);
    }

    @PostMapping(value = "/declareResults")
    @Log(title = "业绩积分")
    @ApiOperation(value="业绩积分", notes="查询业绩积分项")
    @ApiImplicitParam(name = "pageModelParams",value = "json对象",required = true)
    public JsonResult declareResults(@RequestBody PageModelParams pageModelParams){
        PageInfo<YjIntegral> pageInfo = yjIntegralService.selectDeclareResults(pageModelParams);
        return JsonResult.success(pageInfo , JsonResultConstants.SUCCESS);
    }
}
