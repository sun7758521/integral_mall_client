package com.msj.goods.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.msj.goods.common.constants.UserConstants;
import com.msj.goods.common.utils.DateUtils;
import com.msj.goods.common.utils.ShiroUtils;
import com.msj.goods.common.web.base.PageModelParams;
import com.msj.goods.entity.IntegralApproval;
import com.msj.goods.entity.SysUser;
import com.msj.goods.mapper.IntegralApprovalMapper;
import com.msj.goods.service.IntegralApprovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 审批管理 服务实现类
 * </p>
 *
 * @author sun li
 * @since 2018-11-05
 */
@Service
public class IntegralApprovalServiceImpl extends ServiceImpl<IntegralApprovalMapper, IntegralApproval> implements IntegralApprovalService {

    @Autowired
    private IntegralApprovalMapper integralApprovalMapper;

    /**
     * 查询冠军
     * @return IntegralApproval
     *
     * @param */
    @Override
    public List<Map> selectGJ() {


        return integralApprovalMapper.selectGJ();
    }

    @Override
    public PageInfo<IntegralApproval> selectOneIntegralApproval(PageModelParams params) {
        SysUser user = ShiroUtils.getUserEntity();
        //分页
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("user_phone",user.getPhonenumber());
        ew.eq("user_name",user.getUserName());
        ew.eq("status","1");
        List<IntegralApproval>  logs = integralApprovalMapper.selectList(ew);
        return new PageInfo<>(logs);

    }

    @Override
    public boolean insertIntegralApprover(Map<String,String> map) {
        IntegralApproval integralApproval = new IntegralApproval();
        integralApproval.setApprovalNum("JFB"+ DateUtils.dateTime());
        integralApproval.setAddIntegral(Long.parseLong(map.get("addIntegral")));
        integralApproval.setSqTime(DateUtils.getNowDate());
        integralApproval.setIntegralTypeId(Integer.parseInt(map.get("behaviorId")));
        integralApproval.setApprovalContent(map.get("behaviorContent"));
        integralApproval.setApprovalTitle(map.get("behaviorTitle"));
        integralApproval.setApprovalImg1(map.get("approvalImg1"));
        integralApproval.setApprovalImg2(map.get("approvalImg2"));
        integralApproval.setApprovalImg3(map.get("approvalImg3"));
        integralApproval.setApprovalImg4(map.get("approvalImg4"));
        integralApproval.setApprovalImg5(map.get("approvalImg5"));
        integralApproval.setApprovalImg6(map.get("approvalImg6"));
        integralApproval.setApprovalImg7(map.get("approvalImg7"));
        integralApproval.setApprovalImg8(map.get("approvalImg8"));
        integralApproval.setApprovalImg9(map.get("approvalImg9"));
        integralApproval.setSpRemark(map.get("spRemark"));
        integralApproval.setUserDept(map.get("deptName"));
        integralApproval.setUserDeptId(Integer.parseInt(map.get("deptId")));
        /*integralApproval.setUserPostId(Integer.parseInt(map.get("postId")));
        integralApproval.setUserPost(map.get("postName"));*/
        integralApproval.setTypeId(Integer.parseInt(map.get("typeId")));
        integralApproval.setChaoSongId(Long.parseLong(map.get("chaoSongId")));
        integralApproval.setChaoSongName(map.get("chaoSongName"));
        integralApproval.setChaoSongImg(map.get("chaoSongNameImg"));
        integralApproval.setChaoSongDeptName(map.get("chaoSongDeptName"));
        integralApproval.setChaoSongDeptId(map.get("chaoSongDeptId"));
        SysUser user =  ShiroUtils.getUserEntity();
        // 获得积分的人名称展示
        integralApproval.setUserId(Integer.parseInt(map.get("userId")));
        integralApproval.setUserName(map.get("userName"));
        integralApproval.setUserImg(map.get("userImg"));
        // 提交积分的人名称展示
        integralApproval.setTiJiaoName(user.getUserName());
        integralApproval.setTiJiaoNameImg(user.getAvatar());
        integralApproval.setTiJiaoId(user.getUserId().intValue());
        /** 正常状态 0 */
        integralApproval.setStatus(Integer.parseInt(UserConstants.NORMAL));
         int  i = this.baseMapper.insert(integralApproval);
         if (i>0){
             return true;
         }
        return false;
    }

    /**
     *  查询审批日志列表
     */
    @Override
    public PageInfo<IntegralApproval> selectIntegralApprovalLog() {
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
              ew.eq("ti_jiao_id",user.getUserId());
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  查询有多少人给自己抄送数量
     */
    @Override
    public Integer selectCsrs(Integer userId) {
        return this.baseMapper.selectCsrs(userId);
    }

    /**
     *  查看审批日志列表
     */
    @Override
    public PageInfo<IntegralApproval> selectApproverLog() {

        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("ti_jiao_id",user.getUserId());
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return  new PageInfo<>(list);

    }
    /**
     *  查询我发起的列表日志
     */
    @Override
    public PageInfo<IntegralApproval> selectMyFq() {
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("ti_jiao_id",user.getUserId());
        ew.eq("status",0);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  我发起的已审核通过和不通过的列表
     */
    @Override
    public PageInfo<IntegralApproval> selectMyFqYesAndNo() {
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("user_id", user.getUserId());
        ew.in("status","1,2");
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  抄送我的未审核列表
     */
    @Override
    public PageInfo<IntegralApproval> selectCswdList() {
       SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        //通过当前登录用户查询抄送字机的有多少数据 未审核的
        ew.eq("chao_song_id",user.getUserId());
        ew.eq("status",0);
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }

    /**
     *  抄送我的已审核列表
     *  1，审核通过 2.审核不通过
     */
    @Override
    public PageInfo<IntegralApproval> selectCswdListYes() {
        SysUser user = ShiroUtils.getUserEntity();
        EntityWrapper<IntegralApproval> ew = new EntityWrapper<>();
        ew.eq("chao_song_id", user.getUserId());
        ew.in("status","1,2");
        List<IntegralApproval> list = this.baseMapper.selectList(ew);
        return new PageInfo<>(list);
    }
}
