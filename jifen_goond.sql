/*
Navicat MySQL Data Transfer

Source Server         : 192.168.0.101
Source Server Version : 50561
Source Host           : 192.168.0.101:3306
Source Database       : ry

Target Server Type    : MYSQL
Target Server Version : 50561
File Encoding         : 65001

Date: 2018-12-08 11:51:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gg_table
-- ----------------------------
DROP TABLE IF EXISTS `gg_table`;
CREATE TABLE `gg_table` (
  `g_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '广告id',
  `g_name` varchar(20) DEFAULT NULL COMMENT '广告名称',
  `g_imgs` varchar(255) DEFAULT '' COMMENT '广告图片',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`g_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='广告图片';

-- ----------------------------
-- Records of gg_table
-- ----------------------------
INSERT INTO `gg_table` VALUES ('1', '首页', '/advertising/qiye.jpg', null);
INSERT INTO `gg_table` VALUES ('2', '工作站', '/advertising/jifen.png', null);
INSERT INTO `gg_table` VALUES ('3', '积分商场', '/advertising/qiye.jpg', null);

-- ----------------------------
-- Table structure for gzd
-- ----------------------------
DROP TABLE IF EXISTS `gzd`;
CREATE TABLE `gzd` (
  `gzt_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '工作台主键',
  `yy_img` varchar(255) DEFAULT '' COMMENT '应用图标',
  `yy_title` varchar(20) DEFAULT '' COMMENT '应用标题',
  `yy_type` varchar(20) DEFAULT NULL COMMENT '应用管理',
  `status` int(255) DEFAULT '0' COMMENT '状态,（0使用中 1 停用  2开启抽奖，3管理愿望分 4爱心个数）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(50) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`gzt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='工作台应用管理';

-- ----------------------------
-- Records of gzd
-- ----------------------------
INSERT INTO `gzd` VALUES ('1', '/work/image/city.png', '考勤', '员工应用', '0', '2018-10-20 09:28:31', '2018-10-20 09:28:35', '');
INSERT INTO `gzd` VALUES ('2', '/work/image/page.png', '工作日志', '员工应用', '0', '2018-10-20 09:32:56', '2018-10-20 09:33:01', '');
INSERT INTO `gzd` VALUES ('3', '/work/image/task2.png', '悬赏任务', '员工应用', '0', '2018-10-20 09:33:05', '2018-10-20 09:33:08', '');
INSERT INTO `gzd` VALUES ('4', '/work/image/test.png', '	\r\n申报积分', '员工应用', '0', '2018-10-20 09:33:11', '2018-10-20 09:33:14', '');
INSERT INTO `gzd` VALUES ('5', '/work/image/right.png', '领导表扬', '	\r\n管理员应用', '0', '2018-10-20 09:33:18', '2018-10-20 09:33:22', '');
INSERT INTO `gzd` VALUES ('6', '/work/image/to.png', '爱心点赞', '	\r\n员工应用', '0', '2018-10-20 09:33:25', '2018-10-20 09:33:30', '');
INSERT INTO `gzd` VALUES ('7', '/work/image/test.png', '积分申诉', '员工应用', '0', '2018-10-20 09:33:33', '2018-10-20 09:33:36', '');
INSERT INTO `gzd` VALUES ('8', '/work/image/free.png', '自由奖扣', '员工应用', '0', '2018-10-20 09:33:40', '2018-10-20 09:34:10', '');
INSERT INTO `gzd` VALUES ('9', '/work/image/question.png', '水平考核', '员工应用', '0', '2018-10-20 09:33:47', '2018-10-20 09:34:16', '');
INSERT INTO `gzd` VALUES ('10', '/work/image/app.png', '经营哲学', '员工应用', '0', '2018-10-20 09:33:43', '2018-10-20 09:34:13', '');
INSERT INTO `gzd` VALUES ('11', '/work/image/mall.png', '积分商城', '员工应用', '0', '2018-10-20 09:33:50', '2018-10-20 09:34:19', '');
INSERT INTO `gzd` VALUES ('12', '/work/image/gift.png', '积分抽奖', '员工应用', '0', '2018-10-20 09:33:54', '2018-10-20 09:34:22', '');
INSERT INTO `gzd` VALUES ('13', '/work/image/call.png', '公告', '员工应用', '0', '2018-10-20 09:33:57', '2018-10-20 09:34:25', '');
INSERT INTO `gzd` VALUES ('14', '/work/image/number3.png', '发布任务', '管理员应用', '0', '2018-10-20 09:34:01', '2018-10-20 09:34:28', '');
INSERT INTO `gzd` VALUES ('15', '/work/image/frame.png', '管理奖扣', '管理员应用', '0', '2018-10-20 09:34:06', '2018-10-20 09:34:35', '');
INSERT INTO `gzd` VALUES ('16', '/work/image/app.png', '发布公告', '管理员应用', '0', '2018-10-20 09:34:04', '2018-10-20 09:34:32', '');

-- ----------------------------
-- Table structure for integral
-- ----------------------------
DROP TABLE IF EXISTS `integral`;
CREATE TABLE `integral` (
  `integral_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '积分主键',
  `user_id` int(11) DEFAULT NULL COMMENT '关联那个员工',
  `user_name` varchar(10) DEFAULT NULL COMMENT '员工姓名',
  `user_phone` varchar(11) DEFAULT NULL COMMENT '员工手机号',
  `count_integral` int(20) DEFAULT '0' COMMENT '总积分',
  `del_integral` int(200) DEFAULT '0' COMMENT '扣除积分',
  `add_integral` int(200) DEFAULT '0' COMMENT '奖励积分',
  `type_id` int(2) DEFAULT NULL COMMENT '类型',
  `post_id` int(11) DEFAULT NULL COMMENT '职位id',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`integral_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COMMENT='积分表';

-- ----------------------------
-- Records of integral
-- ----------------------------
INSERT INTO `integral` VALUES ('26', '1', '若依', '15888888888', '0', '0', '0', null, null, '103');
INSERT INTO `integral` VALUES ('49', '23', '曹飞', '13131936792', '400', '0', '0', null, null, null);
INSERT INTO `integral` VALUES ('50', '24', '孙瑞娜', '18833424134', '400', '0', '0', null, null, null);
INSERT INTO `integral` VALUES ('51', '25', '陈莉', '17798147767', '400', '0', '0', null, null, null);

-- ----------------------------
-- Table structure for integral_approval
-- ----------------------------
DROP TABLE IF EXISTS `integral_approval`;
CREATE TABLE `integral_approval` (
  `approval_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '审批主键',
  `approval_num` varchar(50) DEFAULT '' COMMENT '审批编号',
  `approval_title` varchar(255) DEFAULT '' COMMENT '审批标题',
  `approval_content` varchar(255) DEFAULT '' COMMENT '审批内容',
  `user_id` int(11) DEFAULT NULL COMMENT '关联用户id',
  `user_name` varchar(10) DEFAULT NULL COMMENT '员工姓名',
  `user_img` varchar(255) DEFAULT NULL COMMENT '员工头像',
  `user_phone` bigint(20) DEFAULT NULL COMMENT '申请人电话也是唯一标识',
  `user_dept` varchar(10) DEFAULT NULL COMMENT '所属部门',
  `user_dept_id` int(11) DEFAULT NULL COMMENT '部门id',
  `user_post_id` int(11) DEFAULT NULL COMMENT '职位id',
  `user_post` varchar(20) DEFAULT NULL COMMENT '员工职位',
  `ti_jiao_id` int(11) DEFAULT NULL COMMENT '提交人的id',
  `ti_jiao_name` varchar(20) DEFAULT '' COMMENT '审请人姓名',
  `ti_jiao_name_img` varchar(152) DEFAULT '' COMMENT '申请人头像',
  `integral_type_id` int(10) DEFAULT '0' COMMENT '积分类型',
  `sq_time` datetime DEFAULT NULL COMMENT '申请时间',
  `sp_time` datetime DEFAULT NULL COMMENT '审批时间',
  `status` int(11) DEFAULT '0' COMMENT '审批状态(0,审批中 1审批通过，2审批不通过，3撤销审批 )',
  `sq_integral` int(20) DEFAULT '0' COMMENT '申请积分',
  `sp_remark` varchar(30) DEFAULT NULL COMMENT '审批备注',
  `approval_time` datetime DEFAULT NULL COMMENT '申请时间',
  `approval_img1` varchar(1000) DEFAULT '' COMMENT '上传图片1',
  `approval_img2` varchar(255) DEFAULT '' COMMENT '上传图片2',
  `approval_img3` varchar(255) DEFAULT '' COMMENT '上传图片3',
  `approval_img4` varchar(255) DEFAULT NULL COMMENT '上传图片4',
  `approval_img5` varchar(255) DEFAULT NULL COMMENT '上传图片5',
  `approval_img6` varchar(255) DEFAULT NULL COMMENT '上传图片6',
  `approval_img7` varchar(255) DEFAULT NULL COMMENT '上传图片7',
  `approval_img8` varchar(255) DEFAULT NULL COMMENT '上传图片8',
  `approval_img9` varchar(255) DEFAULT NULL COMMENT '上传图片9',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `k_integral` int(10) DEFAULT '0' COMMENT '扣除积分',
  `chao_song_id` int(11) DEFAULT NULL COMMENT '抄送人id',
  `chao_song_name` varchar(20) DEFAULT NULL COMMENT '抄送人姓名',
  `chao_song_img` varchar(255) DEFAULT NULL COMMENT '抄送人头像',
  `chao_song_dept_name` varchar(20) DEFAULT NULL COMMENT '抄送人部门',
  `chao_song_dept_id` int(11) DEFAULT NULL COMMENT '抄送人id',
  `type_id` int(11) DEFAULT NULL COMMENT '积分类型 1 品德积分 2 业绩积分 3 行为积分',
  `count_integral` bigint(20) DEFAULT NULL COMMENT '总积分',
  `add_integral` bigint(20) DEFAULT NULL COMMENT '添加积分',
  `deduce` bigint(20) DEFAULT '0' COMMENT '扣除总积分和',
  `amount` bigint(20) DEFAULT '0' COMMENT '总分',
  `ti_jiao_ren_ids` varchar(255) DEFAULT NULL COMMENT '提交人的ids',
  `chao_song_ids` varchar(255) DEFAULT NULL COMMENT '抄送人的ids',
  `shen_pi_ren_ids` varchar(255) DEFAULT NULL COMMENT '审批人的ids',
  PRIMARY KEY (`approval_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='审批管理';

-- ----------------------------
-- Records of integral_approval
-- ----------------------------
INSERT INTO `integral_approval` VALUES ('1', 'JFB20181204140053', '积分标题', '', '39', '曹飞', 'http://192.168.0.101:8080/af6e8bdf-6baa-4786-89af-41184714bba9blob.jpg', '13131936792', '系统信息部', '111', '7', null, '39', '曹飞', 'http://192.168.0.101:8080/af6e8bdf-6baa-4786-89af-41184714bba9blob.jpg', '2', '2018-12-04 14:00:53', '2018-12-04 14:01:15', '1', '200', null, null, 'http://118.190.208.221:8080/32ab5b85-e8cd-44e6-bdf0-5276a65d892b微信截图_20181029154209.png', '', '', null, null, null, null, null, null, null, '0', null, null, null, null, null, '0', null, null, '0', '0', '39', '39,40', '40');
INSERT INTO `integral_approval` VALUES ('2', 'JFB20181204140439', '积分标题', '', '39', '曹飞', 'http://192.168.0.101:8080/af6e8bdf-6baa-4786-89af-41184714bba9blob.jpg', '13131936792', '系统信息部', '111', '7', null, '39', '曹飞', 'http://192.168.0.101:8080/af6e8bdf-6baa-4786-89af-41184714bba9blob.jpg', '2', '2018-12-04 14:04:39', null, '0', '200', null, null, 'http://192.168.0.101:8080/bb95b911-084d-4367-809d-f104fa9d6c23微信图片_20181105144425.jpg', '', '', null, null, null, null, null, null, null, '0', null, null, null, null, null, '0', null, null, '0', '0', '39', '39,40', '40');

-- ----------------------------
-- Table structure for integral_goods
-- ----------------------------
DROP TABLE IF EXISTS `integral_goods`;
CREATE TABLE `integral_goods` (
  `good_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `good_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `good_img` varchar(255) DEFAULT NULL COMMENT '商品封面',
  `good_count` int(20) DEFAULT '0' COMMENT '总数量',
  `good_kc` int(50) DEFAULT NULL COMMENT '商品库存',
  `ydh_num` int(50) DEFAULT NULL COMMENT '已兑换数量',
  `dh_integral` int(50) DEFAULT NULL COMMENT '兑换积分',
  `status` int(1) DEFAULT '0' COMMENT '兑换转态(0,兑换中  1停止兑换)',
  `good_lb_img` varchar(1000) DEFAULT NULL COMMENT '商品轮播图',
  `good_details` varchar(255) DEFAULT NULL COMMENT '商品详情',
  `create_time` datetime DEFAULT NULL COMMENT '上传时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`good_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品管理表';

-- ----------------------------
-- Records of integral_goods
-- ----------------------------

-- ----------------------------
-- Table structure for integral_jk
-- ----------------------------
DROP TABLE IF EXISTS `integral_jk`;
CREATE TABLE `integral_jk` (
  `jk_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `jk_num` varchar(30) DEFAULT NULL COMMENT '奖扣编号',
  `jk_title` varchar(50) DEFAULT NULL COMMENT '奖扣标题',
  `jk_name` varchar(5) DEFAULT NULL COMMENT '奖扣员工姓名',
  `jk_phone` varchar(11) DEFAULT NULL COMMENT '奖扣人员姓名',
  `dept_id` int(11) DEFAULT NULL COMMENT '奖励员工部门',
  `dept_name` varchar(20) DEFAULT NULL COMMENT '奖励人员所属部门',
  `jk_img` varchar(255) DEFAULT NULL COMMENT '奖扣员工头像',
  `j_integral` int(10) DEFAULT NULL COMMENT '奖励积分',
  `k_integral` int(10) DEFAULT NULL COMMENT '扣除积分',
  `jk_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '奖扣时间',
  `jk_describe` varchar(255) DEFAULT NULL COMMENT '积分奖扣描述',
  `status` int(1) DEFAULT NULL COMMENT '状态',
  `remark` varchar(50) DEFAULT NULL COMMENT '描述',
  `type_id` int(1) DEFAULT NULL COMMENT '奖励类型 abc',
  PRIMARY KEY (`jk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分奖扣表';

-- ----------------------------
-- Records of integral_jk
-- ----------------------------

-- ----------------------------
-- Table structure for integral_log
-- ----------------------------
DROP TABLE IF EXISTS `integral_log`;
CREATE TABLE `integral_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '积分日志',
  `approval_num` varchar(30) DEFAULT NULL COMMENT '审批编号',
  `user_id` int(11) DEFAULT NULL COMMENT '关联用户id',
  `integral_id` int(11) DEFAULT NULL COMMENT '积分表的主键',
  `user_phone` bigint(12) DEFAULT NULL COMMENT '员工手机号',
  `user_name` varchar(6) DEFAULT NULL COMMENT '员工姓名',
  `user_img` varchar(255) DEFAULT NULL COMMENT '员工头像',
  `user_dept` varchar(10) DEFAULT NULL COMMENT '员工部门',
  `user_post` varchar(10) DEFAULT NULL COMMENT '员工职位',
  `integral_title` varchar(255) DEFAULT NULL COMMENT '申请积分项目标题',
  `integral_content` varchar(255) DEFAULT NULL COMMENT '积分内容',
  `bian_integral` int(11) DEFAULT '0' COMMENT '变动积分(所为刚才加的积分)',
  `type_id` int(11) DEFAULT NULL COMMENT '积分类型 1 品德积分 2 业绩积分 3 行为积分',
  `get_time` datetime DEFAULT NULL COMMENT '获取积分时间',
  `status` int(1) DEFAULT '0' COMMENT '转态 （0正常    1.撤销刚才所加的积分 2.管理员添加积分）',
  `remark` varchar(20) DEFAULT NULL COMMENT '扣除积分',
  `k_integral` int(25) DEFAULT '0' COMMENT '扣除积分',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='积分日志表';

-- ----------------------------
-- Records of integral_log
-- ----------------------------
INSERT INTO `integral_log` VALUES ('1', 'JFB20181204140053', '39', null, '13131936792', '曹飞', 'http://192.168.0.101:8080/af6e8bdf-6baa-4786-89af-41184714bba9blob.jpg', '系统信息部', '美容师/理疗师/专员', '积分标题', '积分标题', '200', '0', '2018-12-04 14:01:15', '0', null, '0');

-- ----------------------------
-- Table structure for integral_menu
-- ----------------------------
DROP TABLE IF EXISTS `integral_menu`;
CREATE TABLE `integral_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单主键',
  `parent_id` int(11) DEFAULT '0' COMMENT '父菜单id',
  `ancestors` varchar(255) DEFAULT '' COMMENT '祖级列表',
  `type_name` varchar(30) DEFAULT NULL COMMENT '行为类别',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `type_id` int(10) DEFAULT NULL COMMENT '行为类别',
  `type_count` int(20) DEFAULT NULL COMMENT '子类别个数',
  `status` int(1) DEFAULT '0' COMMENT '状态 0正常  2 禁用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `dept_id` int(11) DEFAULT NULL COMMENT '关联部门主键',
  `post_id` int(11) DEFAULT NULL COMMENT '职位主键',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT=' 积分菜单管理';

-- ----------------------------
-- Records of integral_menu
-- ----------------------------
INSERT INTO `integral_menu` VALUES ('1', '0', '0', '积分类型', '1', '0', null, '0', '2018-10-29 13:36:38', null, null, null, '顶级餐单');
INSERT INTO `integral_menu` VALUES ('2', '1', '0,1', '品德A分管理', '2', '1', null, '0', '2018-10-29 13:36:44', null, null, null, '一级餐单');
INSERT INTO `integral_menu` VALUES ('3', '1', '0,1', '业绩B分管理', '2', '2', null, '0', '2018-10-29 13:36:50', null, null, null, '一级餐单');
INSERT INTO `integral_menu` VALUES ('4', '1', '0,1', '行为C管理', '2', '3', null, '0', '2018-10-29 13:36:53', null, null, null, '一级餐单');
INSERT INTO `integral_menu` VALUES ('5', '4', '0,1,4', '鼎鼎香', null, '3', null, '0', '2018-12-04 10:03:17', null, '100', null, '');

-- ----------------------------
-- Table structure for integral_record
-- ----------------------------
DROP TABLE IF EXISTS `integral_record`;
CREATE TABLE `integral_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品主键',
  `record_name` varchar(20) DEFAULT NULL COMMENT '记录商品名称',
  `record_img` varchar(255) DEFAULT NULL COMMENT '记录商品封面',
  `dh_integral` int(50) DEFAULT '0' COMMENT '兑换积分',
  `user_id` int(11) DEFAULT NULL COMMENT '兑换人的id',
  `user_name` varchar(10) DEFAULT NULL COMMENT '兑换姓名',
  `user_phone` bigint(16) DEFAULT NULL COMMENT '兑换人手机号',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门id',
  `dept_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `sy_integral` int(50) DEFAULT NULL COMMENT '剩余积分',
  `dh_create_time` datetime DEFAULT NULL COMMENT '兑换时间',
  `sh_time` datetime DEFAULT NULL COMMENT '审核时间',
  `status` int(1) DEFAULT '0' COMMENT '状态(0 审核中 1,审核通过 ，2审核不通过)',
  `g_id` int(11) DEFAULT NULL COMMENT '商品id',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品兑换记录表';

-- ----------------------------
-- Records of integral_record
-- ----------------------------

-- ----------------------------
-- Table structure for integral_record_log
-- ----------------------------
DROP TABLE IF EXISTS `integral_record_log`;
CREATE TABLE `integral_record_log` (
  `record_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `record_name` varchar(20) DEFAULT NULL COMMENT '记录商品名称',
  `record_img` varchar(255) DEFAULT NULL COMMENT '记录商品封面',
  `dh_integral` int(50) DEFAULT '0' COMMENT '兑换积分',
  `user_name` varchar(10) DEFAULT NULL COMMENT '兑换姓名',
  `user_phone` bigint(20) DEFAULT NULL COMMENT '兑换人手机号',
  `sy_integral` int(50) DEFAULT NULL COMMENT '剩余积分',
  `dh_create_time` datetime DEFAULT NULL COMMENT '兑换时间',
  `sh_time` datetime DEFAULT NULL COMMENT '审核时间',
  `status` int(1) DEFAULT '0' COMMENT '状态(0 审核通过 ，1审核不通过)',
  `g_id` int(11) DEFAULT NULL COMMENT '商品id',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`record_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品兑换记录日志表';

-- ----------------------------
-- Records of integral_record_log
-- ----------------------------

-- ----------------------------
-- Table structure for integral_sqfs
-- ----------------------------
DROP TABLE IF EXISTS `integral_sqfs`;
CREATE TABLE `integral_sqfs` (
  `sqfs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '申请方式',
  `sq_name` varchar(20) DEFAULT NULL COMMENT '申请方式名称',
  PRIMARY KEY (`sqfs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='积分申请方式';

-- ----------------------------
-- Records of integral_sqfs
-- ----------------------------
INSERT INTO `integral_sqfs` VALUES ('1', '每天一次');
INSERT INTO `integral_sqfs` VALUES ('2', '每周一次');
INSERT INTO `integral_sqfs` VALUES ('3', '每月一次');

-- ----------------------------
-- Table structure for integral_type
-- ----------------------------
DROP TABLE IF EXISTS `integral_type`;
CREATE TABLE `integral_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(25) DEFAULT NULL COMMENT '类型名称',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='积分类别表';

-- ----------------------------
-- Records of integral_type
-- ----------------------------
INSERT INTO `integral_type` VALUES ('1', '品德A分管理');
INSERT INTO `integral_type` VALUES ('2', '业绩B分管理');
INSERT INTO `integral_type` VALUES ('3', '行为C分管理');

-- ----------------------------
-- Table structure for pd_integral
-- ----------------------------
DROP TABLE IF EXISTS `pd_integral`;
CREATE TABLE `pd_integral` (
  `behavior_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '品德积分主键',
  `behavior_category` varchar(255) DEFAULT '' COMMENT '品德类别',
  `behavior_title` varchar(255) DEFAULT '' COMMENT '品德标题',
  `behavior_content` varchar(255) DEFAULT '' COMMENT '品德内容',
  `shen_qing_fang_shi` varchar(20) DEFAULT '' COMMENT '申请方式',
  `type_id` int(10) DEFAULT '0' COMMENT '类型',
  `zui_duo_integral` varchar(20) DEFAULT '' COMMENT '最多奖励',
  `zui_shao_integral` varchar(255) DEFAULT '' COMMENT '最少奖励积分',
  `integral_fen_ji` int(11) DEFAULT NULL COMMENT '积分分级',
  `yi_wan_cheng_ci_shu` int(11) DEFAULT '0' COMMENT '已完成次数',
  `status` int(1) DEFAULT '0' COMMENT '使用转态 0,使用中  1.禁用   2 设置审核人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单主键',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `post_id` int(11) DEFAULT NULL COMMENT '职位id',
  `post_name` varchar(20) DEFAULT NULL COMMENT '职位名称',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门名称',
  `dept_name` varchar(20) DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`behavior_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='品德A积分管理';

-- ----------------------------
-- Records of pd_integral
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL,
  `calendar_name` varchar(200) NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `cron_expression` varchar(200) NOT NULL,
  `time_zone_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `entry_id` varchar(95) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `job_name` varchar(200) DEFAULT NULL,
  `job_group` varchar(200) DEFAULT NULL,
  `is_nonconcurrent` varchar(1) DEFAULT NULL,
  `requests_recovery` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `job_class_name` varchar(250) NOT NULL,
  `is_durable` varchar(1) NOT NULL,
  `is_nonconcurrent` varchar(1) NOT NULL,
  `is_update_data` varchar(1) NOT NULL,
  `requests_recovery` varchar(1) NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__1', 'DEFAULT', null, 'com.ruoyi.quartz.util.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000E63726F6E45787072657373696F6E7400124C6A6176612F6C616E672F537472696E673B4C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000A6D6574686F644E616D6571007E00094C000C6D6574686F64506172616D7371007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720020636F6D2E72756F79692E636F6D6D6F6E2E626173652E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E0787074000070707074000E302F3130202A202A202A202A203F740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC897372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000672795461736B74000A72794E6F506172616D7374000074000130740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__2', 'DEFAULT', null, 'com.ruoyi.quartz.util.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000E63726F6E45787072657373696F6E7400124C6A6176612F6C616E672F537472696E673B4C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000A6D6574686F644E616D6571007E00094C000C6D6574686F64506172616D7371007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720020636F6D2E72756F79692E636F6D6D6F6E2E626173652E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E0787074000070707074000E302F3230202A202A202A202A203F740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC897372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000274000672795461736B7400087279506172616D73740002727974000130740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL,
  `lock_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'wangluobu21544056700538', '1544058131407', '15000');

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `str_prop_1` varchar(512) DEFAULT NULL,
  `str_prop_2` varchar(512) DEFAULT NULL,
  `str_prop_3` varchar(512) DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13,4) DEFAULT NULL,
  `dec_prop_2` decimal(13,4) DEFAULT NULL,
  `bool_prop_1` varchar(1) DEFAULT NULL,
  `bool_prop_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) NOT NULL,
  `trigger_type` varchar(8) NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__1', 'DEFAULT', '__TASK_CLASS_NAME__1', 'DEFAULT', null, '1539581800000', '-1', '5', 'PAUSED', 'CRON', '1539581791000', '0', null, '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000E63726F6E45787072657373696F6E7400124C6A6176612F6C616E672F537472696E673B4C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000A6D6574686F644E616D6571007E00094C000C6D6574686F64506172616D7371007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720020636F6D2E72756F79692E636F6D6D6F6E2E626173652E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E0787074000070707074000E302F3130202A202A202A202A203F740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC897372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000672795461736B74000A72794E6F506172616D7374000074000130740001317800);
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', '__TASK_CLASS_NAME__2', 'DEFAULT', '__TASK_CLASS_NAME__2', 'DEFAULT', null, '1539581800000', '-1', '5', 'PAUSED', 'CRON', '1539581791000', '0', null, '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C770800000010000000017400135F5F5441534B5F50524F504552544945535F5F7372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000E63726F6E45787072657373696F6E7400124C6A6176612F6C616E672F537472696E673B4C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000A6D6574686F644E616D6571007E00094C000C6D6574686F64506172616D7371007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720020636F6D2E72756F79692E636F6D6D6F6E2E626173652E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B59741903000078707708000001622CDE29E0787074000070707074000E302F3230202A202A202A202A203F740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC897372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000274000672795461736B7400087279506172616D73740002727974000130740001317800);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(100) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-default', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '默认 skin-default、蓝色 skin-blue、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '初始化密码 123456');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` int(11) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT '' COMMENT '负责人',
  `phone` varchar(11) DEFAULT '' COMMENT '联系电话',
  `email` varchar(50) DEFAULT '' COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('100', '0', '0', '媚思嘉科技', '0', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('101', '100', '0,100', '财务管理部', '1', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('102', '100', '0,100', '人力资源部', '2', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('103', '101', '0,100,101', '集团财务', '1', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('104', '101', '0,100,101', '美容财务', '2', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('105', '115', '0,100,115', '思觅嘉事业部', '3', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('106', '115', '0,100,115', '媚思嘉技术学校', '4', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('107', '115', '0,100,115', '媚思嘉商学院', '5', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('108', '115', '0,100,115', '九雅女子学堂', '1', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('109', '115', '0,100,115', '鼎鼎香餐饮事业部', '2', '孙丽', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00');
INSERT INTO `sys_dept` VALUES ('110', '100', '0,100', '产品技术部', '1', '孙丽', '15888888888', 'ry@qq.com', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('111', '100', '0,100', '系统信息部', '1', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('112', '100', '0,100', '企业策划部', '1', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('113', '100', '0,100', '行政外联部', '1', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('114', '113', '0,100,113', '再就业服务', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('115', '100', '0,100', '运营中心', '1', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('116', '115', '0,100,115', '媚思嘉美容事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('117', '116', '0,100,116', '美容：桥东店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('118', '116', '0,100,116', '美容：滨苑店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('119', '116', '0,100,116', '美容：形象店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('120', '116', '0,100,116', '美容：新华里', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('121', '116', '0,100,116', '美容：燕云台', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('122', '116', '0,100,116', '美容：养生馆', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('123', '116', '0,100,116', '美容：生活馆', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('124', '116', '0,100,116', '美容：任县店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('125', '116', '0,100,116', '美容：内丘店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('126', '116', '0,100,116', '美容：沙河店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('127', '115', '0,100,115', '媚思嘉美发事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('128', '127', '0,100,127', '美发：桥东店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('129', '127', '0,100,127', '美发：燕云台', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('130', '127', '0,100,127', '美发：曼哈顿', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('131', '127', '0,100,127', '美发：生活馆', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('132', '127', '0,100,127', '美发：形象店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('133', '127', '0,100,127', '美发：开元店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('134', '115', '0,100,115', '35度皮肤管理事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('135', '134', '0,100,134', '35度：新华里', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('136', '134', '0,100,134', '35度：天一店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('137', '115', '0,100,115', '栢悦歆事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('138', '137', '0,100,137', '栢悦歆：邯郸店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('139', '137', '0,100,137', '栢悦歆：开元店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('140', '137', '0,100,137', '栢悦歆：海德花园', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('141', '137', '0,100,137', '栢悦歆：锦绣鹏程', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('142', '137', '0,100,137', '栢悦歆：石家庄店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('143', '115', '0,100,115', '医美事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('144', '143', '0,100,143', '医美：东门诊', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('145', '143', '0,100,143', '医美：西门诊', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('146', '143', '0,100,143', '医美：市场部', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('147', '115', '0,100,115', '纹绣事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('148', '147', '0,100,147', '昕和传承事业部', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('149', '147', '0,100,147', '靓神美甲事业部', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('150', '115', '0,100,115', '靓神美发事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('151', '150', '0,100,150', '靓神:海德花园1店', '3', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('152', '115', '0,100,115', '肽吉堂事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('153', '115', '0,100,115', 'MQ事业部', '2', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('154', '100', '0,100', '总裁办', '1', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('155', '100', '0,100', '牙科', '1', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('156', '100', '0,100', '总库', '1', '', '', '', '0', '0', '', null, '', null);
INSERT INTO `sys_dept` VALUES ('157', '147', '0,100,115,147', 'qqqqqqqqqqq', '1', '', '18132927768', '1334091443@qq.com', '0', '2', 'admin', '2018-11-13 11:55:28', '', null);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT '' COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT '' COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别女');
INSERT INTO `sys_dict_data` VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '公告');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '新增操作');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '修改操作');
INSERT INTO `sys_dict_data` VALUES ('18', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '删除操作');
INSERT INTO `sys_dict_data` VALUES ('19', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '授权操作');
INSERT INTO `sys_dict_data` VALUES ('20', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导出操作');
INSERT INTO `sys_dict_data` VALUES ('21', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '导入操作');
INSERT INTO `sys_dict_data` VALUES ('22', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '强退操作');
INSERT INTO `sys_dict_data` VALUES ('23', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '生成操作');
INSERT INTO `sys_dict_data` VALUES ('24', '8', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '清空操作');
INSERT INTO `sys_dict_data` VALUES ('25', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '正常状态');
INSERT INTO `sys_dict_data` VALUES ('26', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '停用状态');
INSERT INTO `sys_dict_data` VALUES ('27', '1', '审批中', '0', 'sys_sp_status', '', '', 'Y', '0', 'admin', '2018-10-27 10:32:32', 'sunli', '2018-10-27 10:32:45', '审批中状态');
INSERT INTO `sys_dict_data` VALUES ('28', '2', '审批通过', '1', 'sys_sp_status', '', '', 'N', '0', 'admin', '2018-10-27 10:32:35', 'sunli', '2018-10-27 10:32:48', '审批通过状态');
INSERT INTO `sys_dict_data` VALUES ('29', '3', '审批不通过', '2', 'sys_sp_status', '', '', 'N', '0', 'admin', '2018-10-27 10:32:38', 'sunli', '2018-10-27 10:32:50', '审批不通过状态');
INSERT INTO `sys_dict_data` VALUES ('30', '4', '撤销审批', '3', 'sys_sp_status', '', '', 'N', '0', 'admin', '2018-10-27 10:32:42', 'sunli', '2018-10-27 10:32:53', '撤销审批状态');
INSERT INTO `sys_dict_data` VALUES ('31', '0', '在职', '0', 'sys_zl_status', '', 'primary', 'Y', '0', '', null, '', null, '员工在职状态');
INSERT INTO `sys_dict_data` VALUES ('32', '1', '离职', '1', 'sys_zl_status', '', 'danger', 'N', '0', '', null, '', null, '员工离职状态');
INSERT INTO `sys_dict_data` VALUES ('33', '0', '参与积分排名', '1', 'sys_jf_status', '', 'primary', 'Y', '0', '', null, '', null, '员工参与积分排名');
INSERT INTO `sys_dict_data` VALUES ('34', '1', '不参与积分排名', '2', 'sys_jf_status', '', 'danger', 'N', '0', '', null, '', null, '员工不参与积分排名');
INSERT INTO `sys_dict_data` VALUES ('35', '0', '使用中', '0', 'integralMenu_type_disable', '', 'primary', 'Y', '0', '', null, '', null, '积分使用中');
INSERT INTO `sys_dict_data` VALUES ('36', '1', '禁用中', '2', 'integralMenu_type_disable', '', 'danger', 'N', '0', '', null, '', null, '积分禁用');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('6', '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('8', '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('9', '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('10', '审批状态', 'sys_sp_status', '0', 'admin', '2018-10-27 10:29:06', 'sunli', '2018-10-27 10:29:14', '审批状态列表');
INSERT INTO `sys_dict_type` VALUES ('11', '员工在离状态', 'sys_zl_status', '0', '孙丽', '2018-10-30 16:09:56', 'sunli', '2018-10-30 16:10:07', '员工在离状态列表');
INSERT INTO `sys_dict_type` VALUES ('12', '是否参与积分', 'sys_jf_status', '0', 'sunli', '2018-10-30 16:13:42', 'sunli', '2018-10-30 16:13:50', '是否参与积分列表');
INSERT INTO `sys_dict_type` VALUES ('13', '积分类别使用转态', 'integralMenu_type_disable', '0', 'sunli', '2018-11-01 10:14:28', 'sunli', '2018-11-01 10:14:36', '积分类别使用转态');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT '' COMMENT '任务组名',
  `method_name` varchar(500) DEFAULT '' COMMENT '任务方法',
  `method_params` varchar(200) DEFAULT '' COMMENT '方法参数',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '0' COMMENT '计划执行错误策略（0默认 1继续 2等待 3放弃）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', 'ryTask', '系统默认（无参）', 'ryNoParams', '', '0/10 * * * * ?', '0', '1', 'admin', '2018-03-16 11:33:00', 'admin', '2018-11-29 09:16:33', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `method_name` varchar(500) DEFAULT NULL COMMENT '任务方法',
  `method_params` varchar(200) DEFAULT '' COMMENT '方法参数',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` text COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3185 DEFAULT CHARSET=utf8 COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES ('1001', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:44:10');
INSERT INTO `sys_job_log` VALUES ('1002', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:44:20');
INSERT INTO `sys_job_log` VALUES ('1003', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:44:30');
INSERT INTO `sys_job_log` VALUES ('1004', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:44:40');
INSERT INTO `sys_job_log` VALUES ('1005', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:44:50');
INSERT INTO `sys_job_log` VALUES ('1006', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:45:00');
INSERT INTO `sys_job_log` VALUES ('1007', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:45:10');
INSERT INTO `sys_job_log` VALUES ('1008', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:45:20');
INSERT INTO `sys_job_log` VALUES ('1009', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:45:30');
INSERT INTO `sys_job_log` VALUES ('1010', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:45:40');
INSERT INTO `sys_job_log` VALUES ('1011', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:45:50');
INSERT INTO `sys_job_log` VALUES ('1012', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 11:46:00');
INSERT INTO `sys_job_log` VALUES ('1013', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:46:10');
INSERT INTO `sys_job_log` VALUES ('1014', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:46:20');
INSERT INTO `sys_job_log` VALUES ('1015', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:46:30');
INSERT INTO `sys_job_log` VALUES ('1016', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:46:40');
INSERT INTO `sys_job_log` VALUES ('1017', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:46:50');
INSERT INTO `sys_job_log` VALUES ('1018', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:47:00');
INSERT INTO `sys_job_log` VALUES ('1019', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:47:10');
INSERT INTO `sys_job_log` VALUES ('1020', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:47:20');
INSERT INTO `sys_job_log` VALUES ('1021', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:47:30');
INSERT INTO `sys_job_log` VALUES ('1022', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:47:40');
INSERT INTO `sys_job_log` VALUES ('1023', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:47:50');
INSERT INTO `sys_job_log` VALUES ('1024', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:48:00');
INSERT INTO `sys_job_log` VALUES ('1025', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 11:48:10');
INSERT INTO `sys_job_log` VALUES ('1026', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:48:20');
INSERT INTO `sys_job_log` VALUES ('1027', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:48:30');
INSERT INTO `sys_job_log` VALUES ('1028', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:48:40');
INSERT INTO `sys_job_log` VALUES ('1029', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 11:48:50');
INSERT INTO `sys_job_log` VALUES ('1030', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:49:00');
INSERT INTO `sys_job_log` VALUES ('1031', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:49:10');
INSERT INTO `sys_job_log` VALUES ('1032', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:49:20');
INSERT INTO `sys_job_log` VALUES ('1033', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:49:30');
INSERT INTO `sys_job_log` VALUES ('1034', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:49:40');
INSERT INTO `sys_job_log` VALUES ('1035', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:49:50');
INSERT INTO `sys_job_log` VALUES ('1036', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:50:00');
INSERT INTO `sys_job_log` VALUES ('1037', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:50:10');
INSERT INTO `sys_job_log` VALUES ('1038', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:50:20');
INSERT INTO `sys_job_log` VALUES ('1039', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:50:30');
INSERT INTO `sys_job_log` VALUES ('1040', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:50:40');
INSERT INTO `sys_job_log` VALUES ('1041', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:50:50');
INSERT INTO `sys_job_log` VALUES ('1042', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:51:00');
INSERT INTO `sys_job_log` VALUES ('1043', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:51:10');
INSERT INTO `sys_job_log` VALUES ('1044', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 11:51:20');
INSERT INTO `sys_job_log` VALUES ('1045', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:51:30');
INSERT INTO `sys_job_log` VALUES ('1046', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:51:40');
INSERT INTO `sys_job_log` VALUES ('1047', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:51:50');
INSERT INTO `sys_job_log` VALUES ('1048', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 11:52:00');
INSERT INTO `sys_job_log` VALUES ('1049', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:52:10');
INSERT INTO `sys_job_log` VALUES ('1050', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:52:20');
INSERT INTO `sys_job_log` VALUES ('1051', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:52:30');
INSERT INTO `sys_job_log` VALUES ('1052', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:52:40');
INSERT INTO `sys_job_log` VALUES ('1053', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:52:50');
INSERT INTO `sys_job_log` VALUES ('1054', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:53:00');
INSERT INTO `sys_job_log` VALUES ('1055', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:53:10');
INSERT INTO `sys_job_log` VALUES ('1056', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 11:53:20');
INSERT INTO `sys_job_log` VALUES ('1057', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:53:30');
INSERT INTO `sys_job_log` VALUES ('1058', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:53:40');
INSERT INTO `sys_job_log` VALUES ('1059', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:53:50');
INSERT INTO `sys_job_log` VALUES ('1060', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:54:00');
INSERT INTO `sys_job_log` VALUES ('1061', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:54:10');
INSERT INTO `sys_job_log` VALUES ('1062', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:54:20');
INSERT INTO `sys_job_log` VALUES ('1063', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:54:30');
INSERT INTO `sys_job_log` VALUES ('1064', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:54:40');
INSERT INTO `sys_job_log` VALUES ('1065', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 11:54:50');
INSERT INTO `sys_job_log` VALUES ('1066', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:55:00');
INSERT INTO `sys_job_log` VALUES ('1067', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:55:10');
INSERT INTO `sys_job_log` VALUES ('1068', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 11:55:20');
INSERT INTO `sys_job_log` VALUES ('1069', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:55:30');
INSERT INTO `sys_job_log` VALUES ('1070', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:55:40');
INSERT INTO `sys_job_log` VALUES ('1071', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:55:50');
INSERT INTO `sys_job_log` VALUES ('1072', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:56:00');
INSERT INTO `sys_job_log` VALUES ('1073', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:56:10');
INSERT INTO `sys_job_log` VALUES ('1074', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:56:20');
INSERT INTO `sys_job_log` VALUES ('1075', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:56:30');
INSERT INTO `sys_job_log` VALUES ('1076', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:56:40');
INSERT INTO `sys_job_log` VALUES ('1077', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:56:50');
INSERT INTO `sys_job_log` VALUES ('1078', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:57:00');
INSERT INTO `sys_job_log` VALUES ('1079', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 11:57:10');
INSERT INTO `sys_job_log` VALUES ('1080', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:57:20');
INSERT INTO `sys_job_log` VALUES ('1081', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 11:57:30');
INSERT INTO `sys_job_log` VALUES ('1082', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:57:40');
INSERT INTO `sys_job_log` VALUES ('1083', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:57:50');
INSERT INTO `sys_job_log` VALUES ('1084', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:58:00');
INSERT INTO `sys_job_log` VALUES ('1085', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:58:10');
INSERT INTO `sys_job_log` VALUES ('1086', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:58:20');
INSERT INTO `sys_job_log` VALUES ('1087', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:58:30');
INSERT INTO `sys_job_log` VALUES ('1088', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:58:40');
INSERT INTO `sys_job_log` VALUES ('1089', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:58:50');
INSERT INTO `sys_job_log` VALUES ('1090', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:59:00');
INSERT INTO `sys_job_log` VALUES ('1091', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:59:10');
INSERT INTO `sys_job_log` VALUES ('1092', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:59:20');
INSERT INTO `sys_job_log` VALUES ('1093', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:59:30');
INSERT INTO `sys_job_log` VALUES ('1094', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 11:59:40');
INSERT INTO `sys_job_log` VALUES ('1095', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 11:59:50');
INSERT INTO `sys_job_log` VALUES ('1096', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:00:00');
INSERT INTO `sys_job_log` VALUES ('1097', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:00:10');
INSERT INTO `sys_job_log` VALUES ('1098', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:00:20');
INSERT INTO `sys_job_log` VALUES ('1099', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:00:30');
INSERT INTO `sys_job_log` VALUES ('1100', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:00:40');
INSERT INTO `sys_job_log` VALUES ('1101', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:00:50');
INSERT INTO `sys_job_log` VALUES ('1102', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:01:00');
INSERT INTO `sys_job_log` VALUES ('1103', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:01:10');
INSERT INTO `sys_job_log` VALUES ('1104', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:01:20');
INSERT INTO `sys_job_log` VALUES ('1105', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:01:30');
INSERT INTO `sys_job_log` VALUES ('1106', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:01:40');
INSERT INTO `sys_job_log` VALUES ('1107', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:01:50');
INSERT INTO `sys_job_log` VALUES ('1108', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:02:00');
INSERT INTO `sys_job_log` VALUES ('1109', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:02:10');
INSERT INTO `sys_job_log` VALUES ('1110', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:02:20');
INSERT INTO `sys_job_log` VALUES ('1111', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:02:30');
INSERT INTO `sys_job_log` VALUES ('1112', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:02:40');
INSERT INTO `sys_job_log` VALUES ('1113', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:02:50');
INSERT INTO `sys_job_log` VALUES ('1114', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:03:00');
INSERT INTO `sys_job_log` VALUES ('1115', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:03:10');
INSERT INTO `sys_job_log` VALUES ('1116', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:03:20');
INSERT INTO `sys_job_log` VALUES ('1117', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:03:30');
INSERT INTO `sys_job_log` VALUES ('1118', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:03:40');
INSERT INTO `sys_job_log` VALUES ('1119', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:03:50');
INSERT INTO `sys_job_log` VALUES ('1120', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:04:00');
INSERT INTO `sys_job_log` VALUES ('1121', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:04:10');
INSERT INTO `sys_job_log` VALUES ('1122', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:04:20');
INSERT INTO `sys_job_log` VALUES ('1123', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:04:30');
INSERT INTO `sys_job_log` VALUES ('1124', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:04:40');
INSERT INTO `sys_job_log` VALUES ('1125', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:04:50');
INSERT INTO `sys_job_log` VALUES ('1126', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 12:05:00');
INSERT INTO `sys_job_log` VALUES ('1127', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:05:10');
INSERT INTO `sys_job_log` VALUES ('1128', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:05:20');
INSERT INTO `sys_job_log` VALUES ('1129', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:05:30');
INSERT INTO `sys_job_log` VALUES ('1130', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:05:40');
INSERT INTO `sys_job_log` VALUES ('1131', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:05:50');
INSERT INTO `sys_job_log` VALUES ('1132', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:06:00');
INSERT INTO `sys_job_log` VALUES ('1133', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:06:10');
INSERT INTO `sys_job_log` VALUES ('1134', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：8毫秒', '0', null, '2018-11-28 12:06:20');
INSERT INTO `sys_job_log` VALUES ('1135', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:06:30');
INSERT INTO `sys_job_log` VALUES ('1136', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：8毫秒', '0', null, '2018-11-28 12:06:40');
INSERT INTO `sys_job_log` VALUES ('1137', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：8毫秒', '0', null, '2018-11-28 12:06:50');
INSERT INTO `sys_job_log` VALUES ('1138', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:07:00');
INSERT INTO `sys_job_log` VALUES ('1139', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:07:10');
INSERT INTO `sys_job_log` VALUES ('1140', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:07:20');
INSERT INTO `sys_job_log` VALUES ('1141', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:07:30');
INSERT INTO `sys_job_log` VALUES ('1142', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:07:40');
INSERT INTO `sys_job_log` VALUES ('1143', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:07:50');
INSERT INTO `sys_job_log` VALUES ('1144', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 12:08:00');
INSERT INTO `sys_job_log` VALUES ('1145', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:08:10');
INSERT INTO `sys_job_log` VALUES ('1146', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:08:20');
INSERT INTO `sys_job_log` VALUES ('1147', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:08:30');
INSERT INTO `sys_job_log` VALUES ('1148', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:08:40');
INSERT INTO `sys_job_log` VALUES ('1149', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:08:50');
INSERT INTO `sys_job_log` VALUES ('1150', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:09:00');
INSERT INTO `sys_job_log` VALUES ('1151', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:09:10');
INSERT INTO `sys_job_log` VALUES ('1152', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:09:20');
INSERT INTO `sys_job_log` VALUES ('1153', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:09:30');
INSERT INTO `sys_job_log` VALUES ('1154', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:09:40');
INSERT INTO `sys_job_log` VALUES ('1155', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:09:50');
INSERT INTO `sys_job_log` VALUES ('1156', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:10:00');
INSERT INTO `sys_job_log` VALUES ('1157', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:10:10');
INSERT INTO `sys_job_log` VALUES ('1158', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:10:20');
INSERT INTO `sys_job_log` VALUES ('1159', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:10:30');
INSERT INTO `sys_job_log` VALUES ('1160', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:10:40');
INSERT INTO `sys_job_log` VALUES ('1161', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:10:50');
INSERT INTO `sys_job_log` VALUES ('1162', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:11:00');
INSERT INTO `sys_job_log` VALUES ('1163', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:11:10');
INSERT INTO `sys_job_log` VALUES ('1164', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:11:20');
INSERT INTO `sys_job_log` VALUES ('1165', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:11:30');
INSERT INTO `sys_job_log` VALUES ('1166', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:11:40');
INSERT INTO `sys_job_log` VALUES ('1167', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:11:50');
INSERT INTO `sys_job_log` VALUES ('1168', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:12:00');
INSERT INTO `sys_job_log` VALUES ('1169', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:12:10');
INSERT INTO `sys_job_log` VALUES ('1170', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:12:20');
INSERT INTO `sys_job_log` VALUES ('1171', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:12:30');
INSERT INTO `sys_job_log` VALUES ('1172', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:12:40');
INSERT INTO `sys_job_log` VALUES ('1173', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:12:50');
INSERT INTO `sys_job_log` VALUES ('1174', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:13:00');
INSERT INTO `sys_job_log` VALUES ('1175', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 12:13:10');
INSERT INTO `sys_job_log` VALUES ('1176', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:13:20');
INSERT INTO `sys_job_log` VALUES ('1177', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:13:30');
INSERT INTO `sys_job_log` VALUES ('1178', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:13:40');
INSERT INTO `sys_job_log` VALUES ('1179', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:13:50');
INSERT INTO `sys_job_log` VALUES ('1180', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:14:00');
INSERT INTO `sys_job_log` VALUES ('1181', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 12:14:10');
INSERT INTO `sys_job_log` VALUES ('1182', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:14:20');
INSERT INTO `sys_job_log` VALUES ('1183', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:14:30');
INSERT INTO `sys_job_log` VALUES ('1184', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:14:40');
INSERT INTO `sys_job_log` VALUES ('1185', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:14:50');
INSERT INTO `sys_job_log` VALUES ('1186', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:15:00');
INSERT INTO `sys_job_log` VALUES ('1187', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:15:10');
INSERT INTO `sys_job_log` VALUES ('1188', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:15:20');
INSERT INTO `sys_job_log` VALUES ('1189', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:15:30');
INSERT INTO `sys_job_log` VALUES ('1190', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:15:40');
INSERT INTO `sys_job_log` VALUES ('1191', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:15:50');
INSERT INTO `sys_job_log` VALUES ('1192', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:16:00');
INSERT INTO `sys_job_log` VALUES ('1193', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:16:10');
INSERT INTO `sys_job_log` VALUES ('1194', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:16:20');
INSERT INTO `sys_job_log` VALUES ('1195', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:16:30');
INSERT INTO `sys_job_log` VALUES ('1196', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:16:40');
INSERT INTO `sys_job_log` VALUES ('1197', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:16:50');
INSERT INTO `sys_job_log` VALUES ('1198', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:17:00');
INSERT INTO `sys_job_log` VALUES ('1199', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:17:10');
INSERT INTO `sys_job_log` VALUES ('1200', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:17:20');
INSERT INTO `sys_job_log` VALUES ('1201', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:17:30');
INSERT INTO `sys_job_log` VALUES ('1202', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:17:40');
INSERT INTO `sys_job_log` VALUES ('1203', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:17:50');
INSERT INTO `sys_job_log` VALUES ('1204', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:18:00');
INSERT INTO `sys_job_log` VALUES ('1205', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:18:10');
INSERT INTO `sys_job_log` VALUES ('1206', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:18:20');
INSERT INTO `sys_job_log` VALUES ('1207', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:18:30');
INSERT INTO `sys_job_log` VALUES ('1208', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:18:40');
INSERT INTO `sys_job_log` VALUES ('1209', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:18:50');
INSERT INTO `sys_job_log` VALUES ('1210', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:19:00');
INSERT INTO `sys_job_log` VALUES ('1211', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:19:10');
INSERT INTO `sys_job_log` VALUES ('1212', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:19:20');
INSERT INTO `sys_job_log` VALUES ('1213', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:19:30');
INSERT INTO `sys_job_log` VALUES ('1214', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:19:40');
INSERT INTO `sys_job_log` VALUES ('1215', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:19:50');
INSERT INTO `sys_job_log` VALUES ('1216', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:20:00');
INSERT INTO `sys_job_log` VALUES ('1217', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:20:10');
INSERT INTO `sys_job_log` VALUES ('1218', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:20:20');
INSERT INTO `sys_job_log` VALUES ('1219', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:20:30');
INSERT INTO `sys_job_log` VALUES ('1220', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:20:40');
INSERT INTO `sys_job_log` VALUES ('1221', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:20:50');
INSERT INTO `sys_job_log` VALUES ('1222', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:21:00');
INSERT INTO `sys_job_log` VALUES ('1223', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:21:10');
INSERT INTO `sys_job_log` VALUES ('1224', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:21:20');
INSERT INTO `sys_job_log` VALUES ('1225', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:21:30');
INSERT INTO `sys_job_log` VALUES ('1226', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:21:40');
INSERT INTO `sys_job_log` VALUES ('1227', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:21:50');
INSERT INTO `sys_job_log` VALUES ('1228', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:22:00');
INSERT INTO `sys_job_log` VALUES ('1229', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:22:10');
INSERT INTO `sys_job_log` VALUES ('1230', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:22:20');
INSERT INTO `sys_job_log` VALUES ('1231', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:22:30');
INSERT INTO `sys_job_log` VALUES ('1232', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:22:40');
INSERT INTO `sys_job_log` VALUES ('1233', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:22:50');
INSERT INTO `sys_job_log` VALUES ('1234', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:23:00');
INSERT INTO `sys_job_log` VALUES ('1235', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:23:10');
INSERT INTO `sys_job_log` VALUES ('1236', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:23:20');
INSERT INTO `sys_job_log` VALUES ('1237', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:23:30');
INSERT INTO `sys_job_log` VALUES ('1238', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:23:40');
INSERT INTO `sys_job_log` VALUES ('1239', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:23:50');
INSERT INTO `sys_job_log` VALUES ('1240', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:24:00');
INSERT INTO `sys_job_log` VALUES ('1241', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:24:10');
INSERT INTO `sys_job_log` VALUES ('1242', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:24:20');
INSERT INTO `sys_job_log` VALUES ('1243', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:24:30');
INSERT INTO `sys_job_log` VALUES ('1244', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:24:40');
INSERT INTO `sys_job_log` VALUES ('1245', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:24:50');
INSERT INTO `sys_job_log` VALUES ('1246', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:25:00');
INSERT INTO `sys_job_log` VALUES ('1247', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:25:10');
INSERT INTO `sys_job_log` VALUES ('1248', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:25:20');
INSERT INTO `sys_job_log` VALUES ('1249', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:25:30');
INSERT INTO `sys_job_log` VALUES ('1250', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:25:40');
INSERT INTO `sys_job_log` VALUES ('1251', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:25:50');
INSERT INTO `sys_job_log` VALUES ('1252', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:26:00');
INSERT INTO `sys_job_log` VALUES ('1253', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:26:10');
INSERT INTO `sys_job_log` VALUES ('1254', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:26:20');
INSERT INTO `sys_job_log` VALUES ('1255', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:26:30');
INSERT INTO `sys_job_log` VALUES ('1256', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:26:40');
INSERT INTO `sys_job_log` VALUES ('1257', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:26:50');
INSERT INTO `sys_job_log` VALUES ('1258', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:27:00');
INSERT INTO `sys_job_log` VALUES ('1259', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:27:10');
INSERT INTO `sys_job_log` VALUES ('1260', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:27:20');
INSERT INTO `sys_job_log` VALUES ('1261', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:27:30');
INSERT INTO `sys_job_log` VALUES ('1262', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:27:40');
INSERT INTO `sys_job_log` VALUES ('1263', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:27:50');
INSERT INTO `sys_job_log` VALUES ('1264', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:28:00');
INSERT INTO `sys_job_log` VALUES ('1265', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 12:28:10');
INSERT INTO `sys_job_log` VALUES ('1266', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:28:20');
INSERT INTO `sys_job_log` VALUES ('1267', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:28:30');
INSERT INTO `sys_job_log` VALUES ('1268', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:28:40');
INSERT INTO `sys_job_log` VALUES ('1269', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:28:50');
INSERT INTO `sys_job_log` VALUES ('1270', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:29:00');
INSERT INTO `sys_job_log` VALUES ('1271', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:29:10');
INSERT INTO `sys_job_log` VALUES ('1272', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:29:20');
INSERT INTO `sys_job_log` VALUES ('1273', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:29:30');
INSERT INTO `sys_job_log` VALUES ('1274', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:29:40');
INSERT INTO `sys_job_log` VALUES ('1275', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:29:50');
INSERT INTO `sys_job_log` VALUES ('1276', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:30:00');
INSERT INTO `sys_job_log` VALUES ('1277', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:30:10');
INSERT INTO `sys_job_log` VALUES ('1278', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:30:20');
INSERT INTO `sys_job_log` VALUES ('1279', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:30:30');
INSERT INTO `sys_job_log` VALUES ('1280', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:30:40');
INSERT INTO `sys_job_log` VALUES ('1281', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：11毫秒', '0', null, '2018-11-28 12:30:50');
INSERT INTO `sys_job_log` VALUES ('1282', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:31:00');
INSERT INTO `sys_job_log` VALUES ('1283', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:31:10');
INSERT INTO `sys_job_log` VALUES ('1284', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:31:20');
INSERT INTO `sys_job_log` VALUES ('1285', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:31:30');
INSERT INTO `sys_job_log` VALUES ('1286', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:31:40');
INSERT INTO `sys_job_log` VALUES ('1287', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:31:50');
INSERT INTO `sys_job_log` VALUES ('1288', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:32:00');
INSERT INTO `sys_job_log` VALUES ('1289', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 12:32:10');
INSERT INTO `sys_job_log` VALUES ('1290', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:32:20');
INSERT INTO `sys_job_log` VALUES ('1291', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:32:30');
INSERT INTO `sys_job_log` VALUES ('1292', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:32:40');
INSERT INTO `sys_job_log` VALUES ('1293', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:32:50');
INSERT INTO `sys_job_log` VALUES ('1294', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:33:00');
INSERT INTO `sys_job_log` VALUES ('1295', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:33:10');
INSERT INTO `sys_job_log` VALUES ('1296', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:33:20');
INSERT INTO `sys_job_log` VALUES ('1297', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:33:30');
INSERT INTO `sys_job_log` VALUES ('1298', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：19毫秒', '0', null, '2018-11-28 12:33:40');
INSERT INTO `sys_job_log` VALUES ('1299', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:33:50');
INSERT INTO `sys_job_log` VALUES ('1300', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:34:00');
INSERT INTO `sys_job_log` VALUES ('1301', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:34:10');
INSERT INTO `sys_job_log` VALUES ('1302', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:34:20');
INSERT INTO `sys_job_log` VALUES ('1303', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:34:30');
INSERT INTO `sys_job_log` VALUES ('1304', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:34:40');
INSERT INTO `sys_job_log` VALUES ('1305', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:34:50');
INSERT INTO `sys_job_log` VALUES ('1306', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:35:00');
INSERT INTO `sys_job_log` VALUES ('1307', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:35:10');
INSERT INTO `sys_job_log` VALUES ('1308', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:35:20');
INSERT INTO `sys_job_log` VALUES ('1309', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:35:30');
INSERT INTO `sys_job_log` VALUES ('1310', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:35:40');
INSERT INTO `sys_job_log` VALUES ('1311', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:35:50');
INSERT INTO `sys_job_log` VALUES ('1312', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:36:00');
INSERT INTO `sys_job_log` VALUES ('1313', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:36:10');
INSERT INTO `sys_job_log` VALUES ('1314', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 12:36:20');
INSERT INTO `sys_job_log` VALUES ('1315', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:36:30');
INSERT INTO `sys_job_log` VALUES ('1316', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:36:40');
INSERT INTO `sys_job_log` VALUES ('1317', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:36:50');
INSERT INTO `sys_job_log` VALUES ('1318', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:37:00');
INSERT INTO `sys_job_log` VALUES ('1319', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:37:10');
INSERT INTO `sys_job_log` VALUES ('1320', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:37:20');
INSERT INTO `sys_job_log` VALUES ('1321', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:37:30');
INSERT INTO `sys_job_log` VALUES ('1322', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:37:40');
INSERT INTO `sys_job_log` VALUES ('1323', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 12:37:50');
INSERT INTO `sys_job_log` VALUES ('1324', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:38:00');
INSERT INTO `sys_job_log` VALUES ('1325', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:38:10');
INSERT INTO `sys_job_log` VALUES ('1326', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:38:20');
INSERT INTO `sys_job_log` VALUES ('1327', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:38:30');
INSERT INTO `sys_job_log` VALUES ('1328', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:38:40');
INSERT INTO `sys_job_log` VALUES ('1329', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:38:50');
INSERT INTO `sys_job_log` VALUES ('1330', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:39:00');
INSERT INTO `sys_job_log` VALUES ('1331', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:39:10');
INSERT INTO `sys_job_log` VALUES ('1332', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:39:20');
INSERT INTO `sys_job_log` VALUES ('1333', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:39:30');
INSERT INTO `sys_job_log` VALUES ('1334', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:39:40');
INSERT INTO `sys_job_log` VALUES ('1335', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:39:50');
INSERT INTO `sys_job_log` VALUES ('1336', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:40:00');
INSERT INTO `sys_job_log` VALUES ('1337', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:40:10');
INSERT INTO `sys_job_log` VALUES ('1338', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:40:20');
INSERT INTO `sys_job_log` VALUES ('1339', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:40:30');
INSERT INTO `sys_job_log` VALUES ('1340', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：9毫秒', '0', null, '2018-11-28 12:40:40');
INSERT INTO `sys_job_log` VALUES ('1341', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:40:50');
INSERT INTO `sys_job_log` VALUES ('1342', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:41:00');
INSERT INTO `sys_job_log` VALUES ('1343', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:41:10');
INSERT INTO `sys_job_log` VALUES ('1344', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:41:20');
INSERT INTO `sys_job_log` VALUES ('1345', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:41:30');
INSERT INTO `sys_job_log` VALUES ('1346', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:41:40');
INSERT INTO `sys_job_log` VALUES ('1347', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:41:50');
INSERT INTO `sys_job_log` VALUES ('1348', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:42:00');
INSERT INTO `sys_job_log` VALUES ('1349', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:42:10');
INSERT INTO `sys_job_log` VALUES ('1350', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:42:20');
INSERT INTO `sys_job_log` VALUES ('1351', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 12:42:30');
INSERT INTO `sys_job_log` VALUES ('1352', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:42:40');
INSERT INTO `sys_job_log` VALUES ('1353', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:42:50');
INSERT INTO `sys_job_log` VALUES ('1354', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:43:00');
INSERT INTO `sys_job_log` VALUES ('1355', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:43:10');
INSERT INTO `sys_job_log` VALUES ('1356', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:43:20');
INSERT INTO `sys_job_log` VALUES ('1357', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:43:30');
INSERT INTO `sys_job_log` VALUES ('1358', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:43:40');
INSERT INTO `sys_job_log` VALUES ('1359', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:43:50');
INSERT INTO `sys_job_log` VALUES ('1360', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:44:00');
INSERT INTO `sys_job_log` VALUES ('1361', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:44:10');
INSERT INTO `sys_job_log` VALUES ('1362', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:44:20');
INSERT INTO `sys_job_log` VALUES ('1363', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:44:30');
INSERT INTO `sys_job_log` VALUES ('1364', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:44:40');
INSERT INTO `sys_job_log` VALUES ('1365', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:44:50');
INSERT INTO `sys_job_log` VALUES ('1366', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:45:00');
INSERT INTO `sys_job_log` VALUES ('1367', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:45:10');
INSERT INTO `sys_job_log` VALUES ('1368', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:45:20');
INSERT INTO `sys_job_log` VALUES ('1369', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:45:30');
INSERT INTO `sys_job_log` VALUES ('1370', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:45:40');
INSERT INTO `sys_job_log` VALUES ('1371', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:45:50');
INSERT INTO `sys_job_log` VALUES ('1372', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:46:00');
INSERT INTO `sys_job_log` VALUES ('1373', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:46:10');
INSERT INTO `sys_job_log` VALUES ('1374', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:46:20');
INSERT INTO `sys_job_log` VALUES ('1375', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:46:30');
INSERT INTO `sys_job_log` VALUES ('1376', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 12:46:40');
INSERT INTO `sys_job_log` VALUES ('1377', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:46:50');
INSERT INTO `sys_job_log` VALUES ('1378', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:47:00');
INSERT INTO `sys_job_log` VALUES ('1379', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:47:10');
INSERT INTO `sys_job_log` VALUES ('1380', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:47:20');
INSERT INTO `sys_job_log` VALUES ('1381', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:47:30');
INSERT INTO `sys_job_log` VALUES ('1382', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 12:47:40');
INSERT INTO `sys_job_log` VALUES ('1383', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:47:50');
INSERT INTO `sys_job_log` VALUES ('1384', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:48:00');
INSERT INTO `sys_job_log` VALUES ('1385', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:48:10');
INSERT INTO `sys_job_log` VALUES ('1386', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:48:20');
INSERT INTO `sys_job_log` VALUES ('1387', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:48:30');
INSERT INTO `sys_job_log` VALUES ('1388', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:48:40');
INSERT INTO `sys_job_log` VALUES ('1389', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:48:50');
INSERT INTO `sys_job_log` VALUES ('1390', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:49:00');
INSERT INTO `sys_job_log` VALUES ('1391', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:49:10');
INSERT INTO `sys_job_log` VALUES ('1392', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:49:20');
INSERT INTO `sys_job_log` VALUES ('1393', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:49:30');
INSERT INTO `sys_job_log` VALUES ('1394', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:49:40');
INSERT INTO `sys_job_log` VALUES ('1395', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:49:50');
INSERT INTO `sys_job_log` VALUES ('1396', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:50:00');
INSERT INTO `sys_job_log` VALUES ('1397', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:50:10');
INSERT INTO `sys_job_log` VALUES ('1398', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:50:20');
INSERT INTO `sys_job_log` VALUES ('1399', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:50:30');
INSERT INTO `sys_job_log` VALUES ('1400', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:50:40');
INSERT INTO `sys_job_log` VALUES ('1401', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:50:50');
INSERT INTO `sys_job_log` VALUES ('1402', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:51:00');
INSERT INTO `sys_job_log` VALUES ('1403', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:51:10');
INSERT INTO `sys_job_log` VALUES ('1404', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:51:20');
INSERT INTO `sys_job_log` VALUES ('1405', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:51:30');
INSERT INTO `sys_job_log` VALUES ('1406', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:51:40');
INSERT INTO `sys_job_log` VALUES ('1407', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:51:50');
INSERT INTO `sys_job_log` VALUES ('1408', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:52:00');
INSERT INTO `sys_job_log` VALUES ('1409', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:52:10');
INSERT INTO `sys_job_log` VALUES ('1410', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:52:20');
INSERT INTO `sys_job_log` VALUES ('1411', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:52:30');
INSERT INTO `sys_job_log` VALUES ('1412', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:52:40');
INSERT INTO `sys_job_log` VALUES ('1413', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:52:50');
INSERT INTO `sys_job_log` VALUES ('1414', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:53:00');
INSERT INTO `sys_job_log` VALUES ('1415', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:53:10');
INSERT INTO `sys_job_log` VALUES ('1416', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:53:20');
INSERT INTO `sys_job_log` VALUES ('1417', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:53:30');
INSERT INTO `sys_job_log` VALUES ('1418', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:53:40');
INSERT INTO `sys_job_log` VALUES ('1419', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:53:50');
INSERT INTO `sys_job_log` VALUES ('1420', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 12:54:00');
INSERT INTO `sys_job_log` VALUES ('1421', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:54:10');
INSERT INTO `sys_job_log` VALUES ('1422', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:54:20');
INSERT INTO `sys_job_log` VALUES ('1423', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:54:30');
INSERT INTO `sys_job_log` VALUES ('1424', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:54:40');
INSERT INTO `sys_job_log` VALUES ('1425', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:54:50');
INSERT INTO `sys_job_log` VALUES ('1426', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:55:00');
INSERT INTO `sys_job_log` VALUES ('1427', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:55:10');
INSERT INTO `sys_job_log` VALUES ('1428', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:55:20');
INSERT INTO `sys_job_log` VALUES ('1429', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:55:30');
INSERT INTO `sys_job_log` VALUES ('1430', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:55:40');
INSERT INTO `sys_job_log` VALUES ('1431', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:55:50');
INSERT INTO `sys_job_log` VALUES ('1432', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:56:00');
INSERT INTO `sys_job_log` VALUES ('1433', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:56:10');
INSERT INTO `sys_job_log` VALUES ('1434', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:56:20');
INSERT INTO `sys_job_log` VALUES ('1435', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:56:30');
INSERT INTO `sys_job_log` VALUES ('1436', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:56:40');
INSERT INTO `sys_job_log` VALUES ('1437', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:56:50');
INSERT INTO `sys_job_log` VALUES ('1438', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:57:00');
INSERT INTO `sys_job_log` VALUES ('1439', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:57:10');
INSERT INTO `sys_job_log` VALUES ('1440', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:57:20');
INSERT INTO `sys_job_log` VALUES ('1441', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:57:30');
INSERT INTO `sys_job_log` VALUES ('1442', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:57:40');
INSERT INTO `sys_job_log` VALUES ('1443', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:57:50');
INSERT INTO `sys_job_log` VALUES ('1444', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:58:00');
INSERT INTO `sys_job_log` VALUES ('1445', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:58:10');
INSERT INTO `sys_job_log` VALUES ('1446', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:58:20');
INSERT INTO `sys_job_log` VALUES ('1447', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:58:30');
INSERT INTO `sys_job_log` VALUES ('1448', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 12:58:40');
INSERT INTO `sys_job_log` VALUES ('1449', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:58:50');
INSERT INTO `sys_job_log` VALUES ('1450', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:59:00');
INSERT INTO `sys_job_log` VALUES ('1451', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 12:59:10');
INSERT INTO `sys_job_log` VALUES ('1452', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:59:20');
INSERT INTO `sys_job_log` VALUES ('1453', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:59:30');
INSERT INTO `sys_job_log` VALUES ('1454', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:59:40');
INSERT INTO `sys_job_log` VALUES ('1455', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 12:59:50');
INSERT INTO `sys_job_log` VALUES ('1456', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:00:00');
INSERT INTO `sys_job_log` VALUES ('1457', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:00:10');
INSERT INTO `sys_job_log` VALUES ('1458', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:00:20');
INSERT INTO `sys_job_log` VALUES ('1459', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:00:30');
INSERT INTO `sys_job_log` VALUES ('1460', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:00:40');
INSERT INTO `sys_job_log` VALUES ('1461', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:00:50');
INSERT INTO `sys_job_log` VALUES ('1462', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:01:00');
INSERT INTO `sys_job_log` VALUES ('1463', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:01:10');
INSERT INTO `sys_job_log` VALUES ('1464', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:01:20');
INSERT INTO `sys_job_log` VALUES ('1465', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:01:30');
INSERT INTO `sys_job_log` VALUES ('1466', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:01:40');
INSERT INTO `sys_job_log` VALUES ('1467', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:01:50');
INSERT INTO `sys_job_log` VALUES ('1468', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:02:00');
INSERT INTO `sys_job_log` VALUES ('1469', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:02:10');
INSERT INTO `sys_job_log` VALUES ('1470', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:02:20');
INSERT INTO `sys_job_log` VALUES ('1471', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:02:30');
INSERT INTO `sys_job_log` VALUES ('1472', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:02:40');
INSERT INTO `sys_job_log` VALUES ('1473', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 13:02:50');
INSERT INTO `sys_job_log` VALUES ('1474', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:03:00');
INSERT INTO `sys_job_log` VALUES ('1475', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:03:10');
INSERT INTO `sys_job_log` VALUES ('1476', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:03:20');
INSERT INTO `sys_job_log` VALUES ('1477', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 13:03:30');
INSERT INTO `sys_job_log` VALUES ('1478', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:03:40');
INSERT INTO `sys_job_log` VALUES ('1479', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:03:50');
INSERT INTO `sys_job_log` VALUES ('1480', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:04:00');
INSERT INTO `sys_job_log` VALUES ('1481', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:04:10');
INSERT INTO `sys_job_log` VALUES ('1482', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:04:20');
INSERT INTO `sys_job_log` VALUES ('1483', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:04:30');
INSERT INTO `sys_job_log` VALUES ('1484', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:04:40');
INSERT INTO `sys_job_log` VALUES ('1485', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:04:50');
INSERT INTO `sys_job_log` VALUES ('1486', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:05:00');
INSERT INTO `sys_job_log` VALUES ('1487', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:05:10');
INSERT INTO `sys_job_log` VALUES ('1488', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:05:20');
INSERT INTO `sys_job_log` VALUES ('1489', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 13:05:30');
INSERT INTO `sys_job_log` VALUES ('1490', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:05:40');
INSERT INTO `sys_job_log` VALUES ('1491', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:05:50');
INSERT INTO `sys_job_log` VALUES ('1492', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：9毫秒', '0', null, '2018-11-28 13:06:00');
INSERT INTO `sys_job_log` VALUES ('1493', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:06:10');
INSERT INTO `sys_job_log` VALUES ('1494', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:06:20');
INSERT INTO `sys_job_log` VALUES ('1495', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:06:30');
INSERT INTO `sys_job_log` VALUES ('1496', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:06:40');
INSERT INTO `sys_job_log` VALUES ('1497', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:06:50');
INSERT INTO `sys_job_log` VALUES ('1498', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:07:00');
INSERT INTO `sys_job_log` VALUES ('1499', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:07:10');
INSERT INTO `sys_job_log` VALUES ('1500', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:07:20');
INSERT INTO `sys_job_log` VALUES ('1501', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:07:30');
INSERT INTO `sys_job_log` VALUES ('1502', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:07:40');
INSERT INTO `sys_job_log` VALUES ('1503', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:07:50');
INSERT INTO `sys_job_log` VALUES ('1504', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:08:00');
INSERT INTO `sys_job_log` VALUES ('1505', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:08:10');
INSERT INTO `sys_job_log` VALUES ('1506', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:08:20');
INSERT INTO `sys_job_log` VALUES ('1507', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:08:30');
INSERT INTO `sys_job_log` VALUES ('1508', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:08:40');
INSERT INTO `sys_job_log` VALUES ('1509', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:08:50');
INSERT INTO `sys_job_log` VALUES ('1510', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:09:00');
INSERT INTO `sys_job_log` VALUES ('1511', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:09:10');
INSERT INTO `sys_job_log` VALUES ('1512', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:09:20');
INSERT INTO `sys_job_log` VALUES ('1513', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:09:30');
INSERT INTO `sys_job_log` VALUES ('1514', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:09:40');
INSERT INTO `sys_job_log` VALUES ('1515', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:09:50');
INSERT INTO `sys_job_log` VALUES ('1516', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:10:00');
INSERT INTO `sys_job_log` VALUES ('1517', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:10:10');
INSERT INTO `sys_job_log` VALUES ('1518', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:10:20');
INSERT INTO `sys_job_log` VALUES ('1519', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:10:30');
INSERT INTO `sys_job_log` VALUES ('1520', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:10:40');
INSERT INTO `sys_job_log` VALUES ('1521', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:10:50');
INSERT INTO `sys_job_log` VALUES ('1522', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:11:00');
INSERT INTO `sys_job_log` VALUES ('1523', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:11:10');
INSERT INTO `sys_job_log` VALUES ('1524', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:11:20');
INSERT INTO `sys_job_log` VALUES ('1525', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:11:30');
INSERT INTO `sys_job_log` VALUES ('1526', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:11:40');
INSERT INTO `sys_job_log` VALUES ('1527', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:11:50');
INSERT INTO `sys_job_log` VALUES ('1528', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:12:00');
INSERT INTO `sys_job_log` VALUES ('1529', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:12:10');
INSERT INTO `sys_job_log` VALUES ('1530', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:12:20');
INSERT INTO `sys_job_log` VALUES ('1531', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:12:30');
INSERT INTO `sys_job_log` VALUES ('1532', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:12:40');
INSERT INTO `sys_job_log` VALUES ('1533', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:12:50');
INSERT INTO `sys_job_log` VALUES ('1534', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:13:00');
INSERT INTO `sys_job_log` VALUES ('1535', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:13:10');
INSERT INTO `sys_job_log` VALUES ('1536', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:13:20');
INSERT INTO `sys_job_log` VALUES ('1537', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:13:30');
INSERT INTO `sys_job_log` VALUES ('1538', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:13:40');
INSERT INTO `sys_job_log` VALUES ('1539', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:13:50');
INSERT INTO `sys_job_log` VALUES ('1540', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:14:00');
INSERT INTO `sys_job_log` VALUES ('1541', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:14:10');
INSERT INTO `sys_job_log` VALUES ('1542', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:14:20');
INSERT INTO `sys_job_log` VALUES ('1543', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:14:30');
INSERT INTO `sys_job_log` VALUES ('1544', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 13:14:40');
INSERT INTO `sys_job_log` VALUES ('1545', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:14:50');
INSERT INTO `sys_job_log` VALUES ('1546', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:15:00');
INSERT INTO `sys_job_log` VALUES ('1547', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:15:10');
INSERT INTO `sys_job_log` VALUES ('1548', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:15:20');
INSERT INTO `sys_job_log` VALUES ('1549', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:15:30');
INSERT INTO `sys_job_log` VALUES ('1550', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:15:40');
INSERT INTO `sys_job_log` VALUES ('1551', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:15:50');
INSERT INTO `sys_job_log` VALUES ('1552', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:16:00');
INSERT INTO `sys_job_log` VALUES ('1553', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:16:10');
INSERT INTO `sys_job_log` VALUES ('1554', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:16:20');
INSERT INTO `sys_job_log` VALUES ('1555', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:16:30');
INSERT INTO `sys_job_log` VALUES ('1556', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:16:40');
INSERT INTO `sys_job_log` VALUES ('1557', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:16:50');
INSERT INTO `sys_job_log` VALUES ('1558', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:17:00');
INSERT INTO `sys_job_log` VALUES ('1559', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:17:10');
INSERT INTO `sys_job_log` VALUES ('1560', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:17:20');
INSERT INTO `sys_job_log` VALUES ('1561', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:17:30');
INSERT INTO `sys_job_log` VALUES ('1562', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:17:40');
INSERT INTO `sys_job_log` VALUES ('1563', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:17:50');
INSERT INTO `sys_job_log` VALUES ('1564', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:18:00');
INSERT INTO `sys_job_log` VALUES ('1565', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:18:10');
INSERT INTO `sys_job_log` VALUES ('1566', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:18:20');
INSERT INTO `sys_job_log` VALUES ('1567', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:18:30');
INSERT INTO `sys_job_log` VALUES ('1568', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:18:40');
INSERT INTO `sys_job_log` VALUES ('1569', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:18:50');
INSERT INTO `sys_job_log` VALUES ('1570', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:19:00');
INSERT INTO `sys_job_log` VALUES ('1571', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:19:10');
INSERT INTO `sys_job_log` VALUES ('1572', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:19:20');
INSERT INTO `sys_job_log` VALUES ('1573', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:19:30');
INSERT INTO `sys_job_log` VALUES ('1574', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:19:40');
INSERT INTO `sys_job_log` VALUES ('1575', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:19:50');
INSERT INTO `sys_job_log` VALUES ('1576', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:20:00');
INSERT INTO `sys_job_log` VALUES ('1577', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:20:10');
INSERT INTO `sys_job_log` VALUES ('1578', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:20:20');
INSERT INTO `sys_job_log` VALUES ('1579', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:20:30');
INSERT INTO `sys_job_log` VALUES ('1580', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:20:40');
INSERT INTO `sys_job_log` VALUES ('1581', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:20:50');
INSERT INTO `sys_job_log` VALUES ('1582', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:21:00');
INSERT INTO `sys_job_log` VALUES ('1583', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:21:10');
INSERT INTO `sys_job_log` VALUES ('1584', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:21:20');
INSERT INTO `sys_job_log` VALUES ('1585', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:21:30');
INSERT INTO `sys_job_log` VALUES ('1586', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:21:40');
INSERT INTO `sys_job_log` VALUES ('1587', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:21:50');
INSERT INTO `sys_job_log` VALUES ('1588', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:22:00');
INSERT INTO `sys_job_log` VALUES ('1589', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:22:10');
INSERT INTO `sys_job_log` VALUES ('1590', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:22:20');
INSERT INTO `sys_job_log` VALUES ('1591', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:22:30');
INSERT INTO `sys_job_log` VALUES ('1592', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:22:40');
INSERT INTO `sys_job_log` VALUES ('1593', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:22:50');
INSERT INTO `sys_job_log` VALUES ('1594', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:23:00');
INSERT INTO `sys_job_log` VALUES ('1595', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:23:10');
INSERT INTO `sys_job_log` VALUES ('1596', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:23:20');
INSERT INTO `sys_job_log` VALUES ('1597', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:23:30');
INSERT INTO `sys_job_log` VALUES ('1598', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:23:40');
INSERT INTO `sys_job_log` VALUES ('1599', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:23:50');
INSERT INTO `sys_job_log` VALUES ('1600', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：13毫秒', '0', null, '2018-11-28 13:24:00');
INSERT INTO `sys_job_log` VALUES ('1601', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:24:10');
INSERT INTO `sys_job_log` VALUES ('1602', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:24:20');
INSERT INTO `sys_job_log` VALUES ('1603', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:24:30');
INSERT INTO `sys_job_log` VALUES ('1604', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:24:40');
INSERT INTO `sys_job_log` VALUES ('1605', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:24:50');
INSERT INTO `sys_job_log` VALUES ('1606', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:25:00');
INSERT INTO `sys_job_log` VALUES ('1607', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:25:10');
INSERT INTO `sys_job_log` VALUES ('1608', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:25:20');
INSERT INTO `sys_job_log` VALUES ('1609', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:25:30');
INSERT INTO `sys_job_log` VALUES ('1610', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:25:40');
INSERT INTO `sys_job_log` VALUES ('1611', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:25:50');
INSERT INTO `sys_job_log` VALUES ('1612', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:26:00');
INSERT INTO `sys_job_log` VALUES ('1613', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:26:10');
INSERT INTO `sys_job_log` VALUES ('1614', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:26:20');
INSERT INTO `sys_job_log` VALUES ('1615', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:26:30');
INSERT INTO `sys_job_log` VALUES ('1616', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:26:40');
INSERT INTO `sys_job_log` VALUES ('1617', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:26:50');
INSERT INTO `sys_job_log` VALUES ('1618', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:27:00');
INSERT INTO `sys_job_log` VALUES ('1619', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:27:10');
INSERT INTO `sys_job_log` VALUES ('1620', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:27:20');
INSERT INTO `sys_job_log` VALUES ('1621', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:27:30');
INSERT INTO `sys_job_log` VALUES ('1622', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:27:40');
INSERT INTO `sys_job_log` VALUES ('1623', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:27:50');
INSERT INTO `sys_job_log` VALUES ('1624', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:28:00');
INSERT INTO `sys_job_log` VALUES ('1625', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:28:10');
INSERT INTO `sys_job_log` VALUES ('1626', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:28:20');
INSERT INTO `sys_job_log` VALUES ('1627', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:28:30');
INSERT INTO `sys_job_log` VALUES ('1628', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:28:40');
INSERT INTO `sys_job_log` VALUES ('1629', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:28:50');
INSERT INTO `sys_job_log` VALUES ('1630', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:29:00');
INSERT INTO `sys_job_log` VALUES ('1631', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:29:10');
INSERT INTO `sys_job_log` VALUES ('1632', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:29:20');
INSERT INTO `sys_job_log` VALUES ('1633', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:29:30');
INSERT INTO `sys_job_log` VALUES ('1634', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:29:40');
INSERT INTO `sys_job_log` VALUES ('1635', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:29:50');
INSERT INTO `sys_job_log` VALUES ('1636', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:30:00');
INSERT INTO `sys_job_log` VALUES ('1637', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:30:10');
INSERT INTO `sys_job_log` VALUES ('1638', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:30:20');
INSERT INTO `sys_job_log` VALUES ('1639', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:30:30');
INSERT INTO `sys_job_log` VALUES ('1640', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:30:40');
INSERT INTO `sys_job_log` VALUES ('1641', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:30:50');
INSERT INTO `sys_job_log` VALUES ('1642', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:31:00');
INSERT INTO `sys_job_log` VALUES ('1643', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:31:10');
INSERT INTO `sys_job_log` VALUES ('1644', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:31:20');
INSERT INTO `sys_job_log` VALUES ('1645', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:31:30');
INSERT INTO `sys_job_log` VALUES ('1646', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:31:40');
INSERT INTO `sys_job_log` VALUES ('1647', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:31:50');
INSERT INTO `sys_job_log` VALUES ('1648', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:32:00');
INSERT INTO `sys_job_log` VALUES ('1649', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:32:10');
INSERT INTO `sys_job_log` VALUES ('1650', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:32:20');
INSERT INTO `sys_job_log` VALUES ('1651', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:32:30');
INSERT INTO `sys_job_log` VALUES ('1652', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:32:40');
INSERT INTO `sys_job_log` VALUES ('1653', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:32:50');
INSERT INTO `sys_job_log` VALUES ('1654', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:33:00');
INSERT INTO `sys_job_log` VALUES ('1655', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:33:10');
INSERT INTO `sys_job_log` VALUES ('1656', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:33:20');
INSERT INTO `sys_job_log` VALUES ('1657', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:33:30');
INSERT INTO `sys_job_log` VALUES ('1658', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:33:40');
INSERT INTO `sys_job_log` VALUES ('1659', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:33:50');
INSERT INTO `sys_job_log` VALUES ('1660', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:34:00');
INSERT INTO `sys_job_log` VALUES ('1661', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:34:10');
INSERT INTO `sys_job_log` VALUES ('1662', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:34:20');
INSERT INTO `sys_job_log` VALUES ('1663', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:34:30');
INSERT INTO `sys_job_log` VALUES ('1664', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:34:40');
INSERT INTO `sys_job_log` VALUES ('1665', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:34:50');
INSERT INTO `sys_job_log` VALUES ('1666', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:35:00');
INSERT INTO `sys_job_log` VALUES ('1667', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:35:10');
INSERT INTO `sys_job_log` VALUES ('1668', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:35:20');
INSERT INTO `sys_job_log` VALUES ('1669', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:35:30');
INSERT INTO `sys_job_log` VALUES ('1670', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:35:40');
INSERT INTO `sys_job_log` VALUES ('1671', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:35:50');
INSERT INTO `sys_job_log` VALUES ('1672', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:36:00');
INSERT INTO `sys_job_log` VALUES ('1673', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:36:10');
INSERT INTO `sys_job_log` VALUES ('1674', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:36:20');
INSERT INTO `sys_job_log` VALUES ('1675', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:36:30');
INSERT INTO `sys_job_log` VALUES ('1676', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:36:40');
INSERT INTO `sys_job_log` VALUES ('1677', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 13:36:50');
INSERT INTO `sys_job_log` VALUES ('1678', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:37:00');
INSERT INTO `sys_job_log` VALUES ('1679', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:37:10');
INSERT INTO `sys_job_log` VALUES ('1680', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:37:20');
INSERT INTO `sys_job_log` VALUES ('1681', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 13:37:30');
INSERT INTO `sys_job_log` VALUES ('1682', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:37:40');
INSERT INTO `sys_job_log` VALUES ('1683', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:37:50');
INSERT INTO `sys_job_log` VALUES ('1684', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:38:00');
INSERT INTO `sys_job_log` VALUES ('1685', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:38:10');
INSERT INTO `sys_job_log` VALUES ('1686', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:38:20');
INSERT INTO `sys_job_log` VALUES ('1687', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:38:30');
INSERT INTO `sys_job_log` VALUES ('1688', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:38:40');
INSERT INTO `sys_job_log` VALUES ('1689', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:38:50');
INSERT INTO `sys_job_log` VALUES ('1690', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:39:00');
INSERT INTO `sys_job_log` VALUES ('1691', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:39:10');
INSERT INTO `sys_job_log` VALUES ('1692', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:39:20');
INSERT INTO `sys_job_log` VALUES ('1693', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:39:30');
INSERT INTO `sys_job_log` VALUES ('1694', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:39:40');
INSERT INTO `sys_job_log` VALUES ('1695', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:39:50');
INSERT INTO `sys_job_log` VALUES ('1696', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:40:00');
INSERT INTO `sys_job_log` VALUES ('1697', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:40:10');
INSERT INTO `sys_job_log` VALUES ('1698', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:40:20');
INSERT INTO `sys_job_log` VALUES ('1699', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:40:30');
INSERT INTO `sys_job_log` VALUES ('1700', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:40:40');
INSERT INTO `sys_job_log` VALUES ('1701', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:40:50');
INSERT INTO `sys_job_log` VALUES ('1702', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:41:00');
INSERT INTO `sys_job_log` VALUES ('1703', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:41:10');
INSERT INTO `sys_job_log` VALUES ('1704', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:41:20');
INSERT INTO `sys_job_log` VALUES ('1705', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:41:30');
INSERT INTO `sys_job_log` VALUES ('1706', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:41:40');
INSERT INTO `sys_job_log` VALUES ('1707', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:41:50');
INSERT INTO `sys_job_log` VALUES ('1708', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:42:00');
INSERT INTO `sys_job_log` VALUES ('1709', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 13:42:10');
INSERT INTO `sys_job_log` VALUES ('1710', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 13:42:20');
INSERT INTO `sys_job_log` VALUES ('1711', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:42:30');
INSERT INTO `sys_job_log` VALUES ('1712', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:42:40');
INSERT INTO `sys_job_log` VALUES ('1713', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:42:50');
INSERT INTO `sys_job_log` VALUES ('1714', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:43:00');
INSERT INTO `sys_job_log` VALUES ('1715', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:43:10');
INSERT INTO `sys_job_log` VALUES ('1716', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：16毫秒', '0', null, '2018-11-28 13:43:20');
INSERT INTO `sys_job_log` VALUES ('1717', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 13:43:30');
INSERT INTO `sys_job_log` VALUES ('1718', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:43:40');
INSERT INTO `sys_job_log` VALUES ('1719', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:43:50');
INSERT INTO `sys_job_log` VALUES ('1720', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:44:00');
INSERT INTO `sys_job_log` VALUES ('1721', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:44:10');
INSERT INTO `sys_job_log` VALUES ('1722', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:44:20');
INSERT INTO `sys_job_log` VALUES ('1723', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:44:30');
INSERT INTO `sys_job_log` VALUES ('1724', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:44:40');
INSERT INTO `sys_job_log` VALUES ('1725', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:44:50');
INSERT INTO `sys_job_log` VALUES ('1726', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 13:45:00');
INSERT INTO `sys_job_log` VALUES ('1727', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:45:10');
INSERT INTO `sys_job_log` VALUES ('1728', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:45:20');
INSERT INTO `sys_job_log` VALUES ('1729', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:45:30');
INSERT INTO `sys_job_log` VALUES ('1730', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:45:40');
INSERT INTO `sys_job_log` VALUES ('1731', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:45:50');
INSERT INTO `sys_job_log` VALUES ('1732', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:46:00');
INSERT INTO `sys_job_log` VALUES ('1733', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:46:10');
INSERT INTO `sys_job_log` VALUES ('1734', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:46:20');
INSERT INTO `sys_job_log` VALUES ('1735', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:46:30');
INSERT INTO `sys_job_log` VALUES ('1736', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:46:40');
INSERT INTO `sys_job_log` VALUES ('1737', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:46:50');
INSERT INTO `sys_job_log` VALUES ('1738', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:47:00');
INSERT INTO `sys_job_log` VALUES ('1739', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:47:10');
INSERT INTO `sys_job_log` VALUES ('1740', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:47:20');
INSERT INTO `sys_job_log` VALUES ('1741', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：10毫秒', '0', null, '2018-11-28 13:47:30');
INSERT INTO `sys_job_log` VALUES ('1742', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:47:40');
INSERT INTO `sys_job_log` VALUES ('1743', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:47:50');
INSERT INTO `sys_job_log` VALUES ('1744', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:48:00');
INSERT INTO `sys_job_log` VALUES ('1745', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:48:10');
INSERT INTO `sys_job_log` VALUES ('1746', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:48:20');
INSERT INTO `sys_job_log` VALUES ('1747', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:48:30');
INSERT INTO `sys_job_log` VALUES ('1748', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:48:40');
INSERT INTO `sys_job_log` VALUES ('1749', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:48:50');
INSERT INTO `sys_job_log` VALUES ('1750', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:49:00');
INSERT INTO `sys_job_log` VALUES ('1751', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:49:10');
INSERT INTO `sys_job_log` VALUES ('1752', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:49:20');
INSERT INTO `sys_job_log` VALUES ('1753', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:49:30');
INSERT INTO `sys_job_log` VALUES ('1754', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:49:40');
INSERT INTO `sys_job_log` VALUES ('1755', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:49:50');
INSERT INTO `sys_job_log` VALUES ('1756', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:50:00');
INSERT INTO `sys_job_log` VALUES ('1757', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:50:10');
INSERT INTO `sys_job_log` VALUES ('1758', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:50:20');
INSERT INTO `sys_job_log` VALUES ('1759', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:50:30');
INSERT INTO `sys_job_log` VALUES ('1760', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:50:40');
INSERT INTO `sys_job_log` VALUES ('1761', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:50:50');
INSERT INTO `sys_job_log` VALUES ('1762', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:51:00');
INSERT INTO `sys_job_log` VALUES ('1763', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:51:10');
INSERT INTO `sys_job_log` VALUES ('1764', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:51:20');
INSERT INTO `sys_job_log` VALUES ('1765', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:51:30');
INSERT INTO `sys_job_log` VALUES ('1766', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:51:40');
INSERT INTO `sys_job_log` VALUES ('1767', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:51:50');
INSERT INTO `sys_job_log` VALUES ('1768', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:52:00');
INSERT INTO `sys_job_log` VALUES ('1769', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:52:10');
INSERT INTO `sys_job_log` VALUES ('1770', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:52:20');
INSERT INTO `sys_job_log` VALUES ('1771', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:52:30');
INSERT INTO `sys_job_log` VALUES ('1772', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:52:40');
INSERT INTO `sys_job_log` VALUES ('1773', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:52:50');
INSERT INTO `sys_job_log` VALUES ('1774', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:53:00');
INSERT INTO `sys_job_log` VALUES ('1775', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:53:10');
INSERT INTO `sys_job_log` VALUES ('1776', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:53:20');
INSERT INTO `sys_job_log` VALUES ('1777', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:53:30');
INSERT INTO `sys_job_log` VALUES ('1778', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:53:40');
INSERT INTO `sys_job_log` VALUES ('1779', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 13:53:50');
INSERT INTO `sys_job_log` VALUES ('1780', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:54:00');
INSERT INTO `sys_job_log` VALUES ('1781', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:54:10');
INSERT INTO `sys_job_log` VALUES ('1782', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:54:20');
INSERT INTO `sys_job_log` VALUES ('1783', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:54:30');
INSERT INTO `sys_job_log` VALUES ('1784', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:54:40');
INSERT INTO `sys_job_log` VALUES ('1785', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:54:50');
INSERT INTO `sys_job_log` VALUES ('1786', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:55:00');
INSERT INTO `sys_job_log` VALUES ('1787', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:55:10');
INSERT INTO `sys_job_log` VALUES ('1788', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:55:20');
INSERT INTO `sys_job_log` VALUES ('1789', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:55:30');
INSERT INTO `sys_job_log` VALUES ('1790', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:55:40');
INSERT INTO `sys_job_log` VALUES ('1791', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:55:50');
INSERT INTO `sys_job_log` VALUES ('1792', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:56:00');
INSERT INTO `sys_job_log` VALUES ('1793', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:56:10');
INSERT INTO `sys_job_log` VALUES ('1794', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:56:20');
INSERT INTO `sys_job_log` VALUES ('1795', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:56:30');
INSERT INTO `sys_job_log` VALUES ('1796', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 13:56:40');
INSERT INTO `sys_job_log` VALUES ('1797', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:56:50');
INSERT INTO `sys_job_log` VALUES ('1798', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:57:00');
INSERT INTO `sys_job_log` VALUES ('1799', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:57:10');
INSERT INTO `sys_job_log` VALUES ('1800', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:57:20');
INSERT INTO `sys_job_log` VALUES ('1801', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:57:30');
INSERT INTO `sys_job_log` VALUES ('1802', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:57:40');
INSERT INTO `sys_job_log` VALUES ('1803', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 13:57:50');
INSERT INTO `sys_job_log` VALUES ('1804', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:58:00');
INSERT INTO `sys_job_log` VALUES ('1805', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:58:10');
INSERT INTO `sys_job_log` VALUES ('1806', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 13:58:20');
INSERT INTO `sys_job_log` VALUES ('1807', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:58:30');
INSERT INTO `sys_job_log` VALUES ('1808', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:58:40');
INSERT INTO `sys_job_log` VALUES ('1809', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:58:50');
INSERT INTO `sys_job_log` VALUES ('1810', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:59:00');
INSERT INTO `sys_job_log` VALUES ('1811', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:59:10');
INSERT INTO `sys_job_log` VALUES ('1812', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:59:20');
INSERT INTO `sys_job_log` VALUES ('1813', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 13:59:30');
INSERT INTO `sys_job_log` VALUES ('1814', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:59:40');
INSERT INTO `sys_job_log` VALUES ('1815', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 13:59:50');
INSERT INTO `sys_job_log` VALUES ('1816', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:00:00');
INSERT INTO `sys_job_log` VALUES ('1817', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:00:10');
INSERT INTO `sys_job_log` VALUES ('1818', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:00:20');
INSERT INTO `sys_job_log` VALUES ('1819', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:00:30');
INSERT INTO `sys_job_log` VALUES ('1820', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:00:40');
INSERT INTO `sys_job_log` VALUES ('1821', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:00:50');
INSERT INTO `sys_job_log` VALUES ('1822', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:01:00');
INSERT INTO `sys_job_log` VALUES ('1823', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:01:10');
INSERT INTO `sys_job_log` VALUES ('1824', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:01:20');
INSERT INTO `sys_job_log` VALUES ('1825', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:01:30');
INSERT INTO `sys_job_log` VALUES ('1826', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:01:40');
INSERT INTO `sys_job_log` VALUES ('1827', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:01:50');
INSERT INTO `sys_job_log` VALUES ('1828', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:02:00');
INSERT INTO `sys_job_log` VALUES ('1829', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:02:10');
INSERT INTO `sys_job_log` VALUES ('1830', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:02:20');
INSERT INTO `sys_job_log` VALUES ('1831', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:02:30');
INSERT INTO `sys_job_log` VALUES ('1832', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:02:40');
INSERT INTO `sys_job_log` VALUES ('1833', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:02:50');
INSERT INTO `sys_job_log` VALUES ('1834', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:03:00');
INSERT INTO `sys_job_log` VALUES ('1835', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:03:10');
INSERT INTO `sys_job_log` VALUES ('1836', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:03:20');
INSERT INTO `sys_job_log` VALUES ('1837', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:03:30');
INSERT INTO `sys_job_log` VALUES ('1838', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:03:40');
INSERT INTO `sys_job_log` VALUES ('1839', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:03:50');
INSERT INTO `sys_job_log` VALUES ('1840', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:04:00');
INSERT INTO `sys_job_log` VALUES ('1841', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:04:10');
INSERT INTO `sys_job_log` VALUES ('1842', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:04:20');
INSERT INTO `sys_job_log` VALUES ('1843', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:04:30');
INSERT INTO `sys_job_log` VALUES ('1844', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:04:40');
INSERT INTO `sys_job_log` VALUES ('1845', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:04:50');
INSERT INTO `sys_job_log` VALUES ('1846', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:05:00');
INSERT INTO `sys_job_log` VALUES ('1847', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:05:10');
INSERT INTO `sys_job_log` VALUES ('1848', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:05:20');
INSERT INTO `sys_job_log` VALUES ('1849', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:05:30');
INSERT INTO `sys_job_log` VALUES ('1850', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:05:40');
INSERT INTO `sys_job_log` VALUES ('1851', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:05:50');
INSERT INTO `sys_job_log` VALUES ('1852', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:06:00');
INSERT INTO `sys_job_log` VALUES ('1853', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:06:10');
INSERT INTO `sys_job_log` VALUES ('1854', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:06:20');
INSERT INTO `sys_job_log` VALUES ('1855', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:06:30');
INSERT INTO `sys_job_log` VALUES ('1856', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:06:40');
INSERT INTO `sys_job_log` VALUES ('1857', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:06:50');
INSERT INTO `sys_job_log` VALUES ('1858', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:07:00');
INSERT INTO `sys_job_log` VALUES ('1859', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:07:10');
INSERT INTO `sys_job_log` VALUES ('1860', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:07:20');
INSERT INTO `sys_job_log` VALUES ('1861', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:07:30');
INSERT INTO `sys_job_log` VALUES ('1862', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:07:40');
INSERT INTO `sys_job_log` VALUES ('1863', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:07:50');
INSERT INTO `sys_job_log` VALUES ('1864', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:08:00');
INSERT INTO `sys_job_log` VALUES ('1865', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:08:10');
INSERT INTO `sys_job_log` VALUES ('1866', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:08:20');
INSERT INTO `sys_job_log` VALUES ('1867', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:08:30');
INSERT INTO `sys_job_log` VALUES ('1868', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:08:40');
INSERT INTO `sys_job_log` VALUES ('1869', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:08:50');
INSERT INTO `sys_job_log` VALUES ('1870', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:09:00');
INSERT INTO `sys_job_log` VALUES ('1871', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:09:10');
INSERT INTO `sys_job_log` VALUES ('1872', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:09:20');
INSERT INTO `sys_job_log` VALUES ('1873', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:09:30');
INSERT INTO `sys_job_log` VALUES ('1874', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:09:40');
INSERT INTO `sys_job_log` VALUES ('1875', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:09:50');
INSERT INTO `sys_job_log` VALUES ('1876', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:10:00');
INSERT INTO `sys_job_log` VALUES ('1877', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:10:10');
INSERT INTO `sys_job_log` VALUES ('1878', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:10:20');
INSERT INTO `sys_job_log` VALUES ('1879', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:10:30');
INSERT INTO `sys_job_log` VALUES ('1880', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:10:40');
INSERT INTO `sys_job_log` VALUES ('1881', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:10:50');
INSERT INTO `sys_job_log` VALUES ('1882', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:11:00');
INSERT INTO `sys_job_log` VALUES ('1883', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:11:10');
INSERT INTO `sys_job_log` VALUES ('1884', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:11:20');
INSERT INTO `sys_job_log` VALUES ('1885', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:11:30');
INSERT INTO `sys_job_log` VALUES ('1886', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:11:40');
INSERT INTO `sys_job_log` VALUES ('1887', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:11:50');
INSERT INTO `sys_job_log` VALUES ('1888', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:12:00');
INSERT INTO `sys_job_log` VALUES ('1889', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：18毫秒', '0', null, '2018-11-28 14:12:10');
INSERT INTO `sys_job_log` VALUES ('1890', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:12:20');
INSERT INTO `sys_job_log` VALUES ('1891', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:12:30');
INSERT INTO `sys_job_log` VALUES ('1892', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:12:40');
INSERT INTO `sys_job_log` VALUES ('1893', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 14:12:50');
INSERT INTO `sys_job_log` VALUES ('1894', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:13:00');
INSERT INTO `sys_job_log` VALUES ('1895', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:13:10');
INSERT INTO `sys_job_log` VALUES ('1896', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:13:20');
INSERT INTO `sys_job_log` VALUES ('1897', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:13:30');
INSERT INTO `sys_job_log` VALUES ('1898', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:13:40');
INSERT INTO `sys_job_log` VALUES ('1899', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:13:50');
INSERT INTO `sys_job_log` VALUES ('1900', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:14:00');
INSERT INTO `sys_job_log` VALUES ('1901', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:14:10');
INSERT INTO `sys_job_log` VALUES ('1902', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:14:20');
INSERT INTO `sys_job_log` VALUES ('1903', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:14:30');
INSERT INTO `sys_job_log` VALUES ('1904', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:14:40');
INSERT INTO `sys_job_log` VALUES ('1905', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 14:14:50');
INSERT INTO `sys_job_log` VALUES ('1906', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：9毫秒', '0', null, '2018-11-28 14:15:00');
INSERT INTO `sys_job_log` VALUES ('1907', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:15:10');
INSERT INTO `sys_job_log` VALUES ('1908', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:15:20');
INSERT INTO `sys_job_log` VALUES ('1909', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:15:30');
INSERT INTO `sys_job_log` VALUES ('1910', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:15:40');
INSERT INTO `sys_job_log` VALUES ('1911', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:15:50');
INSERT INTO `sys_job_log` VALUES ('1912', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:16:00');
INSERT INTO `sys_job_log` VALUES ('1913', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:16:10');
INSERT INTO `sys_job_log` VALUES ('1914', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:16:20');
INSERT INTO `sys_job_log` VALUES ('1915', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:16:30');
INSERT INTO `sys_job_log` VALUES ('1916', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:16:40');
INSERT INTO `sys_job_log` VALUES ('1917', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:16:50');
INSERT INTO `sys_job_log` VALUES ('1918', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:17:00');
INSERT INTO `sys_job_log` VALUES ('1919', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:17:10');
INSERT INTO `sys_job_log` VALUES ('1920', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:17:20');
INSERT INTO `sys_job_log` VALUES ('1921', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:17:30');
INSERT INTO `sys_job_log` VALUES ('1922', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:17:40');
INSERT INTO `sys_job_log` VALUES ('1923', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:17:50');
INSERT INTO `sys_job_log` VALUES ('1924', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:18:00');
INSERT INTO `sys_job_log` VALUES ('1925', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:18:10');
INSERT INTO `sys_job_log` VALUES ('1926', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:18:20');
INSERT INTO `sys_job_log` VALUES ('1927', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:18:30');
INSERT INTO `sys_job_log` VALUES ('1928', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:18:40');
INSERT INTO `sys_job_log` VALUES ('1929', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:18:50');
INSERT INTO `sys_job_log` VALUES ('1930', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:19:00');
INSERT INTO `sys_job_log` VALUES ('1931', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:19:10');
INSERT INTO `sys_job_log` VALUES ('1932', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:19:20');
INSERT INTO `sys_job_log` VALUES ('1933', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:19:30');
INSERT INTO `sys_job_log` VALUES ('1934', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:19:40');
INSERT INTO `sys_job_log` VALUES ('1935', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:19:50');
INSERT INTO `sys_job_log` VALUES ('1936', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:20:00');
INSERT INTO `sys_job_log` VALUES ('1937', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:20:10');
INSERT INTO `sys_job_log` VALUES ('1938', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:20:20');
INSERT INTO `sys_job_log` VALUES ('1939', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:20:30');
INSERT INTO `sys_job_log` VALUES ('1940', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 14:20:40');
INSERT INTO `sys_job_log` VALUES ('1941', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:20:50');
INSERT INTO `sys_job_log` VALUES ('1942', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:21:00');
INSERT INTO `sys_job_log` VALUES ('1943', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:21:10');
INSERT INTO `sys_job_log` VALUES ('1944', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:21:20');
INSERT INTO `sys_job_log` VALUES ('1945', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 14:21:30');
INSERT INTO `sys_job_log` VALUES ('1946', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:21:40');
INSERT INTO `sys_job_log` VALUES ('1947', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:21:50');
INSERT INTO `sys_job_log` VALUES ('1948', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:22:00');
INSERT INTO `sys_job_log` VALUES ('1949', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:22:10');
INSERT INTO `sys_job_log` VALUES ('1950', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:22:20');
INSERT INTO `sys_job_log` VALUES ('1951', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:22:30');
INSERT INTO `sys_job_log` VALUES ('1952', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:22:40');
INSERT INTO `sys_job_log` VALUES ('1953', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:22:50');
INSERT INTO `sys_job_log` VALUES ('1954', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:23:00');
INSERT INTO `sys_job_log` VALUES ('1955', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:23:10');
INSERT INTO `sys_job_log` VALUES ('1956', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:23:20');
INSERT INTO `sys_job_log` VALUES ('1957', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:23:30');
INSERT INTO `sys_job_log` VALUES ('1958', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:23:40');
INSERT INTO `sys_job_log` VALUES ('1959', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:23:50');
INSERT INTO `sys_job_log` VALUES ('1960', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:24:00');
INSERT INTO `sys_job_log` VALUES ('1961', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:24:10');
INSERT INTO `sys_job_log` VALUES ('1962', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:24:20');
INSERT INTO `sys_job_log` VALUES ('1963', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:24:30');
INSERT INTO `sys_job_log` VALUES ('1964', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:24:40');
INSERT INTO `sys_job_log` VALUES ('1965', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:24:50');
INSERT INTO `sys_job_log` VALUES ('1966', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:25:00');
INSERT INTO `sys_job_log` VALUES ('1967', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:25:10');
INSERT INTO `sys_job_log` VALUES ('1968', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:25:20');
INSERT INTO `sys_job_log` VALUES ('1969', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:25:30');
INSERT INTO `sys_job_log` VALUES ('1970', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:25:40');
INSERT INTO `sys_job_log` VALUES ('1971', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:25:50');
INSERT INTO `sys_job_log` VALUES ('1972', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:26:00');
INSERT INTO `sys_job_log` VALUES ('1973', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:26:10');
INSERT INTO `sys_job_log` VALUES ('1974', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:26:20');
INSERT INTO `sys_job_log` VALUES ('1975', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:26:30');
INSERT INTO `sys_job_log` VALUES ('1976', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:26:40');
INSERT INTO `sys_job_log` VALUES ('1977', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:26:50');
INSERT INTO `sys_job_log` VALUES ('1978', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:27:00');
INSERT INTO `sys_job_log` VALUES ('1979', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:27:10');
INSERT INTO `sys_job_log` VALUES ('1980', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:27:20');
INSERT INTO `sys_job_log` VALUES ('1981', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:27:30');
INSERT INTO `sys_job_log` VALUES ('1982', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:27:40');
INSERT INTO `sys_job_log` VALUES ('1983', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:27:50');
INSERT INTO `sys_job_log` VALUES ('1984', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:28:00');
INSERT INTO `sys_job_log` VALUES ('1985', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:28:10');
INSERT INTO `sys_job_log` VALUES ('1986', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:28:20');
INSERT INTO `sys_job_log` VALUES ('1987', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:28:30');
INSERT INTO `sys_job_log` VALUES ('1988', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:28:40');
INSERT INTO `sys_job_log` VALUES ('1989', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:28:50');
INSERT INTO `sys_job_log` VALUES ('1990', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:29:00');
INSERT INTO `sys_job_log` VALUES ('1991', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:29:10');
INSERT INTO `sys_job_log` VALUES ('1992', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:29:20');
INSERT INTO `sys_job_log` VALUES ('1993', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:29:30');
INSERT INTO `sys_job_log` VALUES ('1994', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:29:40');
INSERT INTO `sys_job_log` VALUES ('1995', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:29:50');
INSERT INTO `sys_job_log` VALUES ('1996', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:30:00');
INSERT INTO `sys_job_log` VALUES ('1997', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:30:10');
INSERT INTO `sys_job_log` VALUES ('1998', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:30:20');
INSERT INTO `sys_job_log` VALUES ('1999', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:30:30');
INSERT INTO `sys_job_log` VALUES ('2000', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:30:40');
INSERT INTO `sys_job_log` VALUES ('2001', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:30:50');
INSERT INTO `sys_job_log` VALUES ('2002', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:31:00');
INSERT INTO `sys_job_log` VALUES ('2003', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:31:10');
INSERT INTO `sys_job_log` VALUES ('2004', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:31:20');
INSERT INTO `sys_job_log` VALUES ('2005', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:31:30');
INSERT INTO `sys_job_log` VALUES ('2006', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:31:40');
INSERT INTO `sys_job_log` VALUES ('2007', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:31:50');
INSERT INTO `sys_job_log` VALUES ('2008', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：11毫秒', '0', null, '2018-11-28 14:32:00');
INSERT INTO `sys_job_log` VALUES ('2009', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:32:10');
INSERT INTO `sys_job_log` VALUES ('2010', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:32:20');
INSERT INTO `sys_job_log` VALUES ('2011', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:32:30');
INSERT INTO `sys_job_log` VALUES ('2012', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:32:40');
INSERT INTO `sys_job_log` VALUES ('2013', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:32:50');
INSERT INTO `sys_job_log` VALUES ('2014', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:33:00');
INSERT INTO `sys_job_log` VALUES ('2015', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:33:10');
INSERT INTO `sys_job_log` VALUES ('2016', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:33:20');
INSERT INTO `sys_job_log` VALUES ('2017', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:33:30');
INSERT INTO `sys_job_log` VALUES ('2018', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:33:40');
INSERT INTO `sys_job_log` VALUES ('2019', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:33:50');
INSERT INTO `sys_job_log` VALUES ('2020', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:34:00');
INSERT INTO `sys_job_log` VALUES ('2021', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:34:10');
INSERT INTO `sys_job_log` VALUES ('2022', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:34:20');
INSERT INTO `sys_job_log` VALUES ('2023', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:34:30');
INSERT INTO `sys_job_log` VALUES ('2024', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:34:40');
INSERT INTO `sys_job_log` VALUES ('2025', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:34:50');
INSERT INTO `sys_job_log` VALUES ('2026', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:35:00');
INSERT INTO `sys_job_log` VALUES ('2027', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:35:10');
INSERT INTO `sys_job_log` VALUES ('2028', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:35:20');
INSERT INTO `sys_job_log` VALUES ('2029', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:35:30');
INSERT INTO `sys_job_log` VALUES ('2030', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:35:40');
INSERT INTO `sys_job_log` VALUES ('2031', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:35:50');
INSERT INTO `sys_job_log` VALUES ('2032', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:36:00');
INSERT INTO `sys_job_log` VALUES ('2033', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:36:10');
INSERT INTO `sys_job_log` VALUES ('2034', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:36:20');
INSERT INTO `sys_job_log` VALUES ('2035', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:36:30');
INSERT INTO `sys_job_log` VALUES ('2036', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:36:40');
INSERT INTO `sys_job_log` VALUES ('2037', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:36:50');
INSERT INTO `sys_job_log` VALUES ('2038', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:37:00');
INSERT INTO `sys_job_log` VALUES ('2039', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:37:10');
INSERT INTO `sys_job_log` VALUES ('2040', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:37:20');
INSERT INTO `sys_job_log` VALUES ('2041', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:37:30');
INSERT INTO `sys_job_log` VALUES ('2042', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:37:40');
INSERT INTO `sys_job_log` VALUES ('2043', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:37:50');
INSERT INTO `sys_job_log` VALUES ('2044', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:38:00');
INSERT INTO `sys_job_log` VALUES ('2045', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:38:10');
INSERT INTO `sys_job_log` VALUES ('2046', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:38:20');
INSERT INTO `sys_job_log` VALUES ('2047', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:38:30');
INSERT INTO `sys_job_log` VALUES ('2048', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:38:40');
INSERT INTO `sys_job_log` VALUES ('2049', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:38:50');
INSERT INTO `sys_job_log` VALUES ('2050', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:39:00');
INSERT INTO `sys_job_log` VALUES ('2051', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:39:10');
INSERT INTO `sys_job_log` VALUES ('2052', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:39:20');
INSERT INTO `sys_job_log` VALUES ('2053', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 14:39:30');
INSERT INTO `sys_job_log` VALUES ('2054', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:39:40');
INSERT INTO `sys_job_log` VALUES ('2055', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:39:50');
INSERT INTO `sys_job_log` VALUES ('2056', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:40:00');
INSERT INTO `sys_job_log` VALUES ('2057', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：13毫秒', '0', null, '2018-11-28 14:40:10');
INSERT INTO `sys_job_log` VALUES ('2058', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:40:20');
INSERT INTO `sys_job_log` VALUES ('2059', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:40:30');
INSERT INTO `sys_job_log` VALUES ('2060', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:40:40');
INSERT INTO `sys_job_log` VALUES ('2061', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:40:50');
INSERT INTO `sys_job_log` VALUES ('2062', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:41:00');
INSERT INTO `sys_job_log` VALUES ('2063', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:41:10');
INSERT INTO `sys_job_log` VALUES ('2064', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:41:20');
INSERT INTO `sys_job_log` VALUES ('2065', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:41:30');
INSERT INTO `sys_job_log` VALUES ('2066', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:41:40');
INSERT INTO `sys_job_log` VALUES ('2067', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:41:50');
INSERT INTO `sys_job_log` VALUES ('2068', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:42:00');
INSERT INTO `sys_job_log` VALUES ('2069', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:42:10');
INSERT INTO `sys_job_log` VALUES ('2070', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:42:20');
INSERT INTO `sys_job_log` VALUES ('2071', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:42:30');
INSERT INTO `sys_job_log` VALUES ('2072', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:42:40');
INSERT INTO `sys_job_log` VALUES ('2073', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:42:50');
INSERT INTO `sys_job_log` VALUES ('2074', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:43:00');
INSERT INTO `sys_job_log` VALUES ('2075', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:43:10');
INSERT INTO `sys_job_log` VALUES ('2076', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:43:20');
INSERT INTO `sys_job_log` VALUES ('2077', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:43:30');
INSERT INTO `sys_job_log` VALUES ('2078', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:43:40');
INSERT INTO `sys_job_log` VALUES ('2079', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:43:50');
INSERT INTO `sys_job_log` VALUES ('2080', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:44:00');
INSERT INTO `sys_job_log` VALUES ('2081', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:44:10');
INSERT INTO `sys_job_log` VALUES ('2082', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:44:20');
INSERT INTO `sys_job_log` VALUES ('2083', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:44:30');
INSERT INTO `sys_job_log` VALUES ('2084', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:44:40');
INSERT INTO `sys_job_log` VALUES ('2085', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:44:50');
INSERT INTO `sys_job_log` VALUES ('2086', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:45:00');
INSERT INTO `sys_job_log` VALUES ('2087', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:45:10');
INSERT INTO `sys_job_log` VALUES ('2088', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:45:20');
INSERT INTO `sys_job_log` VALUES ('2089', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:45:30');
INSERT INTO `sys_job_log` VALUES ('2090', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:45:40');
INSERT INTO `sys_job_log` VALUES ('2091', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:45:50');
INSERT INTO `sys_job_log` VALUES ('2092', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:46:00');
INSERT INTO `sys_job_log` VALUES ('2093', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:46:10');
INSERT INTO `sys_job_log` VALUES ('2094', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:46:20');
INSERT INTO `sys_job_log` VALUES ('2095', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:46:30');
INSERT INTO `sys_job_log` VALUES ('2096', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:46:40');
INSERT INTO `sys_job_log` VALUES ('2097', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:46:50');
INSERT INTO `sys_job_log` VALUES ('2098', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:47:00');
INSERT INTO `sys_job_log` VALUES ('2099', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:47:10');
INSERT INTO `sys_job_log` VALUES ('2100', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:47:20');
INSERT INTO `sys_job_log` VALUES ('2101', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:47:30');
INSERT INTO `sys_job_log` VALUES ('2102', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:47:40');
INSERT INTO `sys_job_log` VALUES ('2103', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:47:50');
INSERT INTO `sys_job_log` VALUES ('2104', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:48:00');
INSERT INTO `sys_job_log` VALUES ('2105', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:48:10');
INSERT INTO `sys_job_log` VALUES ('2106', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:48:20');
INSERT INTO `sys_job_log` VALUES ('2107', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:48:30');
INSERT INTO `sys_job_log` VALUES ('2108', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:48:40');
INSERT INTO `sys_job_log` VALUES ('2109', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:48:50');
INSERT INTO `sys_job_log` VALUES ('2110', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:49:00');
INSERT INTO `sys_job_log` VALUES ('2111', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:49:10');
INSERT INTO `sys_job_log` VALUES ('2112', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:49:20');
INSERT INTO `sys_job_log` VALUES ('2113', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:49:30');
INSERT INTO `sys_job_log` VALUES ('2114', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:49:40');
INSERT INTO `sys_job_log` VALUES ('2115', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:49:50');
INSERT INTO `sys_job_log` VALUES ('2116', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:50:00');
INSERT INTO `sys_job_log` VALUES ('2117', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:50:10');
INSERT INTO `sys_job_log` VALUES ('2118', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:50:20');
INSERT INTO `sys_job_log` VALUES ('2119', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:50:30');
INSERT INTO `sys_job_log` VALUES ('2120', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:50:40');
INSERT INTO `sys_job_log` VALUES ('2121', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:50:50');
INSERT INTO `sys_job_log` VALUES ('2122', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:51:00');
INSERT INTO `sys_job_log` VALUES ('2123', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:51:10');
INSERT INTO `sys_job_log` VALUES ('2124', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:51:20');
INSERT INTO `sys_job_log` VALUES ('2125', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:51:30');
INSERT INTO `sys_job_log` VALUES ('2126', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:51:40');
INSERT INTO `sys_job_log` VALUES ('2127', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:51:50');
INSERT INTO `sys_job_log` VALUES ('2128', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:52:00');
INSERT INTO `sys_job_log` VALUES ('2129', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:52:10');
INSERT INTO `sys_job_log` VALUES ('2130', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:52:20');
INSERT INTO `sys_job_log` VALUES ('2131', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：38毫秒', '0', null, '2018-11-28 14:52:30');
INSERT INTO `sys_job_log` VALUES ('2132', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:52:40');
INSERT INTO `sys_job_log` VALUES ('2133', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:52:50');
INSERT INTO `sys_job_log` VALUES ('2134', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:53:00');
INSERT INTO `sys_job_log` VALUES ('2135', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:53:10');
INSERT INTO `sys_job_log` VALUES ('2136', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:53:20');
INSERT INTO `sys_job_log` VALUES ('2137', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:53:30');
INSERT INTO `sys_job_log` VALUES ('2138', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：23毫秒', '0', null, '2018-11-28 14:53:40');
INSERT INTO `sys_job_log` VALUES ('2139', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:53:50');
INSERT INTO `sys_job_log` VALUES ('2140', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:54:00');
INSERT INTO `sys_job_log` VALUES ('2141', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:54:10');
INSERT INTO `sys_job_log` VALUES ('2142', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:54:20');
INSERT INTO `sys_job_log` VALUES ('2143', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:54:30');
INSERT INTO `sys_job_log` VALUES ('2144', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:54:40');
INSERT INTO `sys_job_log` VALUES ('2145', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 14:54:50');
INSERT INTO `sys_job_log` VALUES ('2146', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 14:55:00');
INSERT INTO `sys_job_log` VALUES ('2147', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:55:10');
INSERT INTO `sys_job_log` VALUES ('2148', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:55:20');
INSERT INTO `sys_job_log` VALUES ('2149', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:55:30');
INSERT INTO `sys_job_log` VALUES ('2150', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:55:40');
INSERT INTO `sys_job_log` VALUES ('2151', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:55:50');
INSERT INTO `sys_job_log` VALUES ('2152', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:56:00');
INSERT INTO `sys_job_log` VALUES ('2153', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:56:10');
INSERT INTO `sys_job_log` VALUES ('2154', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:56:20');
INSERT INTO `sys_job_log` VALUES ('2155', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 14:56:30');
INSERT INTO `sys_job_log` VALUES ('2156', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:56:40');
INSERT INTO `sys_job_log` VALUES ('2157', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:56:50');
INSERT INTO `sys_job_log` VALUES ('2158', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:57:00');
INSERT INTO `sys_job_log` VALUES ('2159', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:57:10');
INSERT INTO `sys_job_log` VALUES ('2160', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:57:20');
INSERT INTO `sys_job_log` VALUES ('2161', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 14:57:30');
INSERT INTO `sys_job_log` VALUES ('2162', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:57:40');
INSERT INTO `sys_job_log` VALUES ('2163', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:57:50');
INSERT INTO `sys_job_log` VALUES ('2164', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:58:00');
INSERT INTO `sys_job_log` VALUES ('2165', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：8毫秒', '0', null, '2018-11-28 14:58:10');
INSERT INTO `sys_job_log` VALUES ('2166', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 14:58:20');
INSERT INTO `sys_job_log` VALUES ('2167', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 14:58:30');
INSERT INTO `sys_job_log` VALUES ('2168', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:58:40');
INSERT INTO `sys_job_log` VALUES ('2169', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:58:50');
INSERT INTO `sys_job_log` VALUES ('2170', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:59:00');
INSERT INTO `sys_job_log` VALUES ('2171', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:59:10');
INSERT INTO `sys_job_log` VALUES ('2172', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:59:20');
INSERT INTO `sys_job_log` VALUES ('2173', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:59:30');
INSERT INTO `sys_job_log` VALUES ('2174', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:59:40');
INSERT INTO `sys_job_log` VALUES ('2175', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 14:59:50');
INSERT INTO `sys_job_log` VALUES ('2176', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:00:00');
INSERT INTO `sys_job_log` VALUES ('2177', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 15:00:10');
INSERT INTO `sys_job_log` VALUES ('2178', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:00:20');
INSERT INTO `sys_job_log` VALUES ('2179', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:00:30');
INSERT INTO `sys_job_log` VALUES ('2180', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:00:40');
INSERT INTO `sys_job_log` VALUES ('2181', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:00:50');
INSERT INTO `sys_job_log` VALUES ('2182', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:01:00');
INSERT INTO `sys_job_log` VALUES ('2183', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:01:10');
INSERT INTO `sys_job_log` VALUES ('2184', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:01:20');
INSERT INTO `sys_job_log` VALUES ('2185', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:01:30');
INSERT INTO `sys_job_log` VALUES ('2186', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:01:40');
INSERT INTO `sys_job_log` VALUES ('2187', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:01:50');
INSERT INTO `sys_job_log` VALUES ('2188', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:02:00');
INSERT INTO `sys_job_log` VALUES ('2189', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:02:10');
INSERT INTO `sys_job_log` VALUES ('2190', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:02:20');
INSERT INTO `sys_job_log` VALUES ('2191', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:02:30');
INSERT INTO `sys_job_log` VALUES ('2192', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:02:40');
INSERT INTO `sys_job_log` VALUES ('2193', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:02:50');
INSERT INTO `sys_job_log` VALUES ('2194', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:03:00');
INSERT INTO `sys_job_log` VALUES ('2195', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:03:10');
INSERT INTO `sys_job_log` VALUES ('2196', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:03:20');
INSERT INTO `sys_job_log` VALUES ('2197', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:03:30');
INSERT INTO `sys_job_log` VALUES ('2198', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:03:40');
INSERT INTO `sys_job_log` VALUES ('2199', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:03:50');
INSERT INTO `sys_job_log` VALUES ('2200', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:04:00');
INSERT INTO `sys_job_log` VALUES ('2201', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:04:10');
INSERT INTO `sys_job_log` VALUES ('2202', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:04:20');
INSERT INTO `sys_job_log` VALUES ('2203', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:04:30');
INSERT INTO `sys_job_log` VALUES ('2204', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:04:40');
INSERT INTO `sys_job_log` VALUES ('2205', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:04:50');
INSERT INTO `sys_job_log` VALUES ('2206', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:05:00');
INSERT INTO `sys_job_log` VALUES ('2207', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:05:10');
INSERT INTO `sys_job_log` VALUES ('2208', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:05:20');
INSERT INTO `sys_job_log` VALUES ('2209', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:05:30');
INSERT INTO `sys_job_log` VALUES ('2210', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 15:05:40');
INSERT INTO `sys_job_log` VALUES ('2211', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:05:50');
INSERT INTO `sys_job_log` VALUES ('2212', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:06:00');
INSERT INTO `sys_job_log` VALUES ('2213', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:06:10');
INSERT INTO `sys_job_log` VALUES ('2214', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:06:20');
INSERT INTO `sys_job_log` VALUES ('2215', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:06:30');
INSERT INTO `sys_job_log` VALUES ('2216', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:06:40');
INSERT INTO `sys_job_log` VALUES ('2217', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：14毫秒', '0', null, '2018-11-28 15:06:50');
INSERT INTO `sys_job_log` VALUES ('2218', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:07:00');
INSERT INTO `sys_job_log` VALUES ('2219', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:07:10');
INSERT INTO `sys_job_log` VALUES ('2220', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:07:20');
INSERT INTO `sys_job_log` VALUES ('2221', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:07:30');
INSERT INTO `sys_job_log` VALUES ('2222', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:07:40');
INSERT INTO `sys_job_log` VALUES ('2223', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:07:50');
INSERT INTO `sys_job_log` VALUES ('2224', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:08:00');
INSERT INTO `sys_job_log` VALUES ('2225', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:08:10');
INSERT INTO `sys_job_log` VALUES ('2226', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:08:20');
INSERT INTO `sys_job_log` VALUES ('2227', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:08:30');
INSERT INTO `sys_job_log` VALUES ('2228', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：9毫秒', '0', null, '2018-11-28 15:08:40');
INSERT INTO `sys_job_log` VALUES ('2229', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:08:50');
INSERT INTO `sys_job_log` VALUES ('2230', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:09:00');
INSERT INTO `sys_job_log` VALUES ('2231', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:09:10');
INSERT INTO `sys_job_log` VALUES ('2232', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:09:20');
INSERT INTO `sys_job_log` VALUES ('2233', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 15:09:30');
INSERT INTO `sys_job_log` VALUES ('2234', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:09:40');
INSERT INTO `sys_job_log` VALUES ('2235', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:09:50');
INSERT INTO `sys_job_log` VALUES ('2236', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:10:00');
INSERT INTO `sys_job_log` VALUES ('2237', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:10:10');
INSERT INTO `sys_job_log` VALUES ('2238', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:10:20');
INSERT INTO `sys_job_log` VALUES ('2239', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:10:30');
INSERT INTO `sys_job_log` VALUES ('2240', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:10:40');
INSERT INTO `sys_job_log` VALUES ('2241', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:10:50');
INSERT INTO `sys_job_log` VALUES ('2242', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:11:00');
INSERT INTO `sys_job_log` VALUES ('2243', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:11:10');
INSERT INTO `sys_job_log` VALUES ('2244', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:11:20');
INSERT INTO `sys_job_log` VALUES ('2245', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:11:30');
INSERT INTO `sys_job_log` VALUES ('2246', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:11:40');
INSERT INTO `sys_job_log` VALUES ('2247', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:11:50');
INSERT INTO `sys_job_log` VALUES ('2248', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：9毫秒', '0', null, '2018-11-28 15:12:00');
INSERT INTO `sys_job_log` VALUES ('2249', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:12:10');
INSERT INTO `sys_job_log` VALUES ('2250', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:12:20');
INSERT INTO `sys_job_log` VALUES ('2251', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:12:30');
INSERT INTO `sys_job_log` VALUES ('2252', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:12:40');
INSERT INTO `sys_job_log` VALUES ('2253', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:12:50');
INSERT INTO `sys_job_log` VALUES ('2254', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:13:00');
INSERT INTO `sys_job_log` VALUES ('2255', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:13:10');
INSERT INTO `sys_job_log` VALUES ('2256', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:13:20');
INSERT INTO `sys_job_log` VALUES ('2257', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:13:30');
INSERT INTO `sys_job_log` VALUES ('2258', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:13:40');
INSERT INTO `sys_job_log` VALUES ('2259', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:13:50');
INSERT INTO `sys_job_log` VALUES ('2260', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:14:00');
INSERT INTO `sys_job_log` VALUES ('2261', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:14:10');
INSERT INTO `sys_job_log` VALUES ('2262', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:14:20');
INSERT INTO `sys_job_log` VALUES ('2263', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:14:30');
INSERT INTO `sys_job_log` VALUES ('2264', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:14:40');
INSERT INTO `sys_job_log` VALUES ('2265', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 15:14:50');
INSERT INTO `sys_job_log` VALUES ('2266', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:15:00');
INSERT INTO `sys_job_log` VALUES ('2267', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:15:10');
INSERT INTO `sys_job_log` VALUES ('2268', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:15:20');
INSERT INTO `sys_job_log` VALUES ('2269', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:15:30');
INSERT INTO `sys_job_log` VALUES ('2270', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:15:40');
INSERT INTO `sys_job_log` VALUES ('2271', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:15:50');
INSERT INTO `sys_job_log` VALUES ('2272', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:16:00');
INSERT INTO `sys_job_log` VALUES ('2273', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:16:10');
INSERT INTO `sys_job_log` VALUES ('2274', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:16:20');
INSERT INTO `sys_job_log` VALUES ('2275', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:16:30');
INSERT INTO `sys_job_log` VALUES ('2276', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:16:40');
INSERT INTO `sys_job_log` VALUES ('2277', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:16:50');
INSERT INTO `sys_job_log` VALUES ('2278', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:17:00');
INSERT INTO `sys_job_log` VALUES ('2279', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:17:10');
INSERT INTO `sys_job_log` VALUES ('2280', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:17:20');
INSERT INTO `sys_job_log` VALUES ('2281', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:17:30');
INSERT INTO `sys_job_log` VALUES ('2282', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:17:40');
INSERT INTO `sys_job_log` VALUES ('2283', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:17:50');
INSERT INTO `sys_job_log` VALUES ('2284', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:18:00');
INSERT INTO `sys_job_log` VALUES ('2285', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:18:10');
INSERT INTO `sys_job_log` VALUES ('2286', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:18:20');
INSERT INTO `sys_job_log` VALUES ('2287', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:18:30');
INSERT INTO `sys_job_log` VALUES ('2288', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:18:40');
INSERT INTO `sys_job_log` VALUES ('2289', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:18:50');
INSERT INTO `sys_job_log` VALUES ('2290', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:19:00');
INSERT INTO `sys_job_log` VALUES ('2291', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:19:10');
INSERT INTO `sys_job_log` VALUES ('2292', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:19:20');
INSERT INTO `sys_job_log` VALUES ('2293', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:19:30');
INSERT INTO `sys_job_log` VALUES ('2294', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:19:40');
INSERT INTO `sys_job_log` VALUES ('2295', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:19:50');
INSERT INTO `sys_job_log` VALUES ('2296', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:20:00');
INSERT INTO `sys_job_log` VALUES ('2297', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:20:10');
INSERT INTO `sys_job_log` VALUES ('2298', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:20:20');
INSERT INTO `sys_job_log` VALUES ('2299', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:20:30');
INSERT INTO `sys_job_log` VALUES ('2300', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:20:40');
INSERT INTO `sys_job_log` VALUES ('2301', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:20:50');
INSERT INTO `sys_job_log` VALUES ('2302', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:21:00');
INSERT INTO `sys_job_log` VALUES ('2303', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:21:10');
INSERT INTO `sys_job_log` VALUES ('2304', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:21:20');
INSERT INTO `sys_job_log` VALUES ('2305', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:21:30');
INSERT INTO `sys_job_log` VALUES ('2306', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:21:40');
INSERT INTO `sys_job_log` VALUES ('2307', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:21:50');
INSERT INTO `sys_job_log` VALUES ('2308', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:22:00');
INSERT INTO `sys_job_log` VALUES ('2309', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:22:10');
INSERT INTO `sys_job_log` VALUES ('2310', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:22:20');
INSERT INTO `sys_job_log` VALUES ('2311', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:22:30');
INSERT INTO `sys_job_log` VALUES ('2312', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:22:40');
INSERT INTO `sys_job_log` VALUES ('2313', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:22:50');
INSERT INTO `sys_job_log` VALUES ('2314', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:23:00');
INSERT INTO `sys_job_log` VALUES ('2315', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:23:10');
INSERT INTO `sys_job_log` VALUES ('2316', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:23:20');
INSERT INTO `sys_job_log` VALUES ('2317', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:23:30');
INSERT INTO `sys_job_log` VALUES ('2318', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:23:40');
INSERT INTO `sys_job_log` VALUES ('2319', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:23:50');
INSERT INTO `sys_job_log` VALUES ('2320', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:24:00');
INSERT INTO `sys_job_log` VALUES ('2321', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:24:10');
INSERT INTO `sys_job_log` VALUES ('2322', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:24:20');
INSERT INTO `sys_job_log` VALUES ('2323', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:24:30');
INSERT INTO `sys_job_log` VALUES ('2324', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:24:40');
INSERT INTO `sys_job_log` VALUES ('2325', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:24:50');
INSERT INTO `sys_job_log` VALUES ('2326', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:25:00');
INSERT INTO `sys_job_log` VALUES ('2327', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:25:10');
INSERT INTO `sys_job_log` VALUES ('2328', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:25:20');
INSERT INTO `sys_job_log` VALUES ('2329', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:25:30');
INSERT INTO `sys_job_log` VALUES ('2330', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:25:40');
INSERT INTO `sys_job_log` VALUES ('2331', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:25:50');
INSERT INTO `sys_job_log` VALUES ('2332', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:26:00');
INSERT INTO `sys_job_log` VALUES ('2333', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:26:10');
INSERT INTO `sys_job_log` VALUES ('2334', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:26:20');
INSERT INTO `sys_job_log` VALUES ('2335', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:26:30');
INSERT INTO `sys_job_log` VALUES ('2336', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:26:40');
INSERT INTO `sys_job_log` VALUES ('2337', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:26:50');
INSERT INTO `sys_job_log` VALUES ('2338', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:27:00');
INSERT INTO `sys_job_log` VALUES ('2339', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:27:10');
INSERT INTO `sys_job_log` VALUES ('2340', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:27:20');
INSERT INTO `sys_job_log` VALUES ('2341', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:27:30');
INSERT INTO `sys_job_log` VALUES ('2342', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:27:40');
INSERT INTO `sys_job_log` VALUES ('2343', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:27:50');
INSERT INTO `sys_job_log` VALUES ('2344', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:28:00');
INSERT INTO `sys_job_log` VALUES ('2345', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：10毫秒', '0', null, '2018-11-28 15:28:10');
INSERT INTO `sys_job_log` VALUES ('2346', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:28:20');
INSERT INTO `sys_job_log` VALUES ('2347', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:28:30');
INSERT INTO `sys_job_log` VALUES ('2348', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:28:40');
INSERT INTO `sys_job_log` VALUES ('2349', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:28:50');
INSERT INTO `sys_job_log` VALUES ('2350', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:29:00');
INSERT INTO `sys_job_log` VALUES ('2351', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:29:10');
INSERT INTO `sys_job_log` VALUES ('2352', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:29:20');
INSERT INTO `sys_job_log` VALUES ('2353', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:29:30');
INSERT INTO `sys_job_log` VALUES ('2354', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:29:40');
INSERT INTO `sys_job_log` VALUES ('2355', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:29:50');
INSERT INTO `sys_job_log` VALUES ('2356', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:30:00');
INSERT INTO `sys_job_log` VALUES ('2357', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:30:10');
INSERT INTO `sys_job_log` VALUES ('2358', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:30:20');
INSERT INTO `sys_job_log` VALUES ('2359', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:30:30');
INSERT INTO `sys_job_log` VALUES ('2360', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:30:40');
INSERT INTO `sys_job_log` VALUES ('2361', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:30:50');
INSERT INTO `sys_job_log` VALUES ('2362', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:31:00');
INSERT INTO `sys_job_log` VALUES ('2363', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:31:10');
INSERT INTO `sys_job_log` VALUES ('2364', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 15:31:20');
INSERT INTO `sys_job_log` VALUES ('2365', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:31:30');
INSERT INTO `sys_job_log` VALUES ('2366', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:31:40');
INSERT INTO `sys_job_log` VALUES ('2367', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:31:50');
INSERT INTO `sys_job_log` VALUES ('2368', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:32:00');
INSERT INTO `sys_job_log` VALUES ('2369', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:32:10');
INSERT INTO `sys_job_log` VALUES ('2370', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:32:20');
INSERT INTO `sys_job_log` VALUES ('2371', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:32:30');
INSERT INTO `sys_job_log` VALUES ('2372', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:32:40');
INSERT INTO `sys_job_log` VALUES ('2373', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：11毫秒', '0', null, '2018-11-28 15:32:50');
INSERT INTO `sys_job_log` VALUES ('2374', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:33:00');
INSERT INTO `sys_job_log` VALUES ('2375', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:33:10');
INSERT INTO `sys_job_log` VALUES ('2376', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:33:20');
INSERT INTO `sys_job_log` VALUES ('2377', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:33:30');
INSERT INTO `sys_job_log` VALUES ('2378', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:33:40');
INSERT INTO `sys_job_log` VALUES ('2379', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：10毫秒', '0', null, '2018-11-28 15:33:50');
INSERT INTO `sys_job_log` VALUES ('2380', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:34:00');
INSERT INTO `sys_job_log` VALUES ('2381', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:34:10');
INSERT INTO `sys_job_log` VALUES ('2382', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:34:20');
INSERT INTO `sys_job_log` VALUES ('2383', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:34:30');
INSERT INTO `sys_job_log` VALUES ('2384', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:34:40');
INSERT INTO `sys_job_log` VALUES ('2385', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:34:50');
INSERT INTO `sys_job_log` VALUES ('2386', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:35:00');
INSERT INTO `sys_job_log` VALUES ('2387', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:35:10');
INSERT INTO `sys_job_log` VALUES ('2388', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:35:20');
INSERT INTO `sys_job_log` VALUES ('2389', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:35:30');
INSERT INTO `sys_job_log` VALUES ('2390', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:35:40');
INSERT INTO `sys_job_log` VALUES ('2391', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:35:50');
INSERT INTO `sys_job_log` VALUES ('2392', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：20毫秒', '0', null, '2018-11-28 15:36:00');
INSERT INTO `sys_job_log` VALUES ('2393', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:36:10');
INSERT INTO `sys_job_log` VALUES ('2394', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:36:20');
INSERT INTO `sys_job_log` VALUES ('2395', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:36:30');
INSERT INTO `sys_job_log` VALUES ('2396', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:36:40');
INSERT INTO `sys_job_log` VALUES ('2397', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:36:50');
INSERT INTO `sys_job_log` VALUES ('2398', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:37:00');
INSERT INTO `sys_job_log` VALUES ('2399', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:37:10');
INSERT INTO `sys_job_log` VALUES ('2400', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:37:20');
INSERT INTO `sys_job_log` VALUES ('2401', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：8毫秒', '0', null, '2018-11-28 15:37:30');
INSERT INTO `sys_job_log` VALUES ('2402', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:37:40');
INSERT INTO `sys_job_log` VALUES ('2403', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:37:50');
INSERT INTO `sys_job_log` VALUES ('2404', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：8毫秒', '0', null, '2018-11-28 15:38:00');
INSERT INTO `sys_job_log` VALUES ('2405', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:38:10');
INSERT INTO `sys_job_log` VALUES ('2406', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:38:20');
INSERT INTO `sys_job_log` VALUES ('2407', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:38:30');
INSERT INTO `sys_job_log` VALUES ('2408', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:38:40');
INSERT INTO `sys_job_log` VALUES ('2409', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:38:50');
INSERT INTO `sys_job_log` VALUES ('2410', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:39:00');
INSERT INTO `sys_job_log` VALUES ('2411', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:39:10');
INSERT INTO `sys_job_log` VALUES ('2412', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:39:20');
INSERT INTO `sys_job_log` VALUES ('2413', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:39:30');
INSERT INTO `sys_job_log` VALUES ('2414', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:39:40');
INSERT INTO `sys_job_log` VALUES ('2415', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:39:50');
INSERT INTO `sys_job_log` VALUES ('2416', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 15:40:00');
INSERT INTO `sys_job_log` VALUES ('2417', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:40:10');
INSERT INTO `sys_job_log` VALUES ('2418', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:40:20');
INSERT INTO `sys_job_log` VALUES ('2419', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:40:30');
INSERT INTO `sys_job_log` VALUES ('2420', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:40:40');
INSERT INTO `sys_job_log` VALUES ('2421', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:40:50');
INSERT INTO `sys_job_log` VALUES ('2422', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:41:00');
INSERT INTO `sys_job_log` VALUES ('2423', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:41:10');
INSERT INTO `sys_job_log` VALUES ('2424', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:41:20');
INSERT INTO `sys_job_log` VALUES ('2425', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:41:30');
INSERT INTO `sys_job_log` VALUES ('2426', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:41:40');
INSERT INTO `sys_job_log` VALUES ('2427', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:41:50');
INSERT INTO `sys_job_log` VALUES ('2428', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:42:00');
INSERT INTO `sys_job_log` VALUES ('2429', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:42:10');
INSERT INTO `sys_job_log` VALUES ('2430', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:42:20');
INSERT INTO `sys_job_log` VALUES ('2431', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:42:30');
INSERT INTO `sys_job_log` VALUES ('2432', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:42:40');
INSERT INTO `sys_job_log` VALUES ('2433', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 15:42:50');
INSERT INTO `sys_job_log` VALUES ('2434', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:43:00');
INSERT INTO `sys_job_log` VALUES ('2435', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:43:10');
INSERT INTO `sys_job_log` VALUES ('2436', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:43:20');
INSERT INTO `sys_job_log` VALUES ('2437', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:43:30');
INSERT INTO `sys_job_log` VALUES ('2438', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:43:40');
INSERT INTO `sys_job_log` VALUES ('2439', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:43:50');
INSERT INTO `sys_job_log` VALUES ('2440', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：8毫秒', '0', null, '2018-11-28 15:44:00');
INSERT INTO `sys_job_log` VALUES ('2441', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:44:10');
INSERT INTO `sys_job_log` VALUES ('2442', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:44:20');
INSERT INTO `sys_job_log` VALUES ('2443', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:44:30');
INSERT INTO `sys_job_log` VALUES ('2444', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:44:40');
INSERT INTO `sys_job_log` VALUES ('2445', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:44:50');
INSERT INTO `sys_job_log` VALUES ('2446', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:45:00');
INSERT INTO `sys_job_log` VALUES ('2447', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:45:10');
INSERT INTO `sys_job_log` VALUES ('2448', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:45:20');
INSERT INTO `sys_job_log` VALUES ('2449', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:45:30');
INSERT INTO `sys_job_log` VALUES ('2450', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:45:40');
INSERT INTO `sys_job_log` VALUES ('2451', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:45:50');
INSERT INTO `sys_job_log` VALUES ('2452', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:46:00');
INSERT INTO `sys_job_log` VALUES ('2453', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:46:10');
INSERT INTO `sys_job_log` VALUES ('2454', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:46:20');
INSERT INTO `sys_job_log` VALUES ('2455', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:46:30');
INSERT INTO `sys_job_log` VALUES ('2456', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:46:40');
INSERT INTO `sys_job_log` VALUES ('2457', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:46:50');
INSERT INTO `sys_job_log` VALUES ('2458', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:47:00');
INSERT INTO `sys_job_log` VALUES ('2459', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:47:10');
INSERT INTO `sys_job_log` VALUES ('2460', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:47:20');
INSERT INTO `sys_job_log` VALUES ('2461', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:47:30');
INSERT INTO `sys_job_log` VALUES ('2462', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:47:40');
INSERT INTO `sys_job_log` VALUES ('2463', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:47:50');
INSERT INTO `sys_job_log` VALUES ('2464', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:48:00');
INSERT INTO `sys_job_log` VALUES ('2465', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:48:10');
INSERT INTO `sys_job_log` VALUES ('2466', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:48:20');
INSERT INTO `sys_job_log` VALUES ('2467', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:48:30');
INSERT INTO `sys_job_log` VALUES ('2468', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:48:40');
INSERT INTO `sys_job_log` VALUES ('2469', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:48:50');
INSERT INTO `sys_job_log` VALUES ('2470', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:49:00');
INSERT INTO `sys_job_log` VALUES ('2471', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：12毫秒', '0', null, '2018-11-28 15:49:10');
INSERT INTO `sys_job_log` VALUES ('2472', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:49:20');
INSERT INTO `sys_job_log` VALUES ('2473', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:49:30');
INSERT INTO `sys_job_log` VALUES ('2474', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:49:40');
INSERT INTO `sys_job_log` VALUES ('2475', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:49:50');
INSERT INTO `sys_job_log` VALUES ('2476', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:50:00');
INSERT INTO `sys_job_log` VALUES ('2477', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:50:10');
INSERT INTO `sys_job_log` VALUES ('2478', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:50:20');
INSERT INTO `sys_job_log` VALUES ('2479', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:50:30');
INSERT INTO `sys_job_log` VALUES ('2480', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:50:40');
INSERT INTO `sys_job_log` VALUES ('2481', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:50:50');
INSERT INTO `sys_job_log` VALUES ('2482', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:51:00');
INSERT INTO `sys_job_log` VALUES ('2483', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:51:10');
INSERT INTO `sys_job_log` VALUES ('2484', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:51:20');
INSERT INTO `sys_job_log` VALUES ('2485', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:51:30');
INSERT INTO `sys_job_log` VALUES ('2486', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:51:40');
INSERT INTO `sys_job_log` VALUES ('2487', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:51:50');
INSERT INTO `sys_job_log` VALUES ('2488', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:52:00');
INSERT INTO `sys_job_log` VALUES ('2489', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:52:10');
INSERT INTO `sys_job_log` VALUES ('2490', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:52:20');
INSERT INTO `sys_job_log` VALUES ('2491', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:52:30');
INSERT INTO `sys_job_log` VALUES ('2492', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:52:40');
INSERT INTO `sys_job_log` VALUES ('2493', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:52:50');
INSERT INTO `sys_job_log` VALUES ('2494', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:53:00');
INSERT INTO `sys_job_log` VALUES ('2495', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:53:10');
INSERT INTO `sys_job_log` VALUES ('2496', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:53:20');
INSERT INTO `sys_job_log` VALUES ('2497', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:53:30');
INSERT INTO `sys_job_log` VALUES ('2498', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:53:40');
INSERT INTO `sys_job_log` VALUES ('2499', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:53:50');
INSERT INTO `sys_job_log` VALUES ('2500', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:54:00');
INSERT INTO `sys_job_log` VALUES ('2501', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:54:10');
INSERT INTO `sys_job_log` VALUES ('2502', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:54:20');
INSERT INTO `sys_job_log` VALUES ('2503', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:54:30');
INSERT INTO `sys_job_log` VALUES ('2504', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:54:40');
INSERT INTO `sys_job_log` VALUES ('2505', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:54:50');
INSERT INTO `sys_job_log` VALUES ('2506', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:55:00');
INSERT INTO `sys_job_log` VALUES ('2507', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:55:10');
INSERT INTO `sys_job_log` VALUES ('2508', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:55:20');
INSERT INTO `sys_job_log` VALUES ('2509', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:55:30');
INSERT INTO `sys_job_log` VALUES ('2510', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:55:40');
INSERT INTO `sys_job_log` VALUES ('2511', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:55:50');
INSERT INTO `sys_job_log` VALUES ('2512', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:56:00');
INSERT INTO `sys_job_log` VALUES ('2513', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:56:10');
INSERT INTO `sys_job_log` VALUES ('2514', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:56:20');
INSERT INTO `sys_job_log` VALUES ('2515', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:56:30');
INSERT INTO `sys_job_log` VALUES ('2516', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:56:40');
INSERT INTO `sys_job_log` VALUES ('2517', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:56:50');
INSERT INTO `sys_job_log` VALUES ('2518', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:57:00');
INSERT INTO `sys_job_log` VALUES ('2519', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:57:10');
INSERT INTO `sys_job_log` VALUES ('2520', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:57:20');
INSERT INTO `sys_job_log` VALUES ('2521', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:57:30');
INSERT INTO `sys_job_log` VALUES ('2522', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:57:40');
INSERT INTO `sys_job_log` VALUES ('2523', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:57:50');
INSERT INTO `sys_job_log` VALUES ('2524', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:58:00');
INSERT INTO `sys_job_log` VALUES ('2525', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:58:10');
INSERT INTO `sys_job_log` VALUES ('2526', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:58:20');
INSERT INTO `sys_job_log` VALUES ('2527', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:58:30');
INSERT INTO `sys_job_log` VALUES ('2528', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 15:58:40');
INSERT INTO `sys_job_log` VALUES ('2529', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:58:50');
INSERT INTO `sys_job_log` VALUES ('2530', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:59:00');
INSERT INTO `sys_job_log` VALUES ('2531', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:59:10');
INSERT INTO `sys_job_log` VALUES ('2532', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 15:59:20');
INSERT INTO `sys_job_log` VALUES ('2533', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:59:30');
INSERT INTO `sys_job_log` VALUES ('2534', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 15:59:40');
INSERT INTO `sys_job_log` VALUES ('2535', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 15:59:50');
INSERT INTO `sys_job_log` VALUES ('2536', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:00:00');
INSERT INTO `sys_job_log` VALUES ('2537', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:00:10');
INSERT INTO `sys_job_log` VALUES ('2538', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:00:20');
INSERT INTO `sys_job_log` VALUES ('2539', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:00:30');
INSERT INTO `sys_job_log` VALUES ('2540', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:00:40');
INSERT INTO `sys_job_log` VALUES ('2541', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:00:50');
INSERT INTO `sys_job_log` VALUES ('2542', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:01:00');
INSERT INTO `sys_job_log` VALUES ('2543', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:01:10');
INSERT INTO `sys_job_log` VALUES ('2544', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:01:20');
INSERT INTO `sys_job_log` VALUES ('2545', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:01:30');
INSERT INTO `sys_job_log` VALUES ('2546', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:01:40');
INSERT INTO `sys_job_log` VALUES ('2547', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:01:50');
INSERT INTO `sys_job_log` VALUES ('2548', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:02:00');
INSERT INTO `sys_job_log` VALUES ('2549', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:02:10');
INSERT INTO `sys_job_log` VALUES ('2550', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:02:20');
INSERT INTO `sys_job_log` VALUES ('2551', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:02:30');
INSERT INTO `sys_job_log` VALUES ('2552', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:02:40');
INSERT INTO `sys_job_log` VALUES ('2553', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:02:50');
INSERT INTO `sys_job_log` VALUES ('2554', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:03:00');
INSERT INTO `sys_job_log` VALUES ('2555', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:03:10');
INSERT INTO `sys_job_log` VALUES ('2556', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:03:20');
INSERT INTO `sys_job_log` VALUES ('2557', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:03:30');
INSERT INTO `sys_job_log` VALUES ('2558', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:03:40');
INSERT INTO `sys_job_log` VALUES ('2559', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:03:50');
INSERT INTO `sys_job_log` VALUES ('2560', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:04:00');
INSERT INTO `sys_job_log` VALUES ('2561', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:04:10');
INSERT INTO `sys_job_log` VALUES ('2562', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:04:20');
INSERT INTO `sys_job_log` VALUES ('2563', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:04:30');
INSERT INTO `sys_job_log` VALUES ('2564', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:04:40');
INSERT INTO `sys_job_log` VALUES ('2565', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:04:50');
INSERT INTO `sys_job_log` VALUES ('2566', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:05:00');
INSERT INTO `sys_job_log` VALUES ('2567', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:05:10');
INSERT INTO `sys_job_log` VALUES ('2568', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:05:20');
INSERT INTO `sys_job_log` VALUES ('2569', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:05:30');
INSERT INTO `sys_job_log` VALUES ('2570', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:05:40');
INSERT INTO `sys_job_log` VALUES ('2571', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:05:50');
INSERT INTO `sys_job_log` VALUES ('2572', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:06:00');
INSERT INTO `sys_job_log` VALUES ('2573', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:06:10');
INSERT INTO `sys_job_log` VALUES ('2574', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:06:20');
INSERT INTO `sys_job_log` VALUES ('2575', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:06:30');
INSERT INTO `sys_job_log` VALUES ('2576', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:06:40');
INSERT INTO `sys_job_log` VALUES ('2577', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:06:50');
INSERT INTO `sys_job_log` VALUES ('2578', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:07:00');
INSERT INTO `sys_job_log` VALUES ('2579', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:07:10');
INSERT INTO `sys_job_log` VALUES ('2580', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:07:20');
INSERT INTO `sys_job_log` VALUES ('2581', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:07:30');
INSERT INTO `sys_job_log` VALUES ('2582', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:07:40');
INSERT INTO `sys_job_log` VALUES ('2583', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:07:50');
INSERT INTO `sys_job_log` VALUES ('2584', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:08:00');
INSERT INTO `sys_job_log` VALUES ('2585', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:08:10');
INSERT INTO `sys_job_log` VALUES ('2586', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:08:20');
INSERT INTO `sys_job_log` VALUES ('2587', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:08:30');
INSERT INTO `sys_job_log` VALUES ('2588', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:08:40');
INSERT INTO `sys_job_log` VALUES ('2589', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:08:50');
INSERT INTO `sys_job_log` VALUES ('2590', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:09:00');
INSERT INTO `sys_job_log` VALUES ('2591', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:09:10');
INSERT INTO `sys_job_log` VALUES ('2592', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:09:20');
INSERT INTO `sys_job_log` VALUES ('2593', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:09:30');
INSERT INTO `sys_job_log` VALUES ('2594', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:09:40');
INSERT INTO `sys_job_log` VALUES ('2595', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:09:50');
INSERT INTO `sys_job_log` VALUES ('2596', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:10:00');
INSERT INTO `sys_job_log` VALUES ('2597', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:10:10');
INSERT INTO `sys_job_log` VALUES ('2598', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:10:20');
INSERT INTO `sys_job_log` VALUES ('2599', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:10:30');
INSERT INTO `sys_job_log` VALUES ('2600', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:10:40');
INSERT INTO `sys_job_log` VALUES ('2601', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:10:50');
INSERT INTO `sys_job_log` VALUES ('2602', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:11:00');
INSERT INTO `sys_job_log` VALUES ('2603', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:11:10');
INSERT INTO `sys_job_log` VALUES ('2604', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:11:20');
INSERT INTO `sys_job_log` VALUES ('2605', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:11:30');
INSERT INTO `sys_job_log` VALUES ('2606', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:11:40');
INSERT INTO `sys_job_log` VALUES ('2607', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:11:50');
INSERT INTO `sys_job_log` VALUES ('2608', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:12:00');
INSERT INTO `sys_job_log` VALUES ('2609', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:12:10');
INSERT INTO `sys_job_log` VALUES ('2610', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:12:20');
INSERT INTO `sys_job_log` VALUES ('2611', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:12:30');
INSERT INTO `sys_job_log` VALUES ('2612', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:12:40');
INSERT INTO `sys_job_log` VALUES ('2613', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:12:50');
INSERT INTO `sys_job_log` VALUES ('2614', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:13:00');
INSERT INTO `sys_job_log` VALUES ('2615', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:13:10');
INSERT INTO `sys_job_log` VALUES ('2616', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:13:20');
INSERT INTO `sys_job_log` VALUES ('2617', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:13:30');
INSERT INTO `sys_job_log` VALUES ('2618', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:13:40');
INSERT INTO `sys_job_log` VALUES ('2619', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:13:50');
INSERT INTO `sys_job_log` VALUES ('2620', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:14:00');
INSERT INTO `sys_job_log` VALUES ('2621', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:14:10');
INSERT INTO `sys_job_log` VALUES ('2622', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:14:20');
INSERT INTO `sys_job_log` VALUES ('2623', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:14:30');
INSERT INTO `sys_job_log` VALUES ('2624', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:14:40');
INSERT INTO `sys_job_log` VALUES ('2625', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:14:50');
INSERT INTO `sys_job_log` VALUES ('2626', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:15:00');
INSERT INTO `sys_job_log` VALUES ('2627', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:15:10');
INSERT INTO `sys_job_log` VALUES ('2628', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:15:20');
INSERT INTO `sys_job_log` VALUES ('2629', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:15:30');
INSERT INTO `sys_job_log` VALUES ('2630', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:15:40');
INSERT INTO `sys_job_log` VALUES ('2631', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:15:50');
INSERT INTO `sys_job_log` VALUES ('2632', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:16:00');
INSERT INTO `sys_job_log` VALUES ('2633', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:16:10');
INSERT INTO `sys_job_log` VALUES ('2634', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:16:20');
INSERT INTO `sys_job_log` VALUES ('2635', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:16:30');
INSERT INTO `sys_job_log` VALUES ('2636', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:16:40');
INSERT INTO `sys_job_log` VALUES ('2637', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:16:50');
INSERT INTO `sys_job_log` VALUES ('2638', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:17:00');
INSERT INTO `sys_job_log` VALUES ('2639', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:17:10');
INSERT INTO `sys_job_log` VALUES ('2640', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:17:20');
INSERT INTO `sys_job_log` VALUES ('2641', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:17:30');
INSERT INTO `sys_job_log` VALUES ('2642', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:17:40');
INSERT INTO `sys_job_log` VALUES ('2643', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:17:50');
INSERT INTO `sys_job_log` VALUES ('2644', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:18:00');
INSERT INTO `sys_job_log` VALUES ('2645', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:18:10');
INSERT INTO `sys_job_log` VALUES ('2646', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:18:20');
INSERT INTO `sys_job_log` VALUES ('2647', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:18:30');
INSERT INTO `sys_job_log` VALUES ('2648', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:18:40');
INSERT INTO `sys_job_log` VALUES ('2649', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:18:50');
INSERT INTO `sys_job_log` VALUES ('2650', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:19:00');
INSERT INTO `sys_job_log` VALUES ('2651', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:19:10');
INSERT INTO `sys_job_log` VALUES ('2652', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:19:20');
INSERT INTO `sys_job_log` VALUES ('2653', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:19:30');
INSERT INTO `sys_job_log` VALUES ('2654', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:19:40');
INSERT INTO `sys_job_log` VALUES ('2655', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:19:50');
INSERT INTO `sys_job_log` VALUES ('2656', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:20:00');
INSERT INTO `sys_job_log` VALUES ('2657', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:20:10');
INSERT INTO `sys_job_log` VALUES ('2658', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:20:20');
INSERT INTO `sys_job_log` VALUES ('2659', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:20:30');
INSERT INTO `sys_job_log` VALUES ('2660', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:20:40');
INSERT INTO `sys_job_log` VALUES ('2661', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:20:50');
INSERT INTO `sys_job_log` VALUES ('2662', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:21:00');
INSERT INTO `sys_job_log` VALUES ('2663', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:21:10');
INSERT INTO `sys_job_log` VALUES ('2664', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:21:20');
INSERT INTO `sys_job_log` VALUES ('2665', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:21:30');
INSERT INTO `sys_job_log` VALUES ('2666', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:21:40');
INSERT INTO `sys_job_log` VALUES ('2667', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:21:50');
INSERT INTO `sys_job_log` VALUES ('2668', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:22:00');
INSERT INTO `sys_job_log` VALUES ('2669', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:22:10');
INSERT INTO `sys_job_log` VALUES ('2670', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:22:20');
INSERT INTO `sys_job_log` VALUES ('2671', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:22:30');
INSERT INTO `sys_job_log` VALUES ('2672', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:22:40');
INSERT INTO `sys_job_log` VALUES ('2673', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:22:50');
INSERT INTO `sys_job_log` VALUES ('2674', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:23:00');
INSERT INTO `sys_job_log` VALUES ('2675', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:23:10');
INSERT INTO `sys_job_log` VALUES ('2676', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:23:20');
INSERT INTO `sys_job_log` VALUES ('2677', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:23:30');
INSERT INTO `sys_job_log` VALUES ('2678', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:23:40');
INSERT INTO `sys_job_log` VALUES ('2679', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:23:50');
INSERT INTO `sys_job_log` VALUES ('2680', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:24:00');
INSERT INTO `sys_job_log` VALUES ('2681', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:24:10');
INSERT INTO `sys_job_log` VALUES ('2682', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:24:20');
INSERT INTO `sys_job_log` VALUES ('2683', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:24:30');
INSERT INTO `sys_job_log` VALUES ('2684', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:24:40');
INSERT INTO `sys_job_log` VALUES ('2685', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 16:24:50');
INSERT INTO `sys_job_log` VALUES ('2686', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:25:00');
INSERT INTO `sys_job_log` VALUES ('2687', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:25:10');
INSERT INTO `sys_job_log` VALUES ('2688', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:25:20');
INSERT INTO `sys_job_log` VALUES ('2689', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:25:30');
INSERT INTO `sys_job_log` VALUES ('2690', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:25:40');
INSERT INTO `sys_job_log` VALUES ('2691', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:25:50');
INSERT INTO `sys_job_log` VALUES ('2692', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:26:00');
INSERT INTO `sys_job_log` VALUES ('2693', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:26:10');
INSERT INTO `sys_job_log` VALUES ('2694', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:26:20');
INSERT INTO `sys_job_log` VALUES ('2695', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:26:30');
INSERT INTO `sys_job_log` VALUES ('2696', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:26:40');
INSERT INTO `sys_job_log` VALUES ('2697', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:26:50');
INSERT INTO `sys_job_log` VALUES ('2698', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:27:00');
INSERT INTO `sys_job_log` VALUES ('2699', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:27:10');
INSERT INTO `sys_job_log` VALUES ('2700', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:27:20');
INSERT INTO `sys_job_log` VALUES ('2701', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:27:30');
INSERT INTO `sys_job_log` VALUES ('2702', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:27:40');
INSERT INTO `sys_job_log` VALUES ('2703', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:27:50');
INSERT INTO `sys_job_log` VALUES ('2704', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:28:00');
INSERT INTO `sys_job_log` VALUES ('2705', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:28:10');
INSERT INTO `sys_job_log` VALUES ('2706', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:28:20');
INSERT INTO `sys_job_log` VALUES ('2707', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:28:30');
INSERT INTO `sys_job_log` VALUES ('2708', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:28:40');
INSERT INTO `sys_job_log` VALUES ('2709', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:28:50');
INSERT INTO `sys_job_log` VALUES ('2710', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:29:00');
INSERT INTO `sys_job_log` VALUES ('2711', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:29:10');
INSERT INTO `sys_job_log` VALUES ('2712', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:29:20');
INSERT INTO `sys_job_log` VALUES ('2713', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:29:30');
INSERT INTO `sys_job_log` VALUES ('2714', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:29:40');
INSERT INTO `sys_job_log` VALUES ('2715', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:29:50');
INSERT INTO `sys_job_log` VALUES ('2716', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:30:00');
INSERT INTO `sys_job_log` VALUES ('2717', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:30:10');
INSERT INTO `sys_job_log` VALUES ('2718', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:30:20');
INSERT INTO `sys_job_log` VALUES ('2719', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:30:30');
INSERT INTO `sys_job_log` VALUES ('2720', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:30:40');
INSERT INTO `sys_job_log` VALUES ('2721', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:30:50');
INSERT INTO `sys_job_log` VALUES ('2722', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:31:00');
INSERT INTO `sys_job_log` VALUES ('2723', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:31:10');
INSERT INTO `sys_job_log` VALUES ('2724', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:31:20');
INSERT INTO `sys_job_log` VALUES ('2725', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:31:30');
INSERT INTO `sys_job_log` VALUES ('2726', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:31:40');
INSERT INTO `sys_job_log` VALUES ('2727', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:31:50');
INSERT INTO `sys_job_log` VALUES ('2728', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:32:00');
INSERT INTO `sys_job_log` VALUES ('2729', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:32:10');
INSERT INTO `sys_job_log` VALUES ('2730', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:32:20');
INSERT INTO `sys_job_log` VALUES ('2731', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：15毫秒', '0', null, '2018-11-28 16:32:30');
INSERT INTO `sys_job_log` VALUES ('2732', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:32:40');
INSERT INTO `sys_job_log` VALUES ('2733', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:32:50');
INSERT INTO `sys_job_log` VALUES ('2734', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:33:00');
INSERT INTO `sys_job_log` VALUES ('2735', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:33:10');
INSERT INTO `sys_job_log` VALUES ('2736', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:33:20');
INSERT INTO `sys_job_log` VALUES ('2737', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:33:30');
INSERT INTO `sys_job_log` VALUES ('2738', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:33:40');
INSERT INTO `sys_job_log` VALUES ('2739', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:33:50');
INSERT INTO `sys_job_log` VALUES ('2740', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:34:00');
INSERT INTO `sys_job_log` VALUES ('2741', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:34:10');
INSERT INTO `sys_job_log` VALUES ('2742', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:34:20');
INSERT INTO `sys_job_log` VALUES ('2743', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:34:30');
INSERT INTO `sys_job_log` VALUES ('2744', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:34:40');
INSERT INTO `sys_job_log` VALUES ('2745', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:34:50');
INSERT INTO `sys_job_log` VALUES ('2746', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:35:00');
INSERT INTO `sys_job_log` VALUES ('2747', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:35:10');
INSERT INTO `sys_job_log` VALUES ('2748', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:35:20');
INSERT INTO `sys_job_log` VALUES ('2749', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:35:30');
INSERT INTO `sys_job_log` VALUES ('2750', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:35:40');
INSERT INTO `sys_job_log` VALUES ('2751', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:35:50');
INSERT INTO `sys_job_log` VALUES ('2752', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:36:00');
INSERT INTO `sys_job_log` VALUES ('2753', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：14毫秒', '0', null, '2018-11-28 16:36:10');
INSERT INTO `sys_job_log` VALUES ('2754', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：16毫秒', '0', null, '2018-11-28 16:36:20');
INSERT INTO `sys_job_log` VALUES ('2755', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:36:30');
INSERT INTO `sys_job_log` VALUES ('2756', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 16:36:40');
INSERT INTO `sys_job_log` VALUES ('2757', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:36:50');
INSERT INTO `sys_job_log` VALUES ('2758', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:37:00');
INSERT INTO `sys_job_log` VALUES ('2759', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:37:10');
INSERT INTO `sys_job_log` VALUES ('2760', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:37:20');
INSERT INTO `sys_job_log` VALUES ('2761', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:37:30');
INSERT INTO `sys_job_log` VALUES ('2762', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:37:40');
INSERT INTO `sys_job_log` VALUES ('2763', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:37:50');
INSERT INTO `sys_job_log` VALUES ('2764', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:38:00');
INSERT INTO `sys_job_log` VALUES ('2765', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:38:10');
INSERT INTO `sys_job_log` VALUES ('2766', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:38:20');
INSERT INTO `sys_job_log` VALUES ('2767', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:38:30');
INSERT INTO `sys_job_log` VALUES ('2768', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:38:40');
INSERT INTO `sys_job_log` VALUES ('2769', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:38:50');
INSERT INTO `sys_job_log` VALUES ('2770', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:39:00');
INSERT INTO `sys_job_log` VALUES ('2771', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:39:10');
INSERT INTO `sys_job_log` VALUES ('2772', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:39:20');
INSERT INTO `sys_job_log` VALUES ('2773', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:39:30');
INSERT INTO `sys_job_log` VALUES ('2774', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:39:40');
INSERT INTO `sys_job_log` VALUES ('2775', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:39:50');
INSERT INTO `sys_job_log` VALUES ('2776', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:40:00');
INSERT INTO `sys_job_log` VALUES ('2777', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：11毫秒', '0', null, '2018-11-28 16:40:10');
INSERT INTO `sys_job_log` VALUES ('2778', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:40:20');
INSERT INTO `sys_job_log` VALUES ('2779', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:40:30');
INSERT INTO `sys_job_log` VALUES ('2780', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:40:40');
INSERT INTO `sys_job_log` VALUES ('2781', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:40:50');
INSERT INTO `sys_job_log` VALUES ('2782', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:41:00');
INSERT INTO `sys_job_log` VALUES ('2783', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:41:10');
INSERT INTO `sys_job_log` VALUES ('2784', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:41:20');
INSERT INTO `sys_job_log` VALUES ('2785', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:41:30');
INSERT INTO `sys_job_log` VALUES ('2786', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:41:40');
INSERT INTO `sys_job_log` VALUES ('2787', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:41:50');
INSERT INTO `sys_job_log` VALUES ('2788', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:42:00');
INSERT INTO `sys_job_log` VALUES ('2789', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:42:10');
INSERT INTO `sys_job_log` VALUES ('2790', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:42:20');
INSERT INTO `sys_job_log` VALUES ('2791', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:42:30');
INSERT INTO `sys_job_log` VALUES ('2792', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：12毫秒', '0', null, '2018-11-28 16:42:40');
INSERT INTO `sys_job_log` VALUES ('2793', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:42:50');
INSERT INTO `sys_job_log` VALUES ('2794', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:43:00');
INSERT INTO `sys_job_log` VALUES ('2795', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:43:10');
INSERT INTO `sys_job_log` VALUES ('2796', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:43:20');
INSERT INTO `sys_job_log` VALUES ('2797', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:43:30');
INSERT INTO `sys_job_log` VALUES ('2798', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:43:40');
INSERT INTO `sys_job_log` VALUES ('2799', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:43:50');
INSERT INTO `sys_job_log` VALUES ('2800', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:44:00');
INSERT INTO `sys_job_log` VALUES ('2801', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:44:10');
INSERT INTO `sys_job_log` VALUES ('2802', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:44:20');
INSERT INTO `sys_job_log` VALUES ('2803', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:44:30');
INSERT INTO `sys_job_log` VALUES ('2804', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:44:40');
INSERT INTO `sys_job_log` VALUES ('2805', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:44:50');
INSERT INTO `sys_job_log` VALUES ('2806', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:45:00');
INSERT INTO `sys_job_log` VALUES ('2807', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:45:10');
INSERT INTO `sys_job_log` VALUES ('2808', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:45:20');
INSERT INTO `sys_job_log` VALUES ('2809', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:45:30');
INSERT INTO `sys_job_log` VALUES ('2810', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:45:40');
INSERT INTO `sys_job_log` VALUES ('2811', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:45:50');
INSERT INTO `sys_job_log` VALUES ('2812', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:46:00');
INSERT INTO `sys_job_log` VALUES ('2813', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:46:10');
INSERT INTO `sys_job_log` VALUES ('2814', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:46:20');
INSERT INTO `sys_job_log` VALUES ('2815', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:46:30');
INSERT INTO `sys_job_log` VALUES ('2816', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:46:40');
INSERT INTO `sys_job_log` VALUES ('2817', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:46:50');
INSERT INTO `sys_job_log` VALUES ('2818', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:47:00');
INSERT INTO `sys_job_log` VALUES ('2819', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:47:10');
INSERT INTO `sys_job_log` VALUES ('2820', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:47:20');
INSERT INTO `sys_job_log` VALUES ('2821', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:47:30');
INSERT INTO `sys_job_log` VALUES ('2822', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:47:40');
INSERT INTO `sys_job_log` VALUES ('2823', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:47:50');
INSERT INTO `sys_job_log` VALUES ('2824', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:48:00');
INSERT INTO `sys_job_log` VALUES ('2825', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:48:10');
INSERT INTO `sys_job_log` VALUES ('2826', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:48:20');
INSERT INTO `sys_job_log` VALUES ('2827', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:48:30');
INSERT INTO `sys_job_log` VALUES ('2828', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:48:40');
INSERT INTO `sys_job_log` VALUES ('2829', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 16:48:50');
INSERT INTO `sys_job_log` VALUES ('2830', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:49:00');
INSERT INTO `sys_job_log` VALUES ('2831', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:49:10');
INSERT INTO `sys_job_log` VALUES ('2832', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:49:20');
INSERT INTO `sys_job_log` VALUES ('2833', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 16:49:30');
INSERT INTO `sys_job_log` VALUES ('2834', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:49:40');
INSERT INTO `sys_job_log` VALUES ('2835', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:49:50');
INSERT INTO `sys_job_log` VALUES ('2836', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:50:00');
INSERT INTO `sys_job_log` VALUES ('2837', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 16:50:10');
INSERT INTO `sys_job_log` VALUES ('2838', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:50:20');
INSERT INTO `sys_job_log` VALUES ('2839', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:50:30');
INSERT INTO `sys_job_log` VALUES ('2840', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:50:40');
INSERT INTO `sys_job_log` VALUES ('2841', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:50:50');
INSERT INTO `sys_job_log` VALUES ('2842', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:51:00');
INSERT INTO `sys_job_log` VALUES ('2843', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:51:10');
INSERT INTO `sys_job_log` VALUES ('2844', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:51:20');
INSERT INTO `sys_job_log` VALUES ('2845', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:51:30');
INSERT INTO `sys_job_log` VALUES ('2846', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 16:51:40');
INSERT INTO `sys_job_log` VALUES ('2847', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:51:50');
INSERT INTO `sys_job_log` VALUES ('2848', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 16:52:00');
INSERT INTO `sys_job_log` VALUES ('2849', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:52:10');
INSERT INTO `sys_job_log` VALUES ('2850', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:52:20');
INSERT INTO `sys_job_log` VALUES ('2851', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:52:30');
INSERT INTO `sys_job_log` VALUES ('2852', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:52:40');
INSERT INTO `sys_job_log` VALUES ('2853', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:52:50');
INSERT INTO `sys_job_log` VALUES ('2854', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:53:00');
INSERT INTO `sys_job_log` VALUES ('2855', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:53:10');
INSERT INTO `sys_job_log` VALUES ('2856', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:53:20');
INSERT INTO `sys_job_log` VALUES ('2857', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:53:30');
INSERT INTO `sys_job_log` VALUES ('2858', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:53:40');
INSERT INTO `sys_job_log` VALUES ('2859', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 16:53:50');
INSERT INTO `sys_job_log` VALUES ('2860', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:54:00');
INSERT INTO `sys_job_log` VALUES ('2861', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:54:10');
INSERT INTO `sys_job_log` VALUES ('2862', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:54:20');
INSERT INTO `sys_job_log` VALUES ('2863', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 16:54:30');
INSERT INTO `sys_job_log` VALUES ('2864', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:54:40');
INSERT INTO `sys_job_log` VALUES ('2865', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:54:50');
INSERT INTO `sys_job_log` VALUES ('2866', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:55:00');
INSERT INTO `sys_job_log` VALUES ('2867', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:55:10');
INSERT INTO `sys_job_log` VALUES ('2868', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:55:20');
INSERT INTO `sys_job_log` VALUES ('2869', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 16:55:30');
INSERT INTO `sys_job_log` VALUES ('2870', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 16:55:40');
INSERT INTO `sys_job_log` VALUES ('2871', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:55:50');
INSERT INTO `sys_job_log` VALUES ('2872', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：12毫秒', '0', null, '2018-11-28 16:56:00');
INSERT INTO `sys_job_log` VALUES ('2873', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:56:10');
INSERT INTO `sys_job_log` VALUES ('2874', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:56:20');
INSERT INTO `sys_job_log` VALUES ('2875', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:56:30');
INSERT INTO `sys_job_log` VALUES ('2876', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:56:40');
INSERT INTO `sys_job_log` VALUES ('2877', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:56:50');
INSERT INTO `sys_job_log` VALUES ('2878', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:57:00');
INSERT INTO `sys_job_log` VALUES ('2879', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 16:57:10');
INSERT INTO `sys_job_log` VALUES ('2880', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:57:20');
INSERT INTO `sys_job_log` VALUES ('2881', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:57:30');
INSERT INTO `sys_job_log` VALUES ('2882', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:57:40');
INSERT INTO `sys_job_log` VALUES ('2883', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 16:57:50');
INSERT INTO `sys_job_log` VALUES ('2884', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:58:00');
INSERT INTO `sys_job_log` VALUES ('2885', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 16:58:10');
INSERT INTO `sys_job_log` VALUES ('2886', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:58:20');
INSERT INTO `sys_job_log` VALUES ('2887', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:58:30');
INSERT INTO `sys_job_log` VALUES ('2888', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:58:40');
INSERT INTO `sys_job_log` VALUES ('2889', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:58:50');
INSERT INTO `sys_job_log` VALUES ('2890', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:59:00');
INSERT INTO `sys_job_log` VALUES ('2891', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:59:10');
INSERT INTO `sys_job_log` VALUES ('2892', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:59:20');
INSERT INTO `sys_job_log` VALUES ('2893', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:59:30');
INSERT INTO `sys_job_log` VALUES ('2894', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:59:40');
INSERT INTO `sys_job_log` VALUES ('2895', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 16:59:50');
INSERT INTO `sys_job_log` VALUES ('2896', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:00:00');
INSERT INTO `sys_job_log` VALUES ('2897', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:00:10');
INSERT INTO `sys_job_log` VALUES ('2898', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:00:20');
INSERT INTO `sys_job_log` VALUES ('2899', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:00:30');
INSERT INTO `sys_job_log` VALUES ('2900', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:00:40');
INSERT INTO `sys_job_log` VALUES ('2901', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:00:50');
INSERT INTO `sys_job_log` VALUES ('2902', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:01:00');
INSERT INTO `sys_job_log` VALUES ('2903', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:01:10');
INSERT INTO `sys_job_log` VALUES ('2904', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:01:20');
INSERT INTO `sys_job_log` VALUES ('2905', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:01:30');
INSERT INTO `sys_job_log` VALUES ('2906', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:01:40');
INSERT INTO `sys_job_log` VALUES ('2907', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:01:50');
INSERT INTO `sys_job_log` VALUES ('2908', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:02:00');
INSERT INTO `sys_job_log` VALUES ('2909', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:02:10');
INSERT INTO `sys_job_log` VALUES ('2910', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:02:20');
INSERT INTO `sys_job_log` VALUES ('2911', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:02:30');
INSERT INTO `sys_job_log` VALUES ('2912', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:02:40');
INSERT INTO `sys_job_log` VALUES ('2913', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:02:50');
INSERT INTO `sys_job_log` VALUES ('2914', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:03:00');
INSERT INTO `sys_job_log` VALUES ('2915', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:03:10');
INSERT INTO `sys_job_log` VALUES ('2916', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:03:20');
INSERT INTO `sys_job_log` VALUES ('2917', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:03:30');
INSERT INTO `sys_job_log` VALUES ('2918', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 17:03:40');
INSERT INTO `sys_job_log` VALUES ('2919', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:03:50');
INSERT INTO `sys_job_log` VALUES ('2920', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:04:00');
INSERT INTO `sys_job_log` VALUES ('2921', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 17:04:10');
INSERT INTO `sys_job_log` VALUES ('2922', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:04:20');
INSERT INTO `sys_job_log` VALUES ('2923', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:04:30');
INSERT INTO `sys_job_log` VALUES ('2924', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:04:40');
INSERT INTO `sys_job_log` VALUES ('2925', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:04:50');
INSERT INTO `sys_job_log` VALUES ('2926', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:05:00');
INSERT INTO `sys_job_log` VALUES ('2927', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:05:10');
INSERT INTO `sys_job_log` VALUES ('2928', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:05:20');
INSERT INTO `sys_job_log` VALUES ('2929', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:05:30');
INSERT INTO `sys_job_log` VALUES ('2930', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:05:40');
INSERT INTO `sys_job_log` VALUES ('2931', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:05:50');
INSERT INTO `sys_job_log` VALUES ('2932', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:06:00');
INSERT INTO `sys_job_log` VALUES ('2933', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:06:10');
INSERT INTO `sys_job_log` VALUES ('2934', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:06:20');
INSERT INTO `sys_job_log` VALUES ('2935', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:06:30');
INSERT INTO `sys_job_log` VALUES ('2936', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:06:40');
INSERT INTO `sys_job_log` VALUES ('2937', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:06:50');
INSERT INTO `sys_job_log` VALUES ('2938', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:07:00');
INSERT INTO `sys_job_log` VALUES ('2939', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:07:10');
INSERT INTO `sys_job_log` VALUES ('2940', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:07:20');
INSERT INTO `sys_job_log` VALUES ('2941', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:07:30');
INSERT INTO `sys_job_log` VALUES ('2942', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:07:40');
INSERT INTO `sys_job_log` VALUES ('2943', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:07:50');
INSERT INTO `sys_job_log` VALUES ('2944', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:08:00');
INSERT INTO `sys_job_log` VALUES ('2945', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:08:10');
INSERT INTO `sys_job_log` VALUES ('2946', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:08:20');
INSERT INTO `sys_job_log` VALUES ('2947', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:08:30');
INSERT INTO `sys_job_log` VALUES ('2948', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:08:40');
INSERT INTO `sys_job_log` VALUES ('2949', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:08:50');
INSERT INTO `sys_job_log` VALUES ('2950', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:09:00');
INSERT INTO `sys_job_log` VALUES ('2951', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:09:10');
INSERT INTO `sys_job_log` VALUES ('2952', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:09:20');
INSERT INTO `sys_job_log` VALUES ('2953', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:09:30');
INSERT INTO `sys_job_log` VALUES ('2954', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:09:40');
INSERT INTO `sys_job_log` VALUES ('2955', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:09:50');
INSERT INTO `sys_job_log` VALUES ('2956', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:10:00');
INSERT INTO `sys_job_log` VALUES ('2957', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:10:10');
INSERT INTO `sys_job_log` VALUES ('2958', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:10:20');
INSERT INTO `sys_job_log` VALUES ('2959', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:10:30');
INSERT INTO `sys_job_log` VALUES ('2960', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:10:40');
INSERT INTO `sys_job_log` VALUES ('2961', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:10:50');
INSERT INTO `sys_job_log` VALUES ('2962', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:11:00');
INSERT INTO `sys_job_log` VALUES ('2963', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-28 17:11:10');
INSERT INTO `sys_job_log` VALUES ('2964', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:11:20');
INSERT INTO `sys_job_log` VALUES ('2965', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:11:30');
INSERT INTO `sys_job_log` VALUES ('2966', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:11:40');
INSERT INTO `sys_job_log` VALUES ('2967', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:11:50');
INSERT INTO `sys_job_log` VALUES ('2968', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:12:00');
INSERT INTO `sys_job_log` VALUES ('2969', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 17:12:10');
INSERT INTO `sys_job_log` VALUES ('2970', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:12:20');
INSERT INTO `sys_job_log` VALUES ('2971', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:12:30');
INSERT INTO `sys_job_log` VALUES ('2972', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:12:40');
INSERT INTO `sys_job_log` VALUES ('2973', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:12:50');
INSERT INTO `sys_job_log` VALUES ('2974', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:13:00');
INSERT INTO `sys_job_log` VALUES ('2975', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:13:10');
INSERT INTO `sys_job_log` VALUES ('2976', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:13:20');
INSERT INTO `sys_job_log` VALUES ('2977', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:13:30');
INSERT INTO `sys_job_log` VALUES ('2978', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:13:40');
INSERT INTO `sys_job_log` VALUES ('2979', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:13:50');
INSERT INTO `sys_job_log` VALUES ('2980', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:14:00');
INSERT INTO `sys_job_log` VALUES ('2981', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:14:10');
INSERT INTO `sys_job_log` VALUES ('2982', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:14:20');
INSERT INTO `sys_job_log` VALUES ('2983', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:14:30');
INSERT INTO `sys_job_log` VALUES ('2984', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:14:40');
INSERT INTO `sys_job_log` VALUES ('2985', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:14:50');
INSERT INTO `sys_job_log` VALUES ('2986', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:15:00');
INSERT INTO `sys_job_log` VALUES ('2987', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:15:10');
INSERT INTO `sys_job_log` VALUES ('2988', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:15:20');
INSERT INTO `sys_job_log` VALUES ('2989', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:15:30');
INSERT INTO `sys_job_log` VALUES ('2990', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:15:40');
INSERT INTO `sys_job_log` VALUES ('2991', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:15:50');
INSERT INTO `sys_job_log` VALUES ('2992', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:16:00');
INSERT INTO `sys_job_log` VALUES ('2993', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:16:10');
INSERT INTO `sys_job_log` VALUES ('2994', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:16:20');
INSERT INTO `sys_job_log` VALUES ('2995', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:16:30');
INSERT INTO `sys_job_log` VALUES ('2996', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:16:40');
INSERT INTO `sys_job_log` VALUES ('2997', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:16:50');
INSERT INTO `sys_job_log` VALUES ('2998', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:17:00');
INSERT INTO `sys_job_log` VALUES ('2999', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:17:10');
INSERT INTO `sys_job_log` VALUES ('3000', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:17:20');
INSERT INTO `sys_job_log` VALUES ('3001', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:17:30');
INSERT INTO `sys_job_log` VALUES ('3002', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:17:40');
INSERT INTO `sys_job_log` VALUES ('3003', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:17:50');
INSERT INTO `sys_job_log` VALUES ('3004', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:18:00');
INSERT INTO `sys_job_log` VALUES ('3005', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 17:18:10');
INSERT INTO `sys_job_log` VALUES ('3006', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:18:20');
INSERT INTO `sys_job_log` VALUES ('3007', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:18:30');
INSERT INTO `sys_job_log` VALUES ('3008', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:18:40');
INSERT INTO `sys_job_log` VALUES ('3009', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:18:50');
INSERT INTO `sys_job_log` VALUES ('3010', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:19:00');
INSERT INTO `sys_job_log` VALUES ('3011', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:19:10');
INSERT INTO `sys_job_log` VALUES ('3012', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:19:20');
INSERT INTO `sys_job_log` VALUES ('3013', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:19:30');
INSERT INTO `sys_job_log` VALUES ('3014', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:19:40');
INSERT INTO `sys_job_log` VALUES ('3015', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:19:50');
INSERT INTO `sys_job_log` VALUES ('3016', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:20:00');
INSERT INTO `sys_job_log` VALUES ('3017', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:20:10');
INSERT INTO `sys_job_log` VALUES ('3018', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:20:20');
INSERT INTO `sys_job_log` VALUES ('3019', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:20:30');
INSERT INTO `sys_job_log` VALUES ('3020', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:20:40');
INSERT INTO `sys_job_log` VALUES ('3021', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:20:50');
INSERT INTO `sys_job_log` VALUES ('3022', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:21:00');
INSERT INTO `sys_job_log` VALUES ('3023', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:21:10');
INSERT INTO `sys_job_log` VALUES ('3024', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:21:20');
INSERT INTO `sys_job_log` VALUES ('3025', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:21:30');
INSERT INTO `sys_job_log` VALUES ('3026', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 17:21:40');
INSERT INTO `sys_job_log` VALUES ('3027', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:21:50');
INSERT INTO `sys_job_log` VALUES ('3028', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:22:00');
INSERT INTO `sys_job_log` VALUES ('3029', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:22:10');
INSERT INTO `sys_job_log` VALUES ('3030', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:22:20');
INSERT INTO `sys_job_log` VALUES ('3031', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:22:30');
INSERT INTO `sys_job_log` VALUES ('3032', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：10毫秒', '0', null, '2018-11-28 17:22:40');
INSERT INTO `sys_job_log` VALUES ('3033', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:22:50');
INSERT INTO `sys_job_log` VALUES ('3034', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:23:00');
INSERT INTO `sys_job_log` VALUES ('3035', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:23:10');
INSERT INTO `sys_job_log` VALUES ('3036', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:23:20');
INSERT INTO `sys_job_log` VALUES ('3037', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:23:30');
INSERT INTO `sys_job_log` VALUES ('3038', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:23:40');
INSERT INTO `sys_job_log` VALUES ('3039', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:23:50');
INSERT INTO `sys_job_log` VALUES ('3040', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:24:00');
INSERT INTO `sys_job_log` VALUES ('3041', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:24:10');
INSERT INTO `sys_job_log` VALUES ('3042', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:24:20');
INSERT INTO `sys_job_log` VALUES ('3043', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:24:30');
INSERT INTO `sys_job_log` VALUES ('3044', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:24:40');
INSERT INTO `sys_job_log` VALUES ('3045', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:24:50');
INSERT INTO `sys_job_log` VALUES ('3046', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:25:00');
INSERT INTO `sys_job_log` VALUES ('3047', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:25:10');
INSERT INTO `sys_job_log` VALUES ('3048', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:25:20');
INSERT INTO `sys_job_log` VALUES ('3049', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:25:30');
INSERT INTO `sys_job_log` VALUES ('3050', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:25:40');
INSERT INTO `sys_job_log` VALUES ('3051', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:25:50');
INSERT INTO `sys_job_log` VALUES ('3052', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:26:00');
INSERT INTO `sys_job_log` VALUES ('3053', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:26:10');
INSERT INTO `sys_job_log` VALUES ('3054', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:26:20');
INSERT INTO `sys_job_log` VALUES ('3055', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:26:30');
INSERT INTO `sys_job_log` VALUES ('3056', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:26:40');
INSERT INTO `sys_job_log` VALUES ('3057', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 17:26:50');
INSERT INTO `sys_job_log` VALUES ('3058', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:27:00');
INSERT INTO `sys_job_log` VALUES ('3059', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:27:10');
INSERT INTO `sys_job_log` VALUES ('3060', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:27:20');
INSERT INTO `sys_job_log` VALUES ('3061', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:27:30');
INSERT INTO `sys_job_log` VALUES ('3062', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:27:40');
INSERT INTO `sys_job_log` VALUES ('3063', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:27:50');
INSERT INTO `sys_job_log` VALUES ('3064', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:28:00');
INSERT INTO `sys_job_log` VALUES ('3065', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:28:10');
INSERT INTO `sys_job_log` VALUES ('3066', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：13毫秒', '0', null, '2018-11-28 17:28:20');
INSERT INTO `sys_job_log` VALUES ('3067', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:28:30');
INSERT INTO `sys_job_log` VALUES ('3068', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:28:40');
INSERT INTO `sys_job_log` VALUES ('3069', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:28:50');
INSERT INTO `sys_job_log` VALUES ('3070', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:29:00');
INSERT INTO `sys_job_log` VALUES ('3071', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:29:10');
INSERT INTO `sys_job_log` VALUES ('3072', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:29:20');
INSERT INTO `sys_job_log` VALUES ('3073', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:29:30');
INSERT INTO `sys_job_log` VALUES ('3074', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:29:40');
INSERT INTO `sys_job_log` VALUES ('3075', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:29:50');
INSERT INTO `sys_job_log` VALUES ('3076', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:30:00');
INSERT INTO `sys_job_log` VALUES ('3077', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:30:10');
INSERT INTO `sys_job_log` VALUES ('3078', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:30:20');
INSERT INTO `sys_job_log` VALUES ('3079', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:30:30');
INSERT INTO `sys_job_log` VALUES ('3080', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:30:40');
INSERT INTO `sys_job_log` VALUES ('3081', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:30:50');
INSERT INTO `sys_job_log` VALUES ('3082', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:31:00');
INSERT INTO `sys_job_log` VALUES ('3083', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:31:10');
INSERT INTO `sys_job_log` VALUES ('3084', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:31:20');
INSERT INTO `sys_job_log` VALUES ('3085', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:31:30');
INSERT INTO `sys_job_log` VALUES ('3086', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:31:40');
INSERT INTO `sys_job_log` VALUES ('3087', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:31:50');
INSERT INTO `sys_job_log` VALUES ('3088', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 17:32:00');
INSERT INTO `sys_job_log` VALUES ('3089', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 17:32:10');
INSERT INTO `sys_job_log` VALUES ('3090', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:32:20');
INSERT INTO `sys_job_log` VALUES ('3091', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:32:30');
INSERT INTO `sys_job_log` VALUES ('3092', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:32:40');
INSERT INTO `sys_job_log` VALUES ('3093', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:32:50');
INSERT INTO `sys_job_log` VALUES ('3094', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:33:00');
INSERT INTO `sys_job_log` VALUES ('3095', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:33:10');
INSERT INTO `sys_job_log` VALUES ('3096', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:33:20');
INSERT INTO `sys_job_log` VALUES ('3097', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:33:30');
INSERT INTO `sys_job_log` VALUES ('3098', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:33:40');
INSERT INTO `sys_job_log` VALUES ('3099', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:33:50');
INSERT INTO `sys_job_log` VALUES ('3100', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:34:00');
INSERT INTO `sys_job_log` VALUES ('3101', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:34:10');
INSERT INTO `sys_job_log` VALUES ('3102', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:34:20');
INSERT INTO `sys_job_log` VALUES ('3103', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:34:30');
INSERT INTO `sys_job_log` VALUES ('3104', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:34:40');
INSERT INTO `sys_job_log` VALUES ('3105', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 17:34:50');
INSERT INTO `sys_job_log` VALUES ('3106', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:35:00');
INSERT INTO `sys_job_log` VALUES ('3107', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:35:10');
INSERT INTO `sys_job_log` VALUES ('3108', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:35:20');
INSERT INTO `sys_job_log` VALUES ('3109', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:35:30');
INSERT INTO `sys_job_log` VALUES ('3110', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:35:40');
INSERT INTO `sys_job_log` VALUES ('3111', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:35:50');
INSERT INTO `sys_job_log` VALUES ('3112', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:36:00');
INSERT INTO `sys_job_log` VALUES ('3113', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：29毫秒', '0', null, '2018-11-28 17:36:10');
INSERT INTO `sys_job_log` VALUES ('3114', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:36:20');
INSERT INTO `sys_job_log` VALUES ('3115', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:36:30');
INSERT INTO `sys_job_log` VALUES ('3116', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:36:40');
INSERT INTO `sys_job_log` VALUES ('3117', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:36:50');
INSERT INTO `sys_job_log` VALUES ('3118', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:37:00');
INSERT INTO `sys_job_log` VALUES ('3119', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:37:10');
INSERT INTO `sys_job_log` VALUES ('3120', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:37:20');
INSERT INTO `sys_job_log` VALUES ('3121', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:37:30');
INSERT INTO `sys_job_log` VALUES ('3122', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:37:40');
INSERT INTO `sys_job_log` VALUES ('3123', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:37:50');
INSERT INTO `sys_job_log` VALUES ('3124', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 17:38:00');
INSERT INTO `sys_job_log` VALUES ('3125', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:38:10');
INSERT INTO `sys_job_log` VALUES ('3126', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 17:38:20');
INSERT INTO `sys_job_log` VALUES ('3127', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:38:30');
INSERT INTO `sys_job_log` VALUES ('3128', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:38:40');
INSERT INTO `sys_job_log` VALUES ('3129', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-28 17:38:50');
INSERT INTO `sys_job_log` VALUES ('3130', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:39:00');
INSERT INTO `sys_job_log` VALUES ('3131', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:39:10');
INSERT INTO `sys_job_log` VALUES ('3132', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:39:20');
INSERT INTO `sys_job_log` VALUES ('3133', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:39:30');
INSERT INTO `sys_job_log` VALUES ('3134', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:39:40');
INSERT INTO `sys_job_log` VALUES ('3135', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:39:50');
INSERT INTO `sys_job_log` VALUES ('3136', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:40:00');
INSERT INTO `sys_job_log` VALUES ('3137', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:40:10');
INSERT INTO `sys_job_log` VALUES ('3138', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:40:20');
INSERT INTO `sys_job_log` VALUES ('3139', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:40:30');
INSERT INTO `sys_job_log` VALUES ('3140', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:40:40');
INSERT INTO `sys_job_log` VALUES ('3141', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:40:50');
INSERT INTO `sys_job_log` VALUES ('3142', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:41:00');
INSERT INTO `sys_job_log` VALUES ('3143', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:41:10');
INSERT INTO `sys_job_log` VALUES ('3144', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:41:20');
INSERT INTO `sys_job_log` VALUES ('3145', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:41:30');
INSERT INTO `sys_job_log` VALUES ('3146', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:41:40');
INSERT INTO `sys_job_log` VALUES ('3147', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:41:50');
INSERT INTO `sys_job_log` VALUES ('3148', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:42:00');
INSERT INTO `sys_job_log` VALUES ('3149', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-28 17:42:10');
INSERT INTO `sys_job_log` VALUES ('3150', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:42:20');
INSERT INTO `sys_job_log` VALUES ('3151', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:42:30');
INSERT INTO `sys_job_log` VALUES ('3152', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:42:40');
INSERT INTO `sys_job_log` VALUES ('3153', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:42:50');
INSERT INTO `sys_job_log` VALUES ('3154', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:43:00');
INSERT INTO `sys_job_log` VALUES ('3155', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:43:10');
INSERT INTO `sys_job_log` VALUES ('3156', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:43:20');
INSERT INTO `sys_job_log` VALUES ('3157', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:43:30');
INSERT INTO `sys_job_log` VALUES ('3158', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：8毫秒', '0', null, '2018-11-28 17:43:40');
INSERT INTO `sys_job_log` VALUES ('3159', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:43:50');
INSERT INTO `sys_job_log` VALUES ('3160', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：0毫秒', '0', null, '2018-11-28 17:44:00');
INSERT INTO `sys_job_log` VALUES ('3161', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:44:10');
INSERT INTO `sys_job_log` VALUES ('3162', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:44:20');
INSERT INTO `sys_job_log` VALUES ('3163', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:44:30');
INSERT INTO `sys_job_log` VALUES ('3164', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:44:40');
INSERT INTO `sys_job_log` VALUES ('3165', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:44:50');
INSERT INTO `sys_job_log` VALUES ('3166', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:45:00');
INSERT INTO `sys_job_log` VALUES ('3167', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：5毫秒', '0', null, '2018-11-28 17:45:10');
INSERT INTO `sys_job_log` VALUES ('3168', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:45:20');
INSERT INTO `sys_job_log` VALUES ('3169', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:45:30');
INSERT INTO `sys_job_log` VALUES ('3170', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:45:40');
INSERT INTO `sys_job_log` VALUES ('3171', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-28 17:45:50');
INSERT INTO `sys_job_log` VALUES ('3172', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-28 17:46:00');
INSERT INTO `sys_job_log` VALUES ('3173', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-28 17:46:10');
INSERT INTO `sys_job_log` VALUES ('3174', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-29 09:14:52');
INSERT INTO `sys_job_log` VALUES ('3175', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-29 09:15:00');
INSERT INTO `sys_job_log` VALUES ('3176', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-29 09:15:10');
INSERT INTO `sys_job_log` VALUES ('3177', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-29 09:15:20');
INSERT INTO `sys_job_log` VALUES ('3178', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：7毫秒', '0', null, '2018-11-29 09:15:30');
INSERT INTO `sys_job_log` VALUES ('3179', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：2毫秒', '0', null, '2018-11-29 09:15:40');
INSERT INTO `sys_job_log` VALUES ('3180', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-29 09:15:50');
INSERT INTO `sys_job_log` VALUES ('3181', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：3毫秒', '0', null, '2018-11-29 09:16:00');
INSERT INTO `sys_job_log` VALUES ('3182', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：1毫秒', '0', null, '2018-11-29 09:16:10');
INSERT INTO `sys_job_log` VALUES ('3183', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：6毫秒', '0', null, '2018-11-29 09:16:20');
INSERT INTO `sys_job_log` VALUES ('3184', 'ryTask', '系统默认（无参）', 'ryNoParams', '', 'ryTask 总共耗时：4毫秒', '0', null, '2018-11-29 09:16:30');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('1', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '退出成功', '2018-11-30 15:37:33');
INSERT INTO `sys_logininfor` VALUES ('2', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-11-30 15:47:03');
INSERT INTO `sys_logininfor` VALUES ('3', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 15:29:55');
INSERT INTO `sys_logininfor` VALUES ('4', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 15:30:17');
INSERT INTO `sys_logininfor` VALUES ('5', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-03 15:30:24');
INSERT INTO `sys_logininfor` VALUES ('6', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '1', '密码输入错误1次', '2018-12-03 15:30:31');
INSERT INTO `sys_logininfor` VALUES ('7', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-03 15:30:57');
INSERT INTO `sys_logininfor` VALUES ('8', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-03 15:31:01');
INSERT INTO `sys_logininfor` VALUES ('9', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 15:31:12');
INSERT INTO `sys_logininfor` VALUES ('10', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 15:31:12');
INSERT INTO `sys_logininfor` VALUES ('11', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-03 15:37:40');
INSERT INTO `sys_logininfor` VALUES ('12', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '退出成功', '2018-12-03 15:37:52');
INSERT INTO `sys_logininfor` VALUES ('13', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 15:38:37');
INSERT INTO `sys_logininfor` VALUES ('14', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 15:41:13');
INSERT INTO `sys_logininfor` VALUES ('15', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 15:49:32');
INSERT INTO `sys_logininfor` VALUES ('16', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 15:52:09');
INSERT INTO `sys_logininfor` VALUES ('17', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '1', '验证码错误', '2018-12-03 16:15:01');
INSERT INTO `sys_logininfor` VALUES ('18', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 16:15:06');
INSERT INTO `sys_logininfor` VALUES ('19', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 16:34:33');
INSERT INTO `sys_logininfor` VALUES ('20', 'caofei', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 16:37:08');
INSERT INTO `sys_logininfor` VALUES ('21', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 17:27:27');
INSERT INTO `sys_logininfor` VALUES ('22', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-03 17:45:16');
INSERT INTO `sys_logininfor` VALUES ('23', 'admin', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', '0', '登录成功', '2018-12-04 08:40:33');
INSERT INTO `sys_logininfor` VALUES ('24', 'admin', '192.168.0.104', 'XX 内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2018-12-04 11:55:19');
INSERT INTO `sys_logininfor` VALUES ('25', 'admin', '192.168.0.104', 'XX 内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2018-12-04 11:55:46');
INSERT INTO `sys_logininfor` VALUES ('26', 'admin', '192.168.0.104', 'XX 内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2018-12-04 13:52:12');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` int(11) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) DEFAULT '' COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1143 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '1', '#', 'M', '0', '', 'fa fa-gear', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '2', '#', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '4', '#', 'M', '0', '', 'fa fa-bars', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '系统工具目录');
INSERT INTO `sys_menu` VALUES ('100', '员工管理', '1140', '1', '/system/user', 'C', '0', 'system:user:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1140', '2', '/system/role', 'C', '0', 'system:role:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '1140', '3', '/system/menu', 'C', '0', 'system:menu:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1140', '4', '/system/dept', 'C', '0', 'system:dept:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('104', '岗位管理', '1140', '5', '/system/post', 'C', '0', 'system:post:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '字典管理', '1', '6', '/system/dict', 'C', '0', 'system:dict:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '参数设置', '1', '7', '/system/config', 'C', '0', 'system:config:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', '8', '/system/notice', 'C', '0', 'system:notice:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '系统日志', '0', '3', '#', 'M', '0', '', 'fa fa-bars', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '2', '1', '/monitor/online', 'C', '0', 'monitor:online:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('110', '定时任务', '2', '2', '/monitor/job', 'C', '0', 'monitor:job:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('111', '数据监控', '2', '4', '/monitor/data', 'C', '0', 'monitor:data:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('112', '表单构建', '3', '1', '/tool/build', 'C', '0', 'tool:build:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('113', '代码生成', '3', '2', '/tool/gen', 'C', '0', 'tool:gen:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', '1', '/monitor/operlog', 'C', '0', 'monitor:operlog:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', '2', '/monitor/logininfor', 'C', '0', 'monitor:logininfor:view', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1000', '用户查询', '100', '1', '#', 'F', '0', 'system:user:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1001', '用户新增', '100', '2', '#', 'F', '0', 'system:user:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1002', '用户修改', '100', '3', '#', 'F', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1003', '用户删除', '100', '4', '#', 'F', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1004', '用户导出', '100', '5', '#', 'F', '0', 'system:user:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1005', '重置密码', '100', '5', '#', 'F', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1006', '角色查询', '101', '1', '#', 'F', '0', 'system:role:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1007', '角色新增', '101', '2', '#', 'F', '0', 'system:role:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1008', '角色修改', '101', '3', '#', 'F', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1009', '角色删除', '101', '4', '#', 'F', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1010', '角色导出', '101', '4', '#', 'F', '0', 'system:role:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1011', '菜单查询', '102', '1', '#', 'F', '0', 'system:menu:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1012', '菜单新增', '102', '2', '#', 'F', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单修改', '102', '3', '#', 'F', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单删除', '102', '4', '#', 'F', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1015', '部门查询', '103', '1', '#', 'F', '0', 'system:dept:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1016', '部门新增', '103', '2', '#', 'F', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1017', '部门修改', '103', '3', '#', 'F', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1018', '部门删除', '103', '4', '#', 'F', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1019', '岗位查询', '104', '1', '#', 'F', '0', 'system:post:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1020', '岗位新增', '104', '2', '#', 'F', '0', 'system:post:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位修改', '104', '3', '#', 'F', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位删除', '104', '4', '#', 'F', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位导出', '104', '4', '#', 'F', '0', 'system:post:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1024', '字典查询', '105', '1', '#', 'F', '0', 'system:dict:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1025', '字典新增', '105', '2', '#', 'F', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1026', '字典修改', '105', '3', '#', 'F', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1027', '字典删除', '105', '4', '#', 'F', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1028', '字典导出', '105', '4', '#', 'F', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1029', '参数查询', '106', '1', '#', 'F', '0', 'system:config:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1030', '参数新增', '106', '2', '#', 'F', '0', 'system:config:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1031', '参数修改', '106', '3', '#', 'F', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1032', '参数删除', '106', '4', '#', 'F', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1033', '参数导出', '106', '4', '#', 'F', '0', 'system:config:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1034', '公告查询', '107', '1', '#', 'F', '0', 'system:notice:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1035', '公告新增', '107', '2', '#', 'F', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1036', '公告修改', '107', '3', '#', 'F', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1037', '公告删除', '107', '4', '#', 'F', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1038', '操作查询', '500', '1', '#', 'F', '0', 'monitor:operlog:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1039', '操作删除', '500', '2', '#', 'F', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1040', '详细信息', '500', '3', '#', 'F', '0', 'monitor:operlog:detail', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1041', '日志导出', '500', '3', '#', 'F', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1042', '登录查询', '501', '1', '#', 'F', '0', 'monitor:logininfor:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1043', '登录删除', '501', '2', '#', 'F', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1044', '日志导出', '501', '2', '#', 'F', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1045', '在线查询', '109', '1', '#', 'F', '0', 'monitor:online:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1046', '批量强退', '109', '2', '#', 'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1047', '单条强退', '109', '3', '#', 'F', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1048', '任务查询', '110', '1', '#', 'F', '0', 'monitor:job:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1049', '任务新增', '110', '2', '#', 'F', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1050', '任务修改', '110', '3', '#', 'F', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1051', '任务删除', '110', '4', '#', 'F', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1052', '状态修改', '110', '5', '#', 'F', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1053', '任务导出', '110', '5', '#', 'F', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1054', '生成查询', '113', '1', '#', 'F', '0', 'tool:gen:list', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1055', '生成代码', '113', '2', '#', 'F', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '');
INSERT INTO `sys_menu` VALUES ('1056', '用户离职', '100', '6', '#', 'F', '0', 'system:user:departure', '#', 'admin', '2018-10-16 11:27:50', 'sunli', '2018-10-16 11:28:05', '');
INSERT INTO `sys_menu` VALUES ('1057', '是否参与积分排名', '100', '7', '#', 'F', '0', 'system:user:integral', '#', 'admin', '2018-10-16 11:36:56', 'sunli', '2018-10-16 11:37:04', '');
INSERT INTO `sys_menu` VALUES ('1058', '审报积分管理', '0', '6', '#', 'M', '0', '', 'fa fa-bars', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1059', '积分菜单设置', '1058', '1', '/integral/integralMenu', 'C', '0', 'integral:integralMenu:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1060', '品德A分管理', '1058', '2', '/integral/pdIntegral', 'C', '0', 'integral:pdIntegral:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1061', '业绩B分管理', '1058', '3', '/integral/yjIntegral', 'C', '0', 'integral:yjIntegral:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1062', '行为C分管理', '1058', '4', '/integral/xwIntegral', 'C', '0', 'integral:xwIntegral:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1063', '积分统计', '0', '5', '#', 'M', '0', '', 'fa fa-bars', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1064', '积分榜', '1063', '1', '/integral/integralBang', 'C', '0', 'integral:integralBang:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1065', '积分日志', '1069', '2', '/integral/integralLog', 'C', '0', 'integral:integralLog:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1066', '积分福利管理', '0', '7', '#', 'M', '0', '', 'fa fa-bars', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1067', '积分商品管理', '1066', '1', '/integral/integralGoods', 'C', '0', 'integral:integralGoods:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1068', '积分兑换记录', '1066', '2', '/integral/integralRecord', 'C', '0', 'integral:integralRecord:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1069', '积分审批管理', '0', '6', '#', 'M', '0', '', 'fa fa-bars', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1070', '积分菜单列表', '1058', '1', '#', 'F', '0', 'integral:integralMenu:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1071', '积分菜单添加', '1058', '2', '#', 'F', '0', 'integral:integralMenu:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1072', '积分菜单修改', '1058', '3', '#', 'F', '0', 'integral:integralMenu:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1073', '积分菜单删除', '1058', '4', '#', 'F', '0', 'integral:integralMenu:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1074', '行为积分管理列表', '1062', '1', '#', 'F', '0', 'integral:xwIntegral:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1075', '新增行为积分管理', '1062', '2', '#', 'F', '0', 'integral:xwIntegral:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1076', '修改行为积分管理', '1062', '3', '#', 'F', '0', 'integral:xwIntegral:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1077', '删除行为积分管理', '1062', '4', '#', 'F', '0', 'integral:xwIntegral:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1078', '业绩积分管理列表', '1061', '1', '#', 'F', '0', 'integral:yjIntegral:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1079', '新增保存业绩积分管理', '1061', '2', '#', 'F', '0', 'integral:yjIntegral:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1080', '修改业绩积分管理', '1061', '3', '#', 'F', '0', 'integral:yjIntegral:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1081', '删除业绩积分管理', '1061', '4', '#', 'F', '0', 'integral:yjIntegral:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1082', '品德积分管理列表', '1060', '1', '#', 'F', '0', 'integral:pdIntegral:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1083', '新增品德积分管理', '1060', '2', '#', 'F', '0', 'integral:pdIntegral:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1084', '修改品德积分管理', '1060', '3', '#', 'F', '0', 'integral:pdIntegral:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1085', '删除品德积分管理', '1060', '4', '#', 'F', '0', 'integral:pdIntegral:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1086', '积分审批', '1069', '1', '/integral/integralApproval', 'C', '0', 'integral:integralApproval:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1087', '新增保存审批管理', '1086', '2', '#', 'F', '0', 'integral:integralApproval:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1088', '修改保存审批管理', '1086', '3', '#', 'F', '0', 'integral:integralApproval:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1089', '删除审批管理', '1086', '4', '#', 'F', '0', 'integral:integralApproval:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1090', '查询积分审批管理列表', '1086', '5', '#l', 'F', '0', 'integral:integralApproval:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1091', '审批积分通过管理', '1086', '6', '#', 'F', '0', 'integral:integralApproval:success', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1092', '审批积分不通过管理', '1086', '7', '#', 'F', '0', 'system:integralApproval:no', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1093', '撤销审批管理', '1086', '8', '#', 'F', '0', 'system:integralApproval:che', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1094', '工作台应用管理', '0', '8', '#', 'M', '0', '', 'fa fa-bars', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1095', '工作台应用', '1094', '1', '/work/gzd', 'C', '0', 'work:gzd:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1096', '工作台应用管理列表', '1095', '0', '#', 'F', '0', 'work:gzd:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1097', '新增工作台应用管理', '1095', '1', '#', 'F', '0', 'work:gzd:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1098', '修改工作台应用管理', '1095', '2', '#', 'F', '0', 'work:gzd:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1099', '删除工作台应用管理', '1095', '3', '#', 'F', '0', 'work:gzd:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1106', '积分奖扣', '0', '6', '#', 'M', '0', 'integral:integralJk:view', 'fa fa-bars', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1107', '新增积分奖罚管理', '1106', '1', '#', 'F', '0', 'integral:integralJk:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1108', '修改积分奖罚管理', '1106', '2', '#', 'F', '0', 'integral:integralJk:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1109', '查询积分奖罚管理', '1106', '3', '#', 'F', '0', 'integral:integralJk:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1110', '删除审批管理', '1106', '4', '#', 'F', '0', 'system:reward:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1116', '积分榜展示页面', '1064', '1', '#', 'F', '0', 'integral:integralBang:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1117', '查询积分日志列表', '1065', '1', '#', 'F', '0', 'integral:integralLog:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1118', '查询商品管理列表', '1067', '1', '#', 'F', '0', 'integral:integralGoods:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1119', '新增商品管理', '1067', '2', '#', 'F', '0', 'integral:integralGoods:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1120', '修改商品管理', '1067', '3', '#', 'F', '0', 'integral:integralGoods:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1121', '删除商品管理', '1067', '4', '#', 'F', '0', 'integral:integralGoods:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1122', '商品兑换记录列表', '1068', '1', '#', 'F', '0', 'integral:integralRecord:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1123', '新增商品兑换记录', '1068', '2', '#', 'F', '0', 'integral:integralRecord:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1124', '修改商品兑换记录', '1068', '3', '#', 'F', '0', 'integral:integralRecord:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1125', '删除商品兑换记录', '1068', '4', '#', 'F', '0', 'integral:integralRecord:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1126', '商品兑换日志 ', '1066', '3', '/integral/integralRecordLog', 'C', '0', 'integral:integralRecordLog:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1127', '商品兑换日志列表', '1126', '1', '#', 'F', '0', 'integral:integralRecordLog:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1128', '新增商品兑换记录日志', '1126', '2', '#', 'F', '0', 'integral:integralRecordLog:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1129', '修改商品兑换记录日志', '1126', '3', '#', 'F', '0', 'integral:integralRecordLog:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1130', '删除商品兑换记录日志', '1126', '0', '#', 'F', '0', 'integral:integralRecordLog:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1131', '商品兑换记录审核通过不通过', '1068', '5', '#', 'F', '0', 'integral:integralRecord:success', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1132', '广告图片管理', '0', '10', '#', 'M', '0', '', 'fa fa-bars', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1133', '广告图片设置', '1132', '1', '/integral/ggTable', 'C', '0', 'integral:ggTable:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1134', '查询广告图片列表', '1133', '2', '#', 'F', '0', 'integral:ggTable:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1135', '新增保存广告图片', '1133', '3', '#', 'F', '0', 'integral:ggTable:add', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1136', '修改保存广告图片', '1133', '0', '#', 'F', '0', 'integral:ggTable:edit', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1137', '删除广告图片', '1133', '0', '#', 'F', '0', 'integral:ggTable:remove', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1138', '积分奖扣日志', '1106', '1', '/integral/integralJk', 'C', '0', 'integral:integralJk:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1139', '积分管理奖扣', '1106', '0', '/integral/integralJkUser', 'C', '0', 'integral:integralJkUser:view', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1140', '公司员工管理', '0', '0', '#', 'M', '0', '', 'fa fa-gear', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1141', '积分奖扣员工列表', '1138', '1', '#', 'F', '0', 'integral:integralJkUser:list', '#', '', null, '', null, '');
INSERT INTO `sys_menu` VALUES ('1142', '添加积分员工奖扣', '1138', '2', '#', 'F', '0', 'integral:integralJkUser:add', '#', '', null, '', null, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(2) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(500) NOT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('1', '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');
INSERT INTO `sys_notice` VALUES ('2', '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(30) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(255) DEFAULT '' COMMENT '请求参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('1', '操作日志', '9', 'com.ruoyi.web.controller.monitor.SysOperlogController.clean()', '1', 'admin', null, '/monitor/operlog/clean', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-28 08:56:47');
INSERT INTO `sys_oper_log` VALUES ('2', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/1/1', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-28 10:39:04');
INSERT INTO `sys_oper_log` VALUES ('3', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/1/1', '127.0.0.1', 'XX 内网IP', '{ }', '1', '\r\n### Error updating database.  Cause: com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where good_id = null\' at line 3\r\n### The error may involve com.ruoyi.integral.mapper.IntegralGoodsMapper.updateIntegralGoods-Inline\r\n### The error occurred while setting parameters\r\n### SQL: update integral_goods                    where good_id = ?\r\n### Cause: com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where good_id = null\' at line 3\n; bad SQL grammar []; nested exception is com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'where good_id = null\' at line 3', '2018-11-28 10:42:19');
INSERT INTO `sys_oper_log` VALUES ('4', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/1/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-28 10:42:39');
INSERT INTO `sys_oper_log` VALUES ('5', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-28 10:43:18');
INSERT INTO `sys_oper_log` VALUES ('6', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"24\" ],\r\n  \"goodName\" : [ \"花千骨\" ],\r\n  \"goodKc\" : [ \"800\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"500\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/95171ac6-3f5a-4fc0-bcda-8cefc6d1a2b5timg4.jpg\" ],\r\n  \"goodDetails\" : [ \"人气明星\" ]', '0', null, '2018-11-28 10:43:32');
INSERT INTO `sys_oper_log` VALUES ('7', '定时任务', '2', 'com.ruoyi.web.controller.monitor.SysJobController.changeStatus()', '1', 'admin', '集团财务', '/monitor/job/changeStatus', '127.0.0.1', 'XX 内网IP', '{\r\n  \"jobId\" : [ \"1\" ],\r\n  \"status\" : [ \"1\" ]\r\n}', '0', null, '2018-11-29 09:16:33');
INSERT INTO `sys_oper_log` VALUES ('8', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:12:43');
INSERT INTO `sys_oper_log` VALUES ('9', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:12:46');
INSERT INTO `sys_oper_log` VALUES ('10', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:12:49');
INSERT INTO `sys_oper_log` VALUES ('11', '商品管理', '1', 'com.ruoyi.web.controller.integral.IntegralGoodsController.addSave()', '1', 'admin', '集团财务', '/integral/integralGoods/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodImg\" : [ \"http://192.168.0.101:8080/147a04e5-61c2-49bc-8f94-2e2488deb3220acbc0b2-cde2-4bb1-ad5c-8a37747246c210eb6486-f45a-425f-8d38-3a59774ad7b88aa3a373-7d2b-41f8-adaa-22e35e60e2991450512365_G0UW.jpg\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:', '0', null, '2018-11-29 14:13:30');
INSERT INTO `sys_oper_log` VALUES ('12', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:13:46');
INSERT INTO `sys_oper_log` VALUES ('13', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:13:50');
INSERT INTO `sys_oper_log` VALUES ('14', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"24\" ],\r\n  \"goodName\" : [ \"花千骨\" ],\r\n  \"goodKc\" : [ \"100\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"500\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/c88f561b-abf9-4171-8dee-73fc47a16bbf3c6fa737-807b-4b81-a2cb-3313a08cea697ca26a8', '0', null, '2018-11-29 14:13:52');
INSERT INTO `sys_oper_log` VALUES ('15', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:14:04');
INSERT INTO `sys_oper_log` VALUES ('16', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:14:06');
INSERT INTO `sys_oper_log` VALUES ('17', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:14:13');
INSERT INTO `sys_oper_log` VALUES ('18', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:14:20');
INSERT INTO `sys_oper_log` VALUES ('19', '商品管理', '1', 'com.ruoyi.web.controller.integral.IntegralGoodsController.addSave()', '1', 'admin', '集团财务', '/integral/integralGoods/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodImg\" : [ \"http://192.168.0.101:8080/2c2e87fe-7dae-4a7e-baad-38cf4d5c689d0f18bc40-2409-43b1-b08f-9b291ea08fa65d0fe440-212d-4266-9da9-b1d01affe4dc39d72e2e-6e5b-4de7-9f6c-081c07762c7dtimg1.jpg\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/1660e', '0', null, '2018-11-29 14:14:54');
INSERT INTO `sys_oper_log` VALUES ('20', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:15:11');
INSERT INTO `sys_oper_log` VALUES ('21', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:15:16');
INSERT INTO `sys_oper_log` VALUES ('22', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:15:20');
INSERT INTO `sys_oper_log` VALUES ('23', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"23\" ],\r\n  \"goodName\" : [ \"外套\" ],\r\n  \"goodKc\" : [ \"100\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"500\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/2c2d1860-6cbc-43cf-ac13-d7fd8f65982a0f18bc40-2409-43b1-b08f-9b291ea08fa65d0fe440', '0', null, '2018-11-29 14:15:23');
INSERT INTO `sys_oper_log` VALUES ('24', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX XX', '{ }', '0', null, '2018-11-29 14:15:33');
INSERT INTO `sys_oper_log` VALUES ('25', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:15:39');
INSERT INTO `sys_oper_log` VALUES ('26', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX XX', '{ }', '0', null, '2018-11-29 14:15:45');
INSERT INTO `sys_oper_log` VALUES ('27', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"22\" ],\r\n  \"goodName\" : [ \"礼服\" ],\r\n  \"goodKc\" : [ \"96\" ],\r\n  \"ydhNum\" : [ \"4\" ],\r\n  \"dhIntegral\" : [ \"1000\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/53adb4ed-3600-4a77-bd48-4a4e1c7e90ba2404c071-06c3-4d52-9b3a-787c25926e63logo.png', '0', null, '2018-11-29 14:15:47');
INSERT INTO `sys_oper_log` VALUES ('28', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:16:01');
INSERT INTO `sys_oper_log` VALUES ('29', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:16:11');
INSERT INTO `sys_oper_log` VALUES ('30', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:16:19');
INSERT INTO `sys_oper_log` VALUES ('31', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"21\" ],\r\n  \"goodName\" : [ \"衣服\" ],\r\n  \"goodKc\" : [ \"100\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"500\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/70f3163f-2b37-4aa4-b438-e78e1b299117u=1012437829,4246881130&fm=26&gp=0.jpg,http:', '0', null, '2018-11-29 14:16:21');
INSERT INTO `sys_oper_log` VALUES ('32', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:16:35');
INSERT INTO `sys_oper_log` VALUES ('33', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:16:44');
INSERT INTO `sys_oper_log` VALUES ('34', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:16:58');
INSERT INTO `sys_oper_log` VALUES ('35', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:17:01');
INSERT INTO `sys_oper_log` VALUES ('36', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:17:09');
INSERT INTO `sys_oper_log` VALUES ('37', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"19\" ],\r\n  \"goodName\" : [ \"图片\" ],\r\n  \"goodKc\" : [ \"100\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"500\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/16b7ea0c-152b-4f2f-91f4-6fbfe2189673timg4.jpg,http://192.168.0.101:8080/c76edb07', '0', null, '2018-11-29 14:17:10');
INSERT INTO `sys_oper_log` VALUES ('38', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:17:21');
INSERT INTO `sys_oper_log` VALUES ('39', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:17:24');
INSERT INTO `sys_oper_log` VALUES ('40', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"1\" ],\r\n  \"goodName\" : [ \"柿子\" ],\r\n  \"goodKc\" : [ \"100\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"100\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/c8ffaec5-926f-43c4-b16a-4ffc9a8168410e61e8a4-15f6-4872-b3fe-01f78425850au=1012437', '0', null, '2018-11-29 14:17:27');
INSERT INTO `sys_oper_log` VALUES ('41', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:17:35');
INSERT INTO `sys_oper_log` VALUES ('42', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:17:38');
INSERT INTO `sys_oper_log` VALUES ('43', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"1\" ],\r\n  \"goodName\" : [ \"柿子\" ],\r\n  \"goodKc\" : [ \"100\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"100\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/498b2d4f-6edd-45e2-b3b8-864b0a79ec5d0f18bc40-2409-43b1-b08f-9b291ea08fa65d0fe440-', '0', null, '2018-11-29 14:17:42');
INSERT INTO `sys_oper_log` VALUES ('44', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX XX', '{\r\n  \"goodId\" : [ \"20\" ],\r\n  \"goodName\" : [ \"雨伞\" ],\r\n  \"goodKc\" : [ \"95\" ],\r\n  \"ydhNum\" : [ \"3\" ],\r\n  \"dhIntegral\" : [ \"500\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/0e61e8a4-15f6-4872-b3fe-01f78425850au=1012437829,4246881130&fm=26&gp=0.jpg\" ],\r\n ', '0', null, '2018-11-29 14:18:58');
INSERT INTO `sys_oper_log` VALUES ('45', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:22:02');
INSERT INTO `sys_oper_log` VALUES ('46', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:22:05');
INSERT INTO `sys_oper_log` VALUES ('47', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"20\" ],\r\n  \"goodName\" : [ \"雨伞\" ],\r\n  \"goodKc\" : [ \"95\" ],\r\n  \"ydhNum\" : [ \"3\" ],\r\n  \"dhIntegral\" : [ \"500\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/e12d142d-7773-49b3-8ebe-8a24c288a2591.jpg,http://192.168.0.101:8080/1e66101f-f1c8', '0', null, '2018-11-29 14:22:07');
INSERT INTO `sys_oper_log` VALUES ('48', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX XX', '{ }', '0', null, '2018-11-29 14:26:41');
INSERT INTO `sys_oper_log` VALUES ('49', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:26:46');
INSERT INTO `sys_oper_log` VALUES ('50', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"26\" ],\r\n  \"goodName\" : [ \"出门走走\" ],\r\n  \"goodKc\" : [ \"1000\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"1000\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/8d257dbd-d246-49c8-bdba-b70d2cfac70e1,1.jpg,http://192.168.0.101:8080/2f4c69', '0', null, '2018-11-29 14:26:51');
INSERT INTO `sys_oper_log` VALUES ('51', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:27:35');
INSERT INTO `sys_oper_log` VALUES ('52', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"26\" ],\r\n  \"goodName\" : [ \"出门走走\" ],\r\n  \"goodKc\" : [ \"1000\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"1000\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/8d257dbd-d246-49c8-bdba-b70d2cfac70e1,1.jpg,http://192.168.0.101:8080/2f4c69', '0', null, '2018-11-29 14:27:35');
INSERT INTO `sys_oper_log` VALUES ('53', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:27:58');
INSERT INTO `sys_oper_log` VALUES ('54', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:28:41');
INSERT INTO `sys_oper_log` VALUES ('55', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"26\" ],\r\n  \"goodName\" : [ \"出门走走\" ],\r\n  \"goodKc\" : [ \"1000\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"1000\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/0d3d718b-6d52-4101-a674-f4d7295c27651,1.jpg\" ],\r\n  \"goodDetails\" : [ \"锻炼身体\" ', '0', null, '2018-11-29 14:28:51');
INSERT INTO `sys_oper_log` VALUES ('56', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:37:56');
INSERT INTO `sys_oper_log` VALUES ('57', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:38:08');
INSERT INTO `sys_oper_log` VALUES ('58', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"21\" ],\r\n  \"goodName\" : [ \"衣服\" ],\r\n  \"goodKc\" : [ \"100\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"500\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/b441f0eb-1e0e-4523-b2e8-a8b48bfd2bfb1e66101f-f1c8-4964-904a-15feff355eb11.jpg,ht', '0', null, '2018-11-29 14:38:11');
INSERT INTO `sys_oper_log` VALUES ('59', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:38:24');
INSERT INTO `sys_oper_log` VALUES ('60', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:38:27');
INSERT INTO `sys_oper_log` VALUES ('61', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"26\" ],\r\n  \"goodName\" : [ \"出门走走\" ],\r\n  \"goodKc\" : [ \"1000\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"1000\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/d8e1fd27-ad2d-40f1-a27f-fe08b61434de1e66101f-f1c8-4964-904a-15feff355eb11.jp', '0', null, '2018-11-29 14:38:29');
INSERT INTO `sys_oper_log` VALUES ('62', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX XX', '{ }', '0', null, '2018-11-29 14:38:46');
INSERT INTO `sys_oper_log` VALUES ('63', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:38:50');
INSERT INTO `sys_oper_log` VALUES ('64', '商品管理', '2', 'com.ruoyi.web.controller.integral.IntegralGoodsController.editSave()', '1', 'admin', '集团财务', '/integral/integralGoods/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodId\" : [ \"26\" ],\r\n  \"goodName\" : [ \"出门走走\" ],\r\n  \"goodKc\" : [ \"1000\" ],\r\n  \"ydhNum\" : [ \"0\" ],\r\n  \"dhIntegral\" : [ \"1000\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/97ca6c64-90f4-45f6-9438-a7a5e0ed82430f18bc40-2409-43b1-b08f-9b291ea08fa65d0f', '0', null, '2018-11-29 14:38:51');
INSERT INTO `sys_oper_log` VALUES ('65', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/10/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:42:38');
INSERT INTO `sys_oper_log` VALUES ('66', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/9/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 14:46:40');
INSERT INTO `sys_oper_log` VALUES ('67', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/7/2', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-29 15:11:16');
INSERT INTO `sys_oper_log` VALUES ('68', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/1/2', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-29 15:13:20');
INSERT INTO `sys_oper_log` VALUES ('69', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/1/2', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-29 15:13:44');
INSERT INTO `sys_oper_log` VALUES ('70', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/1/2', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-29 15:15:57');
INSERT INTO `sys_oper_log` VALUES ('71', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/1/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 15:16:45');
INSERT INTO `sys_oper_log` VALUES ('72', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/1/2', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-29 15:20:23');
INSERT INTO `sys_oper_log` VALUES ('73', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/1/2', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-29 15:23:35');
INSERT INTO `sys_oper_log` VALUES ('74', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/1/2', '127.0.0.1', 'XX XX', '{ }', '0', null, '2018-11-29 15:29:17');
INSERT INTO `sys_oper_log` VALUES ('75', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/1/2', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 15:33:41');
INSERT INTO `sys_oper_log` VALUES ('76', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/10/2', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 15:54:06');
INSERT INTO `sys_oper_log` VALUES ('77', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/10/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 15:57:59');
INSERT INTO `sys_oper_log` VALUES ('78', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/1/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 15:58:11');
INSERT INTO `sys_oper_log` VALUES ('79', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/12/2', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-29 16:25:43');
INSERT INTO `sys_oper_log` VALUES ('80', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/11/2', '127.0.0.1', 'XX 内网IP', '{ }', '1', '', '2018-11-29 16:25:55');
INSERT INTO `sys_oper_log` VALUES ('81', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', null, '/integral/integralRecord/success/11/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 16:25:57');
INSERT INTO `sys_oper_log` VALUES ('82', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/14/2', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:03:18');
INSERT INTO `sys_oper_log` VALUES ('83', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/20/2', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:20:14');
INSERT INTO `sys_oper_log` VALUES ('84', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/20/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:20:20');
INSERT INTO `sys_oper_log` VALUES ('85', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/20/2', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:20:28');
INSERT INTO `sys_oper_log` VALUES ('86', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/20/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:20:45');
INSERT INTO `sys_oper_log` VALUES ('87', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/20/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:24:21');
INSERT INTO `sys_oper_log` VALUES ('88', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:32:16');
INSERT INTO `sys_oper_log` VALUES ('89', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX XX', '{ }', '0', null, '2018-11-29 17:32:24');
INSERT INTO `sys_oper_log` VALUES ('90', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:32:29');
INSERT INTO `sys_oper_log` VALUES ('91', '商品管理', '1', 'com.ruoyi.web.controller.integral.IntegralGoodsController.addSave()', '1', 'admin', '集团财务', '/integral/integralGoods/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodImg\" : [ \"http://192.168.0.101:8080/16915905-0ab6-4133-a8f0-7b6631753c4c0acbc0b2-cde2-4bb1-ad5c-8a37747246c210eb6486-f45a-425f-8d38-3a59774ad7b88aa3a373-7d2b-41f8-adaa-22e35e60e2991450512365_G0UW.jpg\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:', '0', null, '2018-11-29 17:32:45');
INSERT INTO `sys_oper_log` VALUES ('92', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:33:00');
INSERT INTO `sys_oper_log` VALUES ('93', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:33:10');
INSERT INTO `sys_oper_log` VALUES ('94', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:33:20');
INSERT INTO `sys_oper_log` VALUES ('95', '商品管理', '1', 'com.ruoyi.web.controller.integral.IntegralGoodsController.addSave()', '1', 'admin', '集团财务', '/integral/integralGoods/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"goodImg\" : [ \"http://192.168.0.101:8080/b0ea6716-90de-4011-85ca-353429e3fd37c27689ab-c098-46a2-acd7-e83502cecdd4timg4.jpg\" ],\r\n  \"goodLbImg\" : [ \"http://192.168.0.101:8080/a84445cb-06ba-4c71-b029-a8b8a7f356c4timg.jpg,http://192.168.0.101:8080/432915', '0', null, '2018-11-29 17:33:33');
INSERT INTO `sys_oper_log` VALUES ('96', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/22/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:34:49');
INSERT INTO `sys_oper_log` VALUES ('97', '审批管理', '0', 'com.ruoyi.web.controller.integral.IntegralRecordController.successStatus()', '1', 'admin', '集团财务', '/integral/integralRecord/success/23/2', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-29 17:35:26');
INSERT INTO `sys_oper_log` VALUES ('98', '用户管理', '1', 'com.ruoyi.web.controller.system.SysUserController.addSave()', '1', 'admin', '集团财务', '/system/user/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"111\" ],\r\n  \"loginName\" : [ \"caofei\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"8\" ],\r\n  \"jiChuInt', '0', null, '2018-11-30 14:36:57');
INSERT INTO `sys_oper_log` VALUES ('99', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-30 16:06:29');
INSERT INTO `sys_oper_log` VALUES ('100', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-30 16:06:32');
INSERT INTO `sys_oper_log` VALUES ('101', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-30 16:06:47');
INSERT INTO `sys_oper_log` VALUES ('102', '上传图片', '0', 'com.ruoyi.web.controller.integral.IntegralGoodsController.uploadFile()', '1', 'admin', '集团财务', '/integral/integralGoods/uploadFile', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-11-30 16:10:15');
INSERT INTO `sys_oper_log` VALUES ('103', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.ruleSave()', '1', 'admin', '集团财务', '/system/role/rule', '127.0.0.1', 'XX 内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"dataScope\" : [ \"2\" ],\r\n  \"deptIds\" : [ \"100,101,103,104,102,110,111,112,113,114,115,105,106,107,108,109,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132', '0', null, '2018-12-03 15:29:56');
INSERT INTO `sys_oper_log` VALUES ('104', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.editSave()', '1', 'admin', '集团财务', '/system/role/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"roleId\" : [ \"1\" ],\r\n  \"roleName\" : [ \"管理员\" ],\r\n  \"roleKey\" : [ \"admin\" ],\r\n  \"roleSort\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"管理员\" ],\r\n  \"menuIds\" : [ \"1,105,1024,1025,1026,1027,1028,106,1029,1030,1031,1032,1033,107,1034,1035,1036,1037,', '0', null, '2018-12-03 15:30:58');
INSERT INTO `sys_oper_log` VALUES ('105', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 15:31:58');
INSERT INTO `sys_oper_log` VALUES ('106', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 15:38:20');
INSERT INTO `sys_oper_log` VALUES ('107', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 15:41:10');
INSERT INTO `sys_oper_log` VALUES ('108', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 15:49:54');
INSERT INTO `sys_oper_log` VALUES ('109', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 15:51:18');
INSERT INTO `sys_oper_log` VALUES ('110', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 15:52:26');
INSERT INTO `sys_oper_log` VALUES ('111', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:15:40');
INSERT INTO `sys_oper_log` VALUES ('112', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:16:55');
INSERT INTO `sys_oper_log` VALUES ('113', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:17:38');
INSERT INTO `sys_oper_log` VALUES ('114', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:19:57');
INSERT INTO `sys_oper_log` VALUES ('115', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX XX', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:27:30');
INSERT INTO `sys_oper_log` VALUES ('116', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:27:55');
INSERT INTO `sys_oper_log` VALUES ('117', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX XX', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:28:29');
INSERT INTO `sys_oper_log` VALUES ('118', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:28:54');
INSERT INTO `sys_oper_log` VALUES ('119', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:35:46');
INSERT INTO `sys_oper_log` VALUES ('120', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'caofei', '系统信息部', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ]\r\n}', '0', null, '2018-12-03 16:37:32');
INSERT INTO `sys_oper_log` VALUES ('121', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"\" ],\r\n  \"zuiDuoIntegral\" : [ \"\" ],\r\n  \"zuiShaoIntegral\" : [ \"\" ],\r\n  \"integralFenJi\" : [ \"\" ],\r\n  ', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\r\n### The error may involve com.ruoyi.integral.mapper.XwIntegralMapper.insertXwIntegral-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into xw_integral    ( behavior_category,    behavior_title,                                status,    create_time,                     dept_id,                          menu_id,                          dept_name )           values ( ?,    ?,                                ?,    ?,                      ?,                          ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'\' for column \'menu_id\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1', '2018-12-04 09:48:17');
INSERT INTO `sys_oper_log` VALUES ('122', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"\" ],\r\n  \"zuiDuoIntegral\" : [ \"\" ],\r\n  \"zuiShaoIntegral\" : [ \"\" ],\r\n  \"integralFenJi\" : [ \"\" ],\r\n  ', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\r\n### The error may involve com.ruoyi.integral.mapper.XwIntegralMapper.insertXwIntegral-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into xw_integral    ( behavior_category,    behavior_title,                                status,    create_time,                     dept_id,                          menu_id,                          dept_name )           values ( ?,    ?,                                ?,    ?,                      ?,                          ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'\' for column \'menu_id\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1', '2018-12-04 09:48:41');
INSERT INTO `sys_oper_log` VALUES ('123', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"PP\" ],\r\n  \"behaviorContent\" : [ \"长期固定积分项目设置\" ],\r\n  \"shenQingFangShi\" : [ \"0\" ],\r\n  \"zuiDuoIntegral\" : [ \"10000\" ],\r\n  \"zuiShaoIntegra', '0', null, '2018-12-04 09:51:20');
INSERT INTO `sys_oper_log` VALUES ('124', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"\" ],\r\n  \"zuiDuoIntegral\" : [ \"\" ],\r\n  \"zuiShaoIntegral\" : [ \"\" ],\r\n  \"integralFenJi\" : [ \"\" ],\r\n  ', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\r\n### The error may involve com.ruoyi.integral.mapper.XwIntegralMapper.insertXwIntegral-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into xw_integral    ( behavior_category,    behavior_title,                                status,    create_time,                     dept_id,                          menu_id,                          dept_name )           values ( ?,    ?,                                ?,    ?,                      ?,                          ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'\' for column \'menu_id\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1', '2018-12-04 09:51:48');
INSERT INTO `sys_oper_log` VALUES ('125', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"\" ],\r\n  \"zuiDuoIntegral\" : [ \"\" ],\r\n  \"zuiShaoIntegral\" : [ \"\" ],\r\n  \"integralFenJi\" : [ \"\" ],\r\n  ', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\r\n### The error may involve com.ruoyi.integral.mapper.XwIntegralMapper.insertXwIntegral-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into xw_integral    ( behavior_category,    behavior_title,                                status,    create_time,                     dept_id,                          menu_id,                          dept_name )           values ( ?,    ?,                                ?,    ?,                      ?,                          ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'\' for column \'menu_id\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1', '2018-12-04 09:54:25');
INSERT INTO `sys_oper_log` VALUES ('126', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"积分内容\" ],\r\n  \"shenQingFangShi\" : [ \"0\" ],\r\n  \"zuiDuoIntegral\" : [ \"100\" ],\r\n  \"zuiShaoIntegral\" : [', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\r\n### The error may involve com.ruoyi.integral.mapper.XwIntegralMapper.insertXwIntegral-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into xw_integral    ( behavior_category,    behavior_title,    behavior_content,    shen_qing_fang_shi,        zui_duo_integral,    zui_shao_integral,            status,    create_time,                     dept_id,                          menu_id,                          dept_name )           values ( ?,    ?,    ?,    ?,        ?,    ?,            ?,    ?,                      ?,                          ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'\' for column \'menu_id\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1', '2018-12-04 09:59:30');
INSERT INTO `sys_oper_log` VALUES ('127', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX XX', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"积分内容\" ],\r\n  \"shenQingFangShi\" : [ \"0\" ],\r\n  \"zuiDuoIntegral\" : [ \"100\" ],\r\n  \"zuiShaoIntegral\" : [', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\r\n### The error may involve com.ruoyi.integral.mapper.XwIntegralMapper.insertXwIntegral-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into xw_integral    ( behavior_category,    behavior_title,    behavior_content,    shen_qing_fang_shi,        zui_duo_integral,    zui_shao_integral,            status,    create_time,                     dept_id,                          menu_id,                          dept_name )           values ( ?,    ?,    ?,    ?,        ?,    ?,            ?,    ?,                      ?,                          ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'\' for column \'menu_id\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1', '2018-12-04 09:59:52');
INSERT INTO `sys_oper_log` VALUES ('128', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX XX', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"积分内容\" ],\r\n  \"shenQingFangShi\" : [ \"0\" ],\r\n  \"zuiDuoIntegral\" : [ \"100\" ],\r\n  \"zuiShaoIntegral\" : [', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\r\n### The error may involve com.ruoyi.integral.mapper.XwIntegralMapper.insertXwIntegral-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into xw_integral    ( behavior_category,    behavior_title,    behavior_content,    shen_qing_fang_shi,        zui_duo_integral,    zui_shao_integral,    integral_fen_ji,        status,    create_time,                     dept_id,                          menu_id,                          dept_name )           values ( ?,    ?,    ?,    ?,        ?,    ?,    ?,        ?,    ?,                      ?,                          ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'\' for column \'menu_id\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1', '2018-12-04 09:59:59');
INSERT INTO `sys_oper_log` VALUES ('129', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"积分内容\" ],\r\n  \"shenQingFangShi\" : [ \"0\" ],\r\n  \"zuiDuoIntegral\" : [ \"100\" ],\r\n  \"zuiShaoIntegral\" : [', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\r\n### The error may involve com.ruoyi.integral.mapper.XwIntegralMapper.insertXwIntegral-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into xw_integral    ( behavior_category,    behavior_title,    behavior_content,    shen_qing_fang_shi,        zui_duo_integral,    zui_shao_integral,    integral_fen_ji,        status,    create_time,                     dept_id,                          menu_id,                          dept_name )           values ( ?,    ?,    ?,    ?,        ?,    ?,    ?,        ?,    ?,                      ?,                          ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'\' for column \'menu_id\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'\' for column \'menu_id\' at row 1', '2018-12-04 10:00:05');
INSERT INTO `sys_oper_log` VALUES ('130', ' 积分菜单管理', '1', 'com.ruoyi.web.controller.integral.IntegralMenuController.addSave()', '1', 'admin', '集团财务', '/integral/integralMenu/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"parentId\" : [ \"4\" ],\r\n  \"deptId\" : [ \"100\" ],\r\n  \"treeName\" : [ \"行为C管理\" ],\r\n  \"typeName\" : [ \"鼎鼎香\" ],\r\n  \"orderNum\" : [ \"\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"remark\" : [ \"\" ]\r\n}', '0', null, '2018-12-04 10:03:17');
INSERT INTO `sys_oper_log` VALUES ('131', '行为c积分管理', '1', 'com.ruoyi.web.controller.integral.XwIntegralController.addSave()', '1', 'admin', '集团财务', '/integral/xwIntegral/add', '127.0.0.1', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"100\" ],\r\n  \"deptName\" : [ \"媚思嘉科技\" ],\r\n  \"behaviorCategory\" : [ \"长期固定积分项目设置\" ],\r\n  \"behaviorTitle\" : [ \"积分标题\" ],\r\n  \"behaviorContent\" : [ \"\" ],\r\n  \"shenQingFangShi\" : [ \"1\" ],\r\n  \"zuiDuoIntegral\" : [ \"200\" ],\r\n  \"zuiShaoIntegral\" : [ \"20', '0', null, '2018-12-04 10:14:17');
INSERT INTO `sys_oper_log` VALUES ('132', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX XX', '{\r\n  \"userId\" : [ \"39\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"10000\" ],\r\n  \"biaoInt', '0', null, '2018-12-04 11:35:27');
INSERT INTO `sys_oper_log` VALUES ('133', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"10000\" ],\r\n  \"biaoInt', '0', null, '2018-12-04 11:36:29');
INSERT INTO `sys_oper_log` VALUES ('134', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '192.168.0.104', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"10000\" ],\r\n  \"biaoInt', '0', null, '2018-12-04 11:56:03');
INSERT INTO `sys_oper_log` VALUES ('135', '重置密码', '2', 'com.ruoyi.web.controller.system.SysUserController.resetPwd()', '1', 'admin', '集团财务', '/system/user/resetPwd/39', '192.168.0.104', 'XX 内网IP', '{ }', '0', null, '2018-12-04 11:58:15');
INSERT INTO `sys_oper_log` VALUES ('136', '重置密码', '2', 'com.ruoyi.web.controller.system.SysUserController.resetPwdSave()', '1', 'admin', '集团财务', '/system/user/resetPwd', '192.168.0.104', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ],\r\n  \"loginName\" : [ \"caofei\" ],\r\n  \"password\" : [ \"123456\" ]\r\n}', '0', null, '2018-12-04 11:58:24');
INSERT INTO `sys_oper_log` VALUES ('137', '用户管理', '1', 'com.ruoyi.web.controller.system.SysUserController.addSave()', '1', 'admin', '集团财务', '/system/user/add', '192.168.0.104', 'XX 内网IP', '{\r\n  \"deptId\" : [ \"111\" ],\r\n  \"loginName\" : [ \"sunli\" ],\r\n  \"userName\" : [ \"孙丽\" ],\r\n  \"password\" : [ \"123456\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"jiChuInte', '0', null, '2018-12-04 11:59:54');
INSERT INTO `sys_oper_log` VALUES ('138', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'admin', '集团财务', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"1\" ]\r\n}', '0', null, '2018-12-04 13:47:59');
INSERT INTO `sys_oper_log` VALUES ('139', '设置 普通审批人和高级审批人', '0', 'com.ruoyi.web.controller.system.SysUserController.sysApp()', '1', 'admin', '集团财务', '/system/user/sysApp/40/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-12-04 13:56:49');
INSERT INTO `sys_oper_log` VALUES ('140', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"39\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"10000\" ],\r\n  \"biaoIn', '0', null, '2018-12-04 13:59:37');
INSERT INTO `sys_oper_log` VALUES ('141', '用户管理', '3', 'com.ruoyi.web.controller.system.SysUserController.remove()', '1', 'admin', '集团财务', '/system/user/remove', '192.168.0.104', 'XX 内网IP', '{\r\n  \"ids\" : [ \"39\" ]\r\n}', '0', null, '2018-12-04 16:38:31');
INSERT INTO `sys_oper_log` VALUES ('142', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"9\" ],\r\n  \"deptId\" : [ \"100\" ],\r\n  \"userName\" : [ \"孙瑞娜\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"400\" ],\r\n  \"biaoInteg', '0', null, '2018-12-05 12:52:58');
INSERT INTO `sys_oper_log` VALUES ('143', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"9\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"孙瑞娜\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"400\" ],\r\n  \"biaoInteg', '0', null, '2018-12-05 12:54:02');
INSERT INTO `sys_oper_log` VALUES ('144', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"10\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"孙瑞娜\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ ', '0', null, '2018-12-05 13:42:08');
INSERT INTO `sys_oper_log` VALUES ('145', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX XX', '{\r\n  \"userId\" : [ \"10\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"孙瑞娜\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ ', '0', null, '2018-12-05 13:43:29');
INSERT INTO `sys_oper_log` VALUES ('146', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', '1', 'admin', '集团财务', '/system/user/profile/updateAvatar', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"1\" ]\r\n}', '0', null, '2018-12-05 13:51:23');
INSERT INTO `sys_oper_log` VALUES ('147', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX XX', '{\r\n  \"userId\" : [ \"10\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"孙瑞娜\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"8\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ ', '0', null, '2018-12-05 13:52:17');
INSERT INTO `sys_oper_log` VALUES ('148', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"10\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"孙瑞娜\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"500\" ],\r\n  \"biaoInt', '0', null, '2018-12-05 13:53:00');
INSERT INTO `sys_oper_log` VALUES ('149', '岗位管理', '3', 'com.ruoyi.web.controller.system.SysPostController.remove()', '1', 'admin', '集团财务', '/system/post/remove', '127.0.0.1', 'XX 内网IP', '{\r\n  \"ids\" : [ \"8\" ]\r\n}', '0', null, '2018-12-05 13:53:16');
INSERT INTO `sys_oper_log` VALUES ('150', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"13\" ],\r\n  \"deptId\" : [ \"100\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"2\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"', '0', null, '2018-12-05 14:21:43');
INSERT INTO `sys_oper_log` VALUES ('151', '设置 普通审批人和高级审批人', '0', 'com.ruoyi.web.controller.system.SysUserController.sysApp()', '1', 'admin', '集团财务', '/system/user/sysApp/10/1', '127.0.0.1', 'XX 内网IP', '{ }', '0', null, '2018-12-05 14:27:20');
INSERT INTO `sys_oper_log` VALUES ('152', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX XX', '{\r\n  \"userId\" : [ \"10\" ],\r\n  \"deptId\" : [ \"111\" ],\r\n  \"userName\" : [ \"孙瑞娜\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"sex\" : [ \"1\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ ', '0', null, '2018-12-05 16:31:34');
INSERT INTO `sys_oper_log` VALUES ('153', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"15\" ],\r\n  \"deptId\" : [ \"100\" ],\r\n  \"userName\" : [ \"孙瑞娜\" ],\r\n  \"phonenumber\" : [ \"18833424134\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"', '0', null, '2018-12-05 16:50:09');
INSERT INTO `sys_oper_log` VALUES ('154', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"18\" ],\r\n  \"deptId\" : [ \"100\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"4', '0', null, '2018-12-05 17:15:33');
INSERT INTO `sys_oper_log` VALUES ('155', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX 内网IP', '{\r\n  \"userId\" : [ \"18\" ],\r\n  \"deptId\" : [ \"100\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"1\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"', '0', null, '2018-12-05 17:15:46');
INSERT INTO `sys_oper_log` VALUES ('156', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.editSave()', '1', 'admin', '集团财务', '/system/user/edit', '127.0.0.1', 'XX XX', '{\r\n  \"userId\" : [ \"18\" ],\r\n  \"deptId\" : [ \"100\" ],\r\n  \"userName\" : [ \"曹飞\" ],\r\n  \"phonenumber\" : [ \"13131936792\" ],\r\n  \"sex\" : [ \"0\" ],\r\n  \"status\" : [ \"0\" ],\r\n  \"roleIds\" : [ \"3\" ],\r\n  \"postIds\" : [ \"7\" ],\r\n  \"integral\" : [ \"0\" ],\r\n  \"jiChuIntegral\" : [ \"', '0', null, '2018-12-05 17:19:16');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) DEFAULT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `integral` varchar(500) DEFAULT '' COMMENT '岗位积分',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1', 'ceo', '总裁', '1', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-11-13 11:54:48', '', '1000');
INSERT INTO `sys_post` VALUES ('2', 'se', '职能总监', '2', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '', '900');
INSERT INTO `sys_post` VALUES ('3', 'hr', '职能经理/事业部总经理', '3', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '', '800');
INSERT INTO `sys_post` VALUES ('4', 'user', '事业部经理/区域经理', '4', '0', 'admin', '2018-03-16 11:33:00', 'ry', '2018-03-16 11:33:00', '', '700');
INSERT INTO `sys_post` VALUES ('5', null, '店经理', '5', '0', 'admin', '2018-03-16 11:33:00', '', '2018-03-16 11:33:00', '', '600');
INSERT INTO `sys_post` VALUES ('6', null, '顾问', '6', '0', 'admin', '2018-03-16 11:33:00', '', '2018-03-16 11:33:00', '', '500');
INSERT INTO `sys_post` VALUES ('7', null, '美容师/理疗师/专员', '7', '0', 'admin', '2018-03-16 11:33:00', '', '2018-03-16 11:33:00', '', '400');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '管理员', 'admin', '1', '2', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-12-03 15:30:58', '管理员');
INSERT INTO `sys_role` VALUES ('2', '普通角色', 'common', '2', '2', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2018-11-23 14:10:55', '普通角色');
INSERT INTO `sys_role` VALUES ('3', '超级管理员', 'superAdmin', '3', '1', '0', '0', 'admin', '2018-11-16 10:23:37', 'admin', '2018-11-20 17:43:05', '超级管理员');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `dept_id` int(11) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('1', '100');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '100');
INSERT INTO `sys_role_menu` VALUES ('1', '101');
INSERT INTO `sys_role_menu` VALUES ('1', '102');
INSERT INTO `sys_role_menu` VALUES ('1', '103');
INSERT INTO `sys_role_menu` VALUES ('1', '104');
INSERT INTO `sys_role_menu` VALUES ('1', '105');
INSERT INTO `sys_role_menu` VALUES ('1', '106');
INSERT INTO `sys_role_menu` VALUES ('1', '107');
INSERT INTO `sys_role_menu` VALUES ('1', '108');
INSERT INTO `sys_role_menu` VALUES ('1', '109');
INSERT INTO `sys_role_menu` VALUES ('1', '110');
INSERT INTO `sys_role_menu` VALUES ('1', '111');
INSERT INTO `sys_role_menu` VALUES ('1', '112');
INSERT INTO `sys_role_menu` VALUES ('1', '113');
INSERT INTO `sys_role_menu` VALUES ('1', '500');
INSERT INTO `sys_role_menu` VALUES ('1', '501');
INSERT INTO `sys_role_menu` VALUES ('1', '1000');
INSERT INTO `sys_role_menu` VALUES ('1', '1001');
INSERT INTO `sys_role_menu` VALUES ('1', '1002');
INSERT INTO `sys_role_menu` VALUES ('1', '1003');
INSERT INTO `sys_role_menu` VALUES ('1', '1004');
INSERT INTO `sys_role_menu` VALUES ('1', '1005');
INSERT INTO `sys_role_menu` VALUES ('1', '1006');
INSERT INTO `sys_role_menu` VALUES ('1', '1007');
INSERT INTO `sys_role_menu` VALUES ('1', '1008');
INSERT INTO `sys_role_menu` VALUES ('1', '1009');
INSERT INTO `sys_role_menu` VALUES ('1', '1010');
INSERT INTO `sys_role_menu` VALUES ('1', '1011');
INSERT INTO `sys_role_menu` VALUES ('1', '1012');
INSERT INTO `sys_role_menu` VALUES ('1', '1013');
INSERT INTO `sys_role_menu` VALUES ('1', '1014');
INSERT INTO `sys_role_menu` VALUES ('1', '1015');
INSERT INTO `sys_role_menu` VALUES ('1', '1016');
INSERT INTO `sys_role_menu` VALUES ('1', '1017');
INSERT INTO `sys_role_menu` VALUES ('1', '1018');
INSERT INTO `sys_role_menu` VALUES ('1', '1019');
INSERT INTO `sys_role_menu` VALUES ('1', '1020');
INSERT INTO `sys_role_menu` VALUES ('1', '1021');
INSERT INTO `sys_role_menu` VALUES ('1', '1022');
INSERT INTO `sys_role_menu` VALUES ('1', '1023');
INSERT INTO `sys_role_menu` VALUES ('1', '1024');
INSERT INTO `sys_role_menu` VALUES ('1', '1025');
INSERT INTO `sys_role_menu` VALUES ('1', '1026');
INSERT INTO `sys_role_menu` VALUES ('1', '1027');
INSERT INTO `sys_role_menu` VALUES ('1', '1028');
INSERT INTO `sys_role_menu` VALUES ('1', '1029');
INSERT INTO `sys_role_menu` VALUES ('1', '1030');
INSERT INTO `sys_role_menu` VALUES ('1', '1031');
INSERT INTO `sys_role_menu` VALUES ('1', '1032');
INSERT INTO `sys_role_menu` VALUES ('1', '1033');
INSERT INTO `sys_role_menu` VALUES ('1', '1034');
INSERT INTO `sys_role_menu` VALUES ('1', '1035');
INSERT INTO `sys_role_menu` VALUES ('1', '1036');
INSERT INTO `sys_role_menu` VALUES ('1', '1037');
INSERT INTO `sys_role_menu` VALUES ('1', '1038');
INSERT INTO `sys_role_menu` VALUES ('1', '1039');
INSERT INTO `sys_role_menu` VALUES ('1', '1040');
INSERT INTO `sys_role_menu` VALUES ('1', '1041');
INSERT INTO `sys_role_menu` VALUES ('1', '1042');
INSERT INTO `sys_role_menu` VALUES ('1', '1043');
INSERT INTO `sys_role_menu` VALUES ('1', '1044');
INSERT INTO `sys_role_menu` VALUES ('1', '1045');
INSERT INTO `sys_role_menu` VALUES ('1', '1046');
INSERT INTO `sys_role_menu` VALUES ('1', '1047');
INSERT INTO `sys_role_menu` VALUES ('1', '1048');
INSERT INTO `sys_role_menu` VALUES ('1', '1049');
INSERT INTO `sys_role_menu` VALUES ('1', '1050');
INSERT INTO `sys_role_menu` VALUES ('1', '1051');
INSERT INTO `sys_role_menu` VALUES ('1', '1052');
INSERT INTO `sys_role_menu` VALUES ('1', '1053');
INSERT INTO `sys_role_menu` VALUES ('1', '1054');
INSERT INTO `sys_role_menu` VALUES ('1', '1055');
INSERT INTO `sys_role_menu` VALUES ('1', '1056');
INSERT INTO `sys_role_menu` VALUES ('1', '1057');
INSERT INTO `sys_role_menu` VALUES ('1', '1058');
INSERT INTO `sys_role_menu` VALUES ('1', '1059');
INSERT INTO `sys_role_menu` VALUES ('1', '1060');
INSERT INTO `sys_role_menu` VALUES ('1', '1061');
INSERT INTO `sys_role_menu` VALUES ('1', '1062');
INSERT INTO `sys_role_menu` VALUES ('1', '1063');
INSERT INTO `sys_role_menu` VALUES ('1', '1064');
INSERT INTO `sys_role_menu` VALUES ('1', '1065');
INSERT INTO `sys_role_menu` VALUES ('1', '1066');
INSERT INTO `sys_role_menu` VALUES ('1', '1067');
INSERT INTO `sys_role_menu` VALUES ('1', '1068');
INSERT INTO `sys_role_menu` VALUES ('1', '1069');
INSERT INTO `sys_role_menu` VALUES ('1', '1070');
INSERT INTO `sys_role_menu` VALUES ('1', '1071');
INSERT INTO `sys_role_menu` VALUES ('1', '1072');
INSERT INTO `sys_role_menu` VALUES ('1', '1073');
INSERT INTO `sys_role_menu` VALUES ('1', '1074');
INSERT INTO `sys_role_menu` VALUES ('1', '1075');
INSERT INTO `sys_role_menu` VALUES ('1', '1076');
INSERT INTO `sys_role_menu` VALUES ('1', '1077');
INSERT INTO `sys_role_menu` VALUES ('1', '1078');
INSERT INTO `sys_role_menu` VALUES ('1', '1079');
INSERT INTO `sys_role_menu` VALUES ('1', '1080');
INSERT INTO `sys_role_menu` VALUES ('1', '1081');
INSERT INTO `sys_role_menu` VALUES ('1', '1082');
INSERT INTO `sys_role_menu` VALUES ('1', '1083');
INSERT INTO `sys_role_menu` VALUES ('1', '1084');
INSERT INTO `sys_role_menu` VALUES ('1', '1085');
INSERT INTO `sys_role_menu` VALUES ('1', '1086');
INSERT INTO `sys_role_menu` VALUES ('1', '1087');
INSERT INTO `sys_role_menu` VALUES ('1', '1088');
INSERT INTO `sys_role_menu` VALUES ('1', '1089');
INSERT INTO `sys_role_menu` VALUES ('1', '1090');
INSERT INTO `sys_role_menu` VALUES ('1', '1091');
INSERT INTO `sys_role_menu` VALUES ('1', '1092');
INSERT INTO `sys_role_menu` VALUES ('1', '1093');
INSERT INTO `sys_role_menu` VALUES ('1', '1094');
INSERT INTO `sys_role_menu` VALUES ('1', '1095');
INSERT INTO `sys_role_menu` VALUES ('1', '1096');
INSERT INTO `sys_role_menu` VALUES ('1', '1097');
INSERT INTO `sys_role_menu` VALUES ('1', '1098');
INSERT INTO `sys_role_menu` VALUES ('1', '1099');
INSERT INTO `sys_role_menu` VALUES ('1', '1106');
INSERT INTO `sys_role_menu` VALUES ('1', '1107');
INSERT INTO `sys_role_menu` VALUES ('1', '1108');
INSERT INTO `sys_role_menu` VALUES ('1', '1109');
INSERT INTO `sys_role_menu` VALUES ('1', '1110');
INSERT INTO `sys_role_menu` VALUES ('1', '1116');
INSERT INTO `sys_role_menu` VALUES ('1', '1117');
INSERT INTO `sys_role_menu` VALUES ('1', '1118');
INSERT INTO `sys_role_menu` VALUES ('1', '1119');
INSERT INTO `sys_role_menu` VALUES ('1', '1120');
INSERT INTO `sys_role_menu` VALUES ('1', '1121');
INSERT INTO `sys_role_menu` VALUES ('1', '1122');
INSERT INTO `sys_role_menu` VALUES ('1', '1123');
INSERT INTO `sys_role_menu` VALUES ('1', '1124');
INSERT INTO `sys_role_menu` VALUES ('1', '1125');
INSERT INTO `sys_role_menu` VALUES ('1', '1126');
INSERT INTO `sys_role_menu` VALUES ('1', '1127');
INSERT INTO `sys_role_menu` VALUES ('1', '1128');
INSERT INTO `sys_role_menu` VALUES ('1', '1129');
INSERT INTO `sys_role_menu` VALUES ('1', '1130');
INSERT INTO `sys_role_menu` VALUES ('1', '1131');
INSERT INTO `sys_role_menu` VALUES ('1', '1132');
INSERT INTO `sys_role_menu` VALUES ('1', '1133');
INSERT INTO `sys_role_menu` VALUES ('1', '1134');
INSERT INTO `sys_role_menu` VALUES ('1', '1135');
INSERT INTO `sys_role_menu` VALUES ('1', '1136');
INSERT INTO `sys_role_menu` VALUES ('1', '1137');
INSERT INTO `sys_role_menu` VALUES ('1', '1138');
INSERT INTO `sys_role_menu` VALUES ('1', '1139');
INSERT INTO `sys_role_menu` VALUES ('1', '1140');
INSERT INTO `sys_role_menu` VALUES ('1', '1141');
INSERT INTO `sys_role_menu` VALUES ('1', '1142');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）    0 在职 1.离职',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）  参与积分排名 0,参与 2 不参与',
  `login_ip` varchar(20) DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `integral` bigint(255) DEFAULT '0' COMMENT '积分',
  `ji_chu_integral` bigint(255) DEFAULT '0' COMMENT '基础积分',
  `biao_integral` bigint(255) DEFAULT '0' COMMENT '可用表样积分',
  `love_integral` bigint(255) DEFAULT '0' COMMENT '本月可用爱心积分',
  `integral_status` int(11) DEFAULT '0' COMMENT '是否参加积分活动（1参加  2不参加）',
  `is_approver` int(11) DEFAULT '0' COMMENT '是不是审批人(1.是审批人 3.不是审批人 2.高级审批人)',
  `dept_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '103', 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', 'http://192.168.0.101:8080/cdc25cd7-85ce-4a34-8297-fc9547852ef1blob.jpg', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '192.168.0.104', '2018-12-04 13:51:43', 'admin', '2018-03-16 11:33:00', 'ry', '2018-12-05 13:51:23', '管理员', '100', '100', '500', '1000', '1', '3', null);
INSERT INTO `sys_user` VALUES ('23', '100', '曹飞', '曹飞', '00', '', '13131936792', '0', 'https://static.dingtalk.com/media/lADPDgQ9qYCi_YPNAnDNAnA_624_624.jpg', '', '', '0', '0', '', null, '', '2018-12-07 16:13:38', '', null, '', '0', '400', '0', '0', '1', '3', null);
INSERT INTO `sys_user` VALUES ('24', '100', '孙瑞娜', '孙瑞娜', '00', '', '18833424134', '0', '', '', '', '0', '0', '', null, '', '2018-12-07 16:34:52', '', null, '', '0', '400', '0', '0', '1', '3', null);
INSERT INTO `sys_user` VALUES ('25', '100', '陈莉', '陈莉', '00', '', '17798147767', '0', '', '', '', '0', '0', '', null, '', '2018-12-07 16:35:01', '', null, '', '0', '400', '0', '0', '1', '3', null);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) DEFAULT '0' COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='在线用户记录';

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('2ff7ce15-25b0-47db-824e-71d1f6cf9c02', 'admin', '集团财务', '127.0.0.1', 'XX 内网IP', 'Chrome', 'Windows 7', 'on_line', '2018-12-06 08:38:57', '2018-12-06 08:43:01', '1800000');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `post_id` int(11) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES ('23', '7');
INSERT INTO `sys_user_post` VALUES ('24', '7');
INSERT INTO `sys_user_post` VALUES ('25', '7');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');

-- ----------------------------
-- Table structure for xw_integral
-- ----------------------------
DROP TABLE IF EXISTS `xw_integral`;
CREATE TABLE `xw_integral` (
  `behavior_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '行为积分主键',
  `behavior_category` varchar(255) DEFAULT '' COMMENT '类别',
  `behavior_title` varchar(255) DEFAULT '' COMMENT '行为标题',
  `behavior_content` varchar(255) DEFAULT '' COMMENT '行为内容',
  `shen_qing_fang_shi` varchar(20) DEFAULT '' COMMENT '申请方式(0.一天  1一周 2.一月)',
  `type_id` int(10) DEFAULT '0' COMMENT '类型',
  `zui_duo_integral` varchar(20) DEFAULT '' COMMENT '最多奖励',
  `zui_shao_integral` varchar(255) DEFAULT '' COMMENT '最少奖励积分',
  `integral_fen_ji` int(11) DEFAULT NULL COMMENT '积分分级',
  `yi_wan_cheng_ci_shu` int(11) DEFAULT '0' COMMENT '已完成次数',
  `status` int(1) DEFAULT NULL COMMENT '使用转态 0,使用中  1.禁用   2 设置审核人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `post_id` int(11) DEFAULT '0' COMMENT '职位名称',
  `post_name` varchar(20) DEFAULT NULL COMMENT '职位名称',
  `dept_id` int(11) DEFAULT '0' COMMENT '部门id',
  `dept_name` varchar(20) DEFAULT NULL COMMENT '部门名称',
  `menu_id` int(11) DEFAULT NULL COMMENT '积分餐单id',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`behavior_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='行为c积分管理';

-- ----------------------------
-- Records of xw_integral
-- ----------------------------
INSERT INTO `xw_integral` VALUES ('1', '长期固定积分项目设置', '积分', '长期固定积分项目设置', '0', '0', '10000', '100', null, '0', '0', '2018-12-04 09:51:00', null, '0', null, '100', '媚思嘉科技', '4', '');
INSERT INTO `xw_integral` VALUES ('2', '长期固定积分项目设置', '积分标题', '', '1', '0', '200', '200', null, '0', '0', '2018-12-04 10:14:17', null, '0', null, '100', '媚思嘉科技', '4', '');

-- ----------------------------
-- Table structure for yj_integral
-- ----------------------------
DROP TABLE IF EXISTS `yj_integral`;
CREATE TABLE `yj_integral` (
  `behavior_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '业绩积分主键',
  `behavior_category` varchar(255) DEFAULT '' COMMENT '类别',
  `behavior_title` varchar(255) DEFAULT '' COMMENT '业绩标题',
  `behavior_content` varchar(255) DEFAULT '' COMMENT '业绩内容',
  `shen_qing_fang_shi` varchar(20) DEFAULT '' COMMENT '申请方式',
  `type_id` int(10) DEFAULT '0' COMMENT '类型',
  `zui_duo_integral` varchar(20) DEFAULT '' COMMENT '最多奖励',
  `zui_shao_integral` varchar(255) DEFAULT '' COMMENT '最少奖励积分',
  `integral_fen_ji` int(11) DEFAULT NULL COMMENT '积分分级',
  `yi_wan_cheng_ci_shu` int(11) DEFAULT '0' COMMENT '已完成次数',
  `status` int(1) DEFAULT '0' COMMENT '使用转态 0,使用中  1.禁用   2 设置审核人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单主键',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `post_id` int(11) DEFAULT NULL COMMENT '职位id',
  `post_name` varchar(20) DEFAULT NULL COMMENT '职位名称',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门id',
  `dept_name` varchar(20) DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`behavior_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业绩B积分管理';

-- ----------------------------
-- Records of yj_integral
-- ----------------------------
